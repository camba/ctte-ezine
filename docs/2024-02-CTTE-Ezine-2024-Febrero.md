---
title: Febrero 2024
---

# Cambá CTTE Ezine Febrero 2024

<div style="text-align: center;">

![Constitucion2](@img/febrero2024.png)

</div>

```
Estoy condenanda a olvidar lo que he sido
Estoy destinada a ignorar donde voy
Donde voy

Estoy atrapada por lo que más quiero
Perdida en la guerra tan solo por hoy
Por hoy

Perdida en la tierra
Por hoy
Un ángel en guerra
Por hoy

Estoy esperando que pase el olvido
No supe quien eras ni sabras quien soy
Quien soy

Estoy obligada a vivir sus mentiras
No tengo pesares no tengo ni doy
Ni doy

Perdida en la tierra
Por hoy
Un ángel en guerra
Por hoy
```

Letra del tema [Angel en guerra](https://www.youtube.com/watch?v=bp3dPvkPaXY) de [Barbarita Palacios](https://www.barbaritapalacios.com.ar/).


## Cooperativas/Economía Popular
- [Politizar la tecnología - radios comunitarias y derecho a la comunicación en los territorios digitales](https://radioslibres.net/wp-content/uploads/2020/07/Politizar-la-tecnologia_Binder-Garcia_Gago.pdf)
- [Los Pioneros de Rochdale y los 7 Principios cooperativos](https://www.youtube.com/watch?v=1bHHpEsBLmQ)
- [Cooperativismo: Estatuto y reglamentos internos](https://www.youtube.com/watch?v=4K9jFAgiO9I)


## Herramientas de software
- [Deskhop](https://github.com/hrvach/deskhop) - Dispositivo para cambiar rápidamente de escritorios
- [Armitage](https://gitlab.com/kalilinux/packages/armitage) - Herramienta gráfica de gestión de cyber ataques para metasploit.
- [Loco.rs](https://github.com/loco-rs/loco) - Framework para una persona en Rust para startups y proyectos de costado.
- [SendScriptWhatsApp](https://github.com/Matt-Fontes/SendScriptWhatsApp) - Script para enviar el guión de Shrek por WhatsApp


## Informes/Encuestas/Lanzamientos/Capacitaciones
- [XKCD Comics de Linux y Unix fans](https://www.cyberciti.biz/humour/top-xkcd-comics-for-linux-and-unix-fans/)
- [¿Qué viene despues del software libre? Bruce Perens esta trabajando es eso](https://www.theregister.com/2023/12/27/bruce_perens_post_open/)
- [Compendio de recursos de Trabajo de Software Libre](https://github.com/fossjobs/fossjobs/wiki/resources)

## Noticias, Enlaces y Textos

### Historia del "Hacking"
- [sudo apt-get install anarchy](https://hackstory.net/Hackstory.es_sudo_apt-get_install_anarchy.html) - Declaración de independencia del ciberespacio
- [Aprende sobre la demoscene en 14 dias](https://github.com/psenough/teach_yourself_demoscene_in_14_days)
- [HackStory](https://hackstory.es/) - Libro de la historia nunca contada del underground hacker en la península Ibérica escrito por [Mercé Molist](https://es.wikipedia.org/wiki/Merc%C3%A8_Molist). [Bajar libro](https://hackstory.es/ebook/Hackstory%20-%20Merce%20Molist%20Ferrer.pdf)
- [Hackea el sistema! Los grupos anarquistas de hacking](https://hispagatos.org/post/historiadegruposanarquista/)


### Tecnología
- [Estandarización de los Webhooks](https://github.com/standard-webhooks/standard-webhooks/blob/main/spec/standard-webhooks.md)
- [La confianza y la inteligencia artificial](https://www.schneier.com/blog/archives/2023/12/ai-and-trust.html)
- [La excepcionalidad belleza del código fuente de doom 3](https://kotaku.com/the-exceptional-beauty-of-doom-3s-source-code-5975610)
- [¿Cómo yo de alguna forma maté Mercurial en Mozilla?](https://glandium.org/blog/?p=4346)
- [React: Chau Chau al useState, useEffect](https://medium.com/@emmanuelodii80/bye-bye-usestate-useeffect-revolutionizing-react-development-d91f95891adb)
