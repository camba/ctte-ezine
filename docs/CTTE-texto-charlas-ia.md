# Regulación de la Inteligencia Artificial | Enrique Chaparro en el Congreso de la Nación
> [Video](https://www.youtube.com/watch?v=_MOyU9rRpa4)

## Objetivos
- Intersección entre de lo Digital y Derechos fundamentales de las personas
- Esfuerzos concertados para el marco regulatorio
- Generar fronteras (fijar limites de lo posible hacer) pero no barreras

## ¿Qué es la IA?
- Tiene inmensas capacidades y pero puede ser usada con malos propositos con buena o malas intensiones
- Hablamos de muchas cosas diferentes
- Llamamos IA a algo que no tiene inteligenica
- Son Matrices jacobianas, distnacias entre vectores

## ¿Cuando estamos hablando de IA en la actualidad, de que estamos hablando?
Modelos de aprendiaje profundo (segmento de la IA) con capacidad generativa basado en **transformers**:
- Modelos de difusión
- Grandes modelos del Lenguaje ( Calculos estadístico de la próxima palabra)

## ¿Cuales son los ejes éticos centrales en los que basar la legislación?
Bioética, que se usa en la Medicina:
- Principios de autonomia, beneciencia, No maleficiencia y justicia
- Agregando un principio de explicabilidad (Sentido epistemologico y carga de responsabilidad, como funciona y quien lo hace)

## Problemática
- El problema no es que nos quitan trabajao, sino que nos quiten ingresos
- Si ayudan a disminuir el trabajo mucho mejor (Ej: pasar de una jornada semanal de 40 a 30)
- ¿Cómo hacemos para que los beneficios agargados sean capitalizados solo por un sector?
- El problema es político y no hay soluciones técnicas para los problemas políticos.

## Alertas
- Uso de recursos naturales (costo energico de entrenar y costo de inferencia)
- Efectos de concentración (Costo de hw y capacidad para desarrollar en el estado del arte)


# Regulación de la Inteligencia Artificial | Fernando Schapachnik en el Congreso de la Nación
> [Video](https://youtu.be/adEdI8DMUyE?t=1465)
## Normativa
- Sin normativa, no hay exportación (Tanto de IA como de Software)
- Adecuación por ejemplo a la UE (niveles de protección para los ciudadanos)


## Riesgos Reales
- Impacto cultural (Ej: Ingles Falklands, Español Malvinas)
- Impacto ambiental (consumo)

## Decisiones Automatizadas
- Sesgo de automatización, las personas le creen a las maquina
- Supervisión humana significativa

## Desafios Comercial
- Presión sobre la Balanza comercial por la Contratación de servicios externos
- Impacto sobre el empleo

## Autoridad de aplicación
- De carácter técnico de fuerte especialización
- Agencia que integre lo informático

## Preparación de RRHH
- Hay que ser informático con especialización en IA
- Sector que salen de las Universidades Públicas


# Otros
- Derechos sobre las creaciones
- Complejos psiquicos
- IA - No tiene responsabilidad, no tiene conciencia, no tiene moral y tampoco piensa
