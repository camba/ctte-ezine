---
title: Enero 2023
---

# Cambá CTTE Ezine Enero 2023

<div style="text- align: center;">

![Argentina](@img/ene2023.jpg)

</div>

```
Yo, monstruo mío
Yo pobre mortal,
equidistante de todo
yo D.N.I: 20.598.061
yo primer hijo de la madre que después fui
yo vieja alumna
de esta escuela de los suplicios

Amazona de mi deseo
Yo, perra en celo de mi sueño rojo

Yo reivindico mi derecho a ser un monstruo
ni varón ni mujer
ni XXY ni H2O

yo monstruo de mi deseo
carne de cada una de mis pinceladas
lienzo azul de mi cuerpo
pintora de mi andar
no quiero más títulos que cargar
no quiero más cargos ni casilleros a donde encajar
ni el nombre justo que me reserve ninguna Ciencia

Yo mariposa ajena a la modernidad
a la posmodernidad
a la normalidad
Oblicua
Bizca
Silvestre
Artesanal

Poeta de la barbarie
con el humus de mi cantar
con el arco iris de mi cantar
con mi aleteo:

Reivindico mi derecho a ser un monstruo
y que otros sean lo Normal
El Vaticano normal
El Credo en dios y la virgísima Normal
los pastores y los rebaños de lo Normal
el Honorable Congreso de las leyes de lo Normal
el viejo Larrouse de lo Normal

Yo solo llevo las prendas de mis cerillas
el rostro de mi mirar
el tacto de lo escuchado y el gesto avispa del besar
y tendré una teta obscena de la luna más perra en mi cintura
y el pene erecto de las guarritas alondras
y 7 lunares
77 lunares
qué digo: 777 lunares de mi endiablada señal de Crear

mi bella monstruosidad
mi ejercicio de inventora
de ramera de las torcazas
mi ser yo entre tanto parecido
entre tanto domesticado
entre tanto metido “de los pelos” en algo
otro nuevo título que cargar
baño de ¿Damas? o ¿Caballeros?
o nuevos rincones para inventar

Yo: trans… pirada
mojada nauseabunda germen de la aurora encantada
la que no pide más permiso
y está rabiosa de luces mayas
luces épicas
luces parias
Menstruales Marlenes Sacayanas bizarras
sin Biblias
sin tablas
sin geografías
sin nada
solo mi derecho vital a ser un monstruo
o como me llame
o como me salga
como me puedan el deseo y las fuckin ganas

mi derecho a explorarme
a reinventarme
hacer de mi mutar mi noble ejercicio
a veranearme otoñarme invernarme:
las hormonas
las ideas
las cachas
y todo el alma… amén

Amén
```

Poema "Yo, monstruo mío" escrito port [Susy Shock](https://es.wikipedia.org/wiki/Susy_Shock), leído por [Alanis Bello](https://sentiido.com/) del [Poemario Trans Pirado](http://susyshock.com.ar/poemario-trans-pirado/)


## Herramientas de software

- [Blockscout](https://github.com/blockscout/blockscout) - Explorador Blockchain para Ethereum
- [Svix-Webhooks](https://github.com/svix/svix-webhooks) - Plataforma para servicios de webhooks
- [WorkAdventure](https://github.com/thecodingmachine/workadventure) - Oficina Virtual como juego 16bits RPG en Aplicación web colaborativa
- [Superplate](https://github.com/pankod/superplate) - Frontend boilerplate (React, Next, etc)
- [Refine](https://github.com/refinedev/refine) - Constructor de CRUDs en React
- [Textualize](https://www.textualize.io/) - TUI Interfaz de usuario con Texto



## Informes/Encuestas/Lanzamientos/Capacitaciones

- [Lambaclass: Camino de aprendizaje](https://github.com/lambdaclass/lambdaclass_hacking_learning_path) - Tutoriales organizados por temática que cubren amplio espectro (Linux, GDB, Unix philosophy, Python, Networks, Git, Db, Docker, Programación Funcional, Erlang, Elixir, Phoenix, OpenAPI, Clojure, Rust, etc)
- [React](https://github.com/midudev/preguntas-entrevista-react) - Compendio de preguntas y respuestas que hacen en entrevistas técnicas
- [Javascript](https://adventjs.dev/es) - 24 retos de programación
- [Aprendiendo Webassembly](https://blog.ttulka.com/learning-webassembly-series/) - Introducción a la temática
- [Postgresql](https://www.crunchydata.com/blog/learn-postgres-at-the-playground) - Aprendiendo en un playground
- Rust - [Rust para principientes](https://www.youtube.com/watch?v=lxPFIDjuKY8),  [Ejericios](https://github.com/rust-lang/rustlings),  [Libro](https://doc.rust-lang.org/book/index.html)



## Noticias, Enlaces y Textos


### Sociales/filosóficas

- [Conectividad y Colectividad](https://youtu.be/MDRCJ5vgSJk?t=593) - Repensar nuestro trabajo especifico, lo remoto, la caracterización de les programadorxs
- [Open AI vs Humanos](https://www.youtube.com/watch?v=U4Mq5-xW24g) - Las máquinas no pueden pensar
- [Sonríe o Muere](https://www.youtube.com/watch?v=kb9fpscybL0) - La trampa del pensamiento positivo de Bárbara Ehrenreich
- [Instalamos una dictadura de pensamiento](https://www.youtube.com/watch?v=1jpYpbS6v5E) - Gustavo Cordera
- Maurizio Lazzarato en Argentina
    - [Taller "Guerra y Finanzas" con Verónica Gago y Luci Cavallero](https://www.youtube.com/watch?v=RCtmcGFAxas)
    - [Diálogo con Diego Sztulwark y Mario Santucho](https://www.youtube.com/watch?v=ij-DJziOYow)


### Tecnología

- [`:(){ :|:& };:`](https://www.cyberciti.biz/faq/understanding-bash-fork-bomb/) - Entendiendo la bomba de bash
- [Webasembly](https://ecostack.dev/posts/wasm-tinygo-vs-rust-vs-assemblyscript/) - Compartiva Go vs Rust, AssemblyScript
- [Cuando usar Grpc y Graphql](https://stackoverflow.blog/2022/11/28/when-to-use-grpc-vs-graphql/)
- [Productividad](https://simonwillison.net/2022/Nov/26/productivity/) - Como mantener de múltiples proyectos de software
- [ChatGPT](https://es.wikipedia.org/wiki/ChatGPT) - Punto de inflexión
    - [Ideas sobre crimenes](https://simonwillison.net/2022/Dec/4/give-me-ideas-for-crimes-to-do/)
    - [¿Programadorxs Junior?](https://medium.com/techtofreedom/chatgpt-this-incredible-ai-chatbot-may-replace-junior-software-engineers-9bdfaee7ff7e)
    - [Nuevos formatos de entrevistas](https://hackernoon.com/will-chatgpt-change-coding-interviews-forever)

### Cooperativas/Economía Popular

- [Panel de Aperturai Plenario FACTTIC: El rol del cooperativismo en la revolución digital](https://www.youtube.com/watch?v=qwPGM90Puek)
- [Resultantes del  plenario FACTTIC Villa la Angostura 2022](https://mailchi.mp/e2f2ba341a17/pas-el-plenario-balance-y-fotos)
- [Desmuteando: Una charla con las coops de FACTTIC](https://www.youtube.com/shorts/gx-0wkCsn_Q)
- [Conectividad desde los territorios - Guardianas del territorio](https://www.youtube.com/watch?v=Lk8i_vnAGEA)
- [Gobierno busca crear empresa estatal de software](https://ansol.com.ar/el-gobierno-busca-crear-una-empresa-estatal-de-software-la-opinion-de-las-cooperativas-de-tecnologia/economia/)
- [Reloj de Ajedrez Parlante en la UNQ](http://www.unq.edu.ar/noticias/6184-alumnos-de-la-unq-presentan-un-reloj-de-ajedrez-parlante.php)
