---
title: Noviembre 2020
---

# Cambá CTTE Ezine Noviembre 2020

## Canción

```
Acá estoy haciendo todo lo que puedo
Aferrándome a lo que soy
pretendiendo que soy superhombre

estoy tratando de manterner
los pies en la tierra
parece que el mundo se esta derrumbando a mi alrededor

Las noches son largas
si, estoy canntado una canción
para tratar de dar respuestas
algo más que un tal vez

Estoy tan confundio
sobre lo que debo hacer
a veces quiero dejarlo todo a un lado

Acá estoy viendome cada vez más viejo
haciéndome cada vez más viejo
y sintiéndome más joven en mi mente

Acá estoy haciendo todo lo que puedo
Aferrándome a lo que soy
pretendiendo que soy superhombre

Estoy trantando dormir
Perdí la cuenta de la ovejas
Mi mente esta funcionando más rápido cada minuto

Qué más puedo hacer
si, no estoy realmente seguro
Sé que estoy corriendo en círculos, pero no puedo detenerme

Estoy tan confundio
sobre lo que debo hacer
a veces quiero dejarlo todo a un lado

Controlando todo en este lugar
Me siento débil, no me siento bien
Tu me dices que tengo que cambiar
Me dices que actúe de acuerdo a mi edad

Pero si todo lo que puedo hacer
es sentarme y ver pasar el tiempo
Entonces tendría que decir adiós
La vida es demasiado corta para verla volar
para verla volar

Acá estoy viéndome cada vez más viejo
haciéndome cada vez más viejo
y sintiéndome más joven en mi mente
```

Canción llamada **Superman** de la banda [Goldfinger](https://es.wikipedia.org/wiki/Goldfinger_(banda)) - [Video con subtitulos](https://www.youtube.com/watch?v=0TBtLyD-IJQ).


## Contribuciones al Software libre de Cambá
- [Live_dj](https://github.com/sgobotta/live_dj/)
- [Tech-Coops](https://github.com/hng/tech-coops/pull/59)

## Herramientas de software

- [eDEX-UI](https://github.com/GitSquared/edex-ui) es un emulador de terminal y monitor de sistema de pantalla completa, multiplataforma y con interfaz de Ciencia Ficción.
- [Eggos](https://github.com/icexin/eggos) - Unikernel hecho en GO para correr directamente sobre x86
- [Game3js](https://github.com/alto-io/game3.js) - Web 3.0 Game Framework basado en principios cooperativistas - [web](https://www.game3js.com/)
- [N8n](https://github.com/n8n-io/n8n) - Automatización de workflows Licenciado con fair-code bajo una Apache 2.0 with Commons Clause - [Web](https://n8n.io/)
- [Cheat.sh](https://github.com/chubin/cheat.sh) - Repositorio de Cheat sheets mundial


## Informes/Encuestas/Lanzamientos/Capacitaciones

- [Lanzamiento de React v17](https://reactjs.org/blog/2020/10/20/react-v17.html)
- [Articulo que resume la historia del último mes del royecto youtube-dl](https://www.zdnet.com/article/github-reinstates-youtube-dl-library-after-eff-intervention/)
- [Los premios más verdes de Tecnología](https://noonies.tech/)
- [Monospace](http://www.p01.org/MONOSPACE/) - Mover puntos con sentimientos, demo js en 1021 bytes, ganadora de la competición Assembly 2020.

# Noticias, Enlaces y Textos

### El deseo de cambiarlo todo

> Si un cuerpo no se define por su pertenencia a una especie, sino por los afectos de los que es capaz, por el grado de su potencia, no se puede apriorísticamente, alejado de la experiencia, lo que podemos. Lo que se puede es lo que se hace, no lo que la pertenencia a la especie de ese cuerpo le dicta qué es o lo que debería ser, sentir y como debería desear. Somos lo que hacemos con lo que de nosotras hicieron - Extracto de [Primavera con Monique Wittig - El devenir lesbiano con el dildo en la mano de Spinoza trasnfeminista de Leonor Silvestri](https://haciendoamigxs.com.ar/queenludd/primavera-con-monique-wittig-el-devenir-lesbiano-con-el-dildo-en-la-mano-de-spinoza-transfeminista/).

- [Nuevo Libro](https://tintalimon.com.ar/libro/reencantar-el-mundo) y [presentación](https://tintalimon.com.ar/post/reencantar-el-mundo/) Reencantar el mundo - Silvia Federici

### Sociales/filosóficos

- [Silvia Alvarez Reflexión Pandemia 2020](./assets/silvia_alvarez_reflexion.mp3)
- [Presentación de Cambiatus.com](https://docs.google.com/presentation/d/1elhM22TYj5-TB859WJOGTd1omtpC7b8AQyQMyhhskJU/edit?fbclid=IwAR2PbmXFBZguN9pYKOWeen4HNYWdzjfEYmPre22UoRkDmzpBWmFLJCZPgS0#slide=id.ga9b43ccc8c_0_22) - [Web](https://cambiatus.com)


### Técnicos

- [Concepto de Miniapps](https://blog.tomayac.com/2020/11/05/learning-from-mini-apps-w3c-tpac-breakout-session/)
- [¿Cómo manejar el síndrome del impostor?](https://dev.to/vtrpldn/how-i-manage-impostor-syndrome-fear-of-failure-and-other-common-programmer-problems-3nk6)


### Movimiento cooperativo

#### Cooperación entre cooperativas

La Federación Argentina de Cooperativas de Trabajo de Tecnología, Innovación y Conocimiento (FACTTIC) cuenta con una sección juvenil, el Espacio de Juventud, a través del cual se proporciona apoyo técnico y soporte para el desarrollo a dos proyectos que han solicitado financiación. El objetivo es servir como ejemplo de cómo las cooperativas de jóvenes pueden ayudarse entre sí a establecerse en distintos continentes. Los proyectos que promociona la FACTTIC son

AfriCo Online, Sudáfrica y Brand K Communications Cooperative Technologies, Kenia
[Ver más info](https://www.globalyouth.coop/es/projects/replication-project/cooperation-amongst-coops)

#### Archivo Histórico del Cooperativismo Argentino (AHCA)

COOPERAR junto con la Universidad de Tres de Febrero, Idelcoop, Iucoop y el Archivo Histórico del Cooperativismo de Crédito, impulsaron este proyecto para constituir un centro documental y guía de archivos de cooperativas que sistematice, localice, y colabore con la conservación de la documentación existente en sus distintos soportes, con el objetivo de ponerla a disposición de investigadores, investigadoras y del movimiento cooperativo para la preservación de su memoria histórica.

Hecho por gcoop. [Archivo histórico](https://www.archivohistorico.coop/es)


#### Otros

- Cooperativismo, producción P2P, y financiación comunitaria en el contexto del procomún. KMO del C-Realm Podcast, entrevista a Michel Bauwens, Dmytri Kleiner y John Restakis
    - [Financiación del Procomún](https://www.guerrillatranslation.es/2014/03/11/dmytri-kleiner-y-la-financiacion-del-procomun-material/)
    - [Hacia un Procomún Material](https://www.guerrillatranslation.es/2014/01/28/hacia-un-procomun-material/)


- [Herramientas colaborativas para trabajar con cooperativas en relación a gobernanza](https://cooperantics.coop/2020/10/01/collaborative-tools/):
    - Tres herramientas para reuniones efectivas
    - Diferentes métodos de toma de decisión (pros y cons)
    - Herramientas de la sociocracia
    - Seeds of change:  sobre el consenso para la toma decisión
    - Seeds of change:  Video sobre consenso


### Otras

- Columna de Tecnología de Neto, Pinky y Cerebro, en Sonámbules en [FM Las CHACRAS 104.9](https://radiolaschacras.blogspot.com/)
   - Entrevista a Beatriz Busaniche de la Fundación Via Libre - [Audio](https://archive.org/details/nota_beatriz_vialibre)
