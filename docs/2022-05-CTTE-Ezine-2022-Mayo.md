---
title: Mayo 2022
---

# Cambá CTTE Ezine Mayo 2022

<div style="text-align: center;">

![frasco](@img/frasco.jpg)

</div>

```
Hace quinientos años, él nació
pero nadie escribió su historia
vinieron los dueños del cielo a matar
a su gente con crucifijos

Cañones y sangre su historia
hace quinientos años, él plantó
una planta en la orilla libre
la masacre no vio
tuvieron que imitarlos o morir
Occidente es el universo

Cañones y hambre en su historia
cañones y sangre su historia

Yo vi, como vio el viento a nuestra tierra
yo vi, la hermosura del viento en tu boca
yo vi, como vio

Su historia no está en museos ni estará
qué la cuenten los que resisten
las ciudades no ven

Yo no represento a nadie
y mi canción
es contar mi versión
mi historia

Yo vi, como vio
y vi, como vio

Yo vi, como vio el viento a nuestra tierra
yo vi, la hermosura del viento en tu boca

Vi, como vio el viento a nuestra tierra
yo vi, la hermosura del viento en tu boca
yo vi, como vio.
```

Letra de la canción [Resistencia Indígena](https://www.youtube.com/watch?v=3VXPsObx1xs) de la banda santafesina [Sig Ragga](https://en.wikipedia.org/wiki/Sig_Ragga)


## Herramientas de software

- [Pywebview](https://github.com/r0x0r/pywebview) - Crea tu GUI para tu programas Python con JavaScript, HTML y CSS
- [Scanphandre](https://github.com/hubblo-org/scaphandre) - ¿Cuánta energía estas consumiendo?
- [Hamster](http://projecthamster.org/) - Graba los Tiempos que usas en tareas
- [Mapscii](https://github.com/rastapasta/mapscii) - Mapas en Ascii


## Informes/Encuestas/Lanzamientos/Capacitaciones

- Lanzamiento de nuevas versiones
    - [Moodle 4](https://moodle.com/moodle-4/)
    - [Ubutnu 22.04 LTS Jammy Jellyfish](https://ubuntu.com/blog/ubuntu-22-04-lts-released)
- [Estado del software libre - Richard Stallman](https://news.slashdot.org/story/22/04/16/2154203/richard-stallman-speaks-on-the-state-of-free-software-and-answers-questions)
- [Análisis de los distintos sistema de mensajeria instantánea](https://twitter.com/Info_Activism/status/1507321427824988179/photo/1) de [Freir Messenger](https://www.freie-messenger.de)
- Radar de Tecnología de Thougthworks - Versión Marzo 2022
  - [Informe General](https://www.thoughtworks.com/content/dam/thoughtworks/documents/radar/2022/03/tr_technology_radar_vol_26_es.pdf)
  - [Tendencias en la industra de la tecnología](https://www.thoughtworks.com/insights/blog/technology-strategy/macro-trends-in-the-tech-industry-march-2022)


## Noticias, Enlaces y Textos

### El Deseo de cambiarlo todo

- [Judith Butler x Leonor Silvestri](https://www.youtube.com/watch?v=QDAeKhmi5Vs)
- [Monique Wittig x Leonor Silvestri](https://www.youtube.com/watch?v=mNijDhm10sQ)
- [Teoría King Kong](https://es.wikipedia.org/wiki/Teor%C3%ADa_King_Kong)


### Sociales/filosóficas

- [Punto de Emancipación 27- Iñigo Errejón: Actualización de lo Nacional y Popula](https://www.youtube.com/watch?v=A5BojM7s4qg)
- [Entrevista a Franco "Bifo" Berardi por Daniel Tognetti y Diego Sztulwark](https://www.youtube.com/watch?v=JCoJ31FS0ds)
- [Entrevista a Cornelius Castoriadis](https://lobosuelto.com/el-proyecto-de-autonomia-no-es-una-utopia-entrevista-con-cornelius-castoriadis/)
- [MACHINE LEARNING DISABILITIES - Dmytri Kleiner](https://dmytri.surge.sh/disabilities/#/)

### Tecnología

- [Jugando con Señales de RF](https://franc205.medium.com/hacking-rf-for-physical-security-9fe485abd682)
- [Compilando a WASM con llvm](https://til.simonwillison.net/webassembly/compile-to-wasm-llvm-macos)
- [Move over JavaScript: Back-end languages are coming to the front-end](https://github.com/readme/featured/server-side-languages-for-front-end)
- [Entendiendo el archivo shadow](https://www.cyberciti.biz/faq/understanding-etcshadow-file/)

### Cooperativas/Economía Popular

- [Extracto del II Encuentro Nacional Ritep - Red de Intercambio Técnico con la Economía Popular, Charla de Grabois y Grobocopatel](https://youtu.be/setqRXGmPTc?t=6924)
- [Carpoolear en peligro de derrumbe](https://www.instagram.com/p/CcgpnxypjLU/) - Necesitan ayuda para desarrollar
    - [Proyecto](https://www.carpoolear.com.ar) - [Organización](https://www.stsrosario.org.ar)
    - Repos: [Frontend](https://github.com/STS-Rosario/carpoolear), [Backend](https://github.com/STS-Rosario/carpoolear_backend)
