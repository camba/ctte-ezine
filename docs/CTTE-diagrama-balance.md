```
graph LR
Activo["Activo (Bienes y Derechos) = Pasivo + Patrimonio Neto (¿Cómo te financias?)"]
Pasivo[Pasivo]-->title
title[Deudas con 3ros]
title-->Fondos[Fondos Ley 20337]
title-->Proveedores[Proveedores]
title-->Fiscales[Fiscales]
title-->Sociales[Sociales]
Sociales-->Capital[Capital Integrar]
Sociales-->Distri[Distribución de excedentes]

Pn[Patrimonio Neto]-->Asociades["Asociades ( Divisible)"]
Asociades-->Capital1[Capital]
Capital1-->Suscripto[Suscripto - Compromiso de pago]
Capital1-->Integrado[Integrado - Pago $]
Pn-->Coop["Cooperativa (Colectivo Indivisible)"]
Coop-->Reservas[Reservas]
Reservas-->Legal[Legal]
Reservas-->Especial[Especial Art 42 - Excedentes no repartibles]

Resultados[Resultados]
Resultados-->NoRepartible["No Repartible (Reserva Especial)"]
Resultados-->Repartible[Repartible]
Repartible-->R["5% Reserva Legal"]
Repartible-->R1["5% Fondo de promoción Cooperativa"]
Repartible-->R2["5% Fondo Estimulo personal"]
Repartible-->R3["85%"]
R3-->Capitalización
R3-->Excedentes
```

Fuente de diagrama del capacitación de balance, hecho y convertido a pdf con [Mermaid](https://mermaidjs.github.io/)
