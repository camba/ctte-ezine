---
title: Enero 2020
---

# Cambá CTTE Ezine Enero 2020

## Frase

```
En barrio de ricachones,
sin armas, ni rencores,
es solo plata y no
amores
```

Pensado, escrito y dejado luego de haber robado el banco río, por Fernando Araujo.
Pueden leer un libro genial de [Rodolfo Palacios, "Sin Armas ni rencores"](https://www.planetadelibros.com.ar/libro-sin-armas-ni-rencores/191819), sobre este hecho.


## Aportes a proyectos de Software libre

- @damianm y @charlie agregaron funcionalidad a un plugin de moodle, Acá el nuevo fork repositorio [Moodle-filter_syntaxhighligther](https://github.com/Cambalab/moodle-filter_syntaxhighlighter/)

## Propuestas de contribuciones

### Github Stars

- **¿Qué?** Hacer una aplicación que permita automaticamente poner estrellas de github a un conjunto de proyectos previamente curados (ej: los de cambá)
- **¿Cómo?** [1](https://gist.github.com/arkokoley/c86177006a2f2d42f280eacb0a590a30) [2](https://github.com/ffujiawei/auto-stars/blob/master/auto_stars.py) [3](https://developer.github.com/v3/activity/starring/)
- **¿Para que?** Simplemente para que nos resulte más fácil darle estrellas a nuestros proyectos

## Herramientas de software

- [Entropic](https://www.entropic.dev/) - Registro federado de paquetes (npm alternative)
- [Commitzen](http://commitizen.github.io/cz-cli/) - Herramientas que fuerza las convenciones para hacer commits
- [Semantic-Release](https://github.com/semantic-release/semantic-release) - Manejo automatizado de versiones y publicación de paquetes
- [Snowpack](https://www.snowpack.dev/) - Para contruir aplicaciones web con menos herramientas y 10 veces más rápido

## Informes/Encuestas

- [Resultados de encuesta sobre Ezine](CTTE-encuesta-ezine-2019-analisis)
- Capacitaciones de Enero:
  - Jesica Lacquaniti: Los impuestos y los circuitos de compras y ventas de Cambá. [Presentación](./assets/impuestos.pdf)
  - Anita Almada: Cultura Devops. [Presentación](https://nubecita.camba.coop/s/HTpHikttqEXdj58)
  - Leonardo Vaquel, Jesus Laime, Flor : Experiencia de desarrollo con Ionic 4 versión React. [Presentación](./assets/CTTE-capacitacion-traky.pdf)
- [Datos de encuesta StateofJS del 2019](CTTE-texto-stateofjs-2019)
- [Godot: Una decada en retrospectiva y futuro](https://godotengine.org/article/retrospective-and-future)

## Noticias, Enlaces y Textos

### El deseo de cambiarlo todo

- Todos los libros de editorial [POPOVA](https://issuu.com/comunicacion.popova) que es un proyecto de edición de materiales teóricos sobre feminismo, disidencia y masculinidades.
- Video sobre [El Manifiesto de las especies de compañía de Donna Haraway](https://www.youtube.com/watch?v=IvENoVPcimU) -  Sparring filosófico de Leonor Silvestri.

### Más sociales/filosóficos

- @Neto nos comparte un articulo sobre **[Solid Fund](CTTE-texto-solidfund)**, el fondo solidario que utilizaron en el viaje a UK.
- @Belen nos comparte **La Infancia como nuevo humanismo: "Chiqui" González** - [Video](https://www.youtube.com/watch?v=m8u22xLvtHs) [Transcripción](http://chiquigonzalez.com.ar/project/la-infancia-como-nuevo-humanismo-tedx-bs-as-2012/)
- [¿Circulos de estudio para las capacitaciones grupales?](CTTE-texto-circulos-estudio)
- Resumen del libro [Spinoza Disidente](CTTE-texto-spinoza-disidente)

### Más técnicos

- [Como escribir y hacer builds de librerias js](https://medium.com/@kelin2025/so-you-wanna-use-es6-modules-714f48b3a953)
- Sale la versión [v1.0.0](https://aframe.io/blog/aframe-v1.0.0/) de [A-Frame](https://aframe.io/) con soporte WEBXR (AR + VR), Web framework para construir experiencias de realidad virtual
- [Deno: Will replace Node.js?](https://www.youtube.com/watch?v=lcoU9jtsK24) - Bert Belder en dotJS 2019

### Otras

- Entrevista del 2010 de la [Oveja Electrónica](http://ovejafm.dreamhosters.com/) a Jose Masón y [Osiris Gómez](https://osiux.com/) socios de [GCOOP](https://www.gcoop.coop/) sobre cooperativas de software. [Audio](./assets/oveja-gcoop.ogg)

### Seguridad

- [Top 20 de popularidad de las herramientas de hackeo del 2019](https://www.kitploit.com/2019/12/top-20-most-popular-hacking-tools-in.html)
