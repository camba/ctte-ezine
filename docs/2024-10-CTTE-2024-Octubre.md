---
title: Octubre 2024
---

# Cambá CTTE Ezine Octubre 2024

<div style="text-align: center;">

![Sueños](@img/octubre2024.jpg)

</div>

```
DESDE QUE MIRO A LOS ÁRBOLES COMO DEIDADES,
HABLO CON ELLOS CON TRES TOQUES SUAVES,
YA TU SABES… TRES TOQUES SUAVES

FLUYEN SABERES DE LA SAVIA VERDE,
TRONCO Y FOLLAJE, VIENTO SALVAJE,
REFUGIO DE AVES, SABERES DE ABUELOS,
TOCA MADERA YO TOCO LA TIERRA

AGÜITA VEN, QUIERO NACER,
SEMILLA DE ESTA TIERRA,
AL SOL CRECER, GIRAR Y VER,
LA LUNA Y LOS PLANETAS

CANTA Y PLANTA, SIEMBRA LA TIERRA, TOCA MADERA
CANTA Y PLANTA, SIEMBRA LA TIERRA, TOCA MADERA
CANTA Y PLANTA, SIEMBRA LA TIERRA, TOCA MADERA
CANTA Y PLANTA, SIEMBRA LA TIERRA, TOCA MADERA

UN PUÑITO DE HOJITAS FRESCAS,
AGUA, LLUVIA Y CANELA PA CANTARLE A LAS ESTRELLAS
Y A LAS BUENAS AMISTADES QUE TANTO AMOR NOS TRAEN,
ABUELOS ANCESTRALES CON SABERES DE MAR

HOJAS DEL VIENTO, FLORES DEL MONTE,
ÁRBOL DEL RÍO, DANOS TUS DONES,
AGÜITA FRESCA LAVA MI ALMA,
SAVIA DEL ÁRBOL CURA MIS PENAS

Y FLORECER, UN BOSQUE DE ANACAGÜITA Y YERBA,
PENSAR TAL VEZ, DEL VIENTO SER,
BAILAR HASTA QUE AMANEZCA

CANTA Y DANZA,
CANTA Y SUEÑA,
CANTA Y VUELA,
CANTA Y SIEMBRA

PLANTA TU BOSQUE, PLANTA TU VOZ, CANTA Y PLANTA, RIEGA Y ESPERA,
COMO LA SELVA SE REGENERA, COMO SIN HACER NADA LA VIDA SE RENUEVA,
DEJEMOS A LAS NUEVAS GENERACIONES FRUTOS LLENOS DE ESPECIES, DE COLORES,
DE SABORES, DE ÁRBOLES FRUTALES Y DE LIBRES MANANTIALES!.
```

Canción [Canta y Planta](https://www.youtube.com/watch?v=4iE20Lv9pfM) de [Paloma del Cerro](https://cmtv.com.ar/biografia/show.php?bnid=2804), [La Charo](https://es.wikipedia.org/wiki/Charo_Bogar%C3%ADn) y [Sofia Viola](https://www.cmtv.com.ar/biografia/show.php?bnid=2806&banda=Sofia_Viola)

## Cooperativas/Economía Popular
- [Espacio Abierto de Tecnologías Las Chacras: Balance de mitad de año](https://ltc.camba.coop/2024/09/16/espacio-abierto-de-tecnologias-las-chacras-balance-de-mitad-de-ano/)
- [Fábrica de inventos Edición invierno II](https://ltc.camba.coop/2024/09/18/fabrica-de-inventos-edicion-invierno-ii/)
- [Camplight Caso de estudio - Salarios auto establecidos](https://www.youtube.com/watch?app=desktop&v=traU0WtipaI)

## Herramientas de software
- [Pydpainter](https://pydpainter.org/) - Programa para hacer pixelart escrito en python
- [DockerSpy](https://github.com/UndeadSec/DockerSpy)  - Busca en images docker y extrae información sensible como secretos de autenticación, claves privadas, etc
- [Isaiah](https://github.com/will-moss/isaiah) - Clon de lazydocker para utilizar por web para Gestionar tu flota de docker de manera simple.
- [SearXNG](https://github.com/searxng/searxng) - Motor de busqueda de internet con resultados integrados de varios servicios de busqueda y bases de datos.

## Informes/Encuestas/Lanzamientos/Capacitaciones

- [Tor y Tails unen fuerzas](https://blog.torproject.org/tor-tails-join-forces/)
- [38c3 - Se anuncia Congreso del Chaos Computer Club a fin de año](https://www.ccc.de/en/updates/2024/38c3-illegal-instructions)
- [Lenguajes de programación más populares del año](https://www.zdnet.com/article/the-most-popular-programming-languages-in-2024-and-what-that-even-means/)
- [Ranking de lenguajes de programación medidos por eficiencia energética](https://haslab.github.io/SAFER/scp21.pdf)
- [Última Encuesta sobre desarrolladores python](https://lp.jetbrains.com/python-developers-survey-2023/)

## Noticias, Enlaces y Textos

### Tecnología
- [No tenes SIM? No hay Problema](https://www.youtube.com/watch?v=RyirQOCUUK8)
- [DJP](https://simonwillison.net/2024/Sep/25/djp-a-plugin-system-for-django/) - Nuevo mecanismo de plugins para django presentado en la DjagoCon Us 2024
- [Linus Torvalds en Conversación con Dirk Hohndel](https://www.youtube.com/watch?v=OM_8UOPFpqE)
- [Ataques de Replay con javscript y hackrf](https://charliegerard.dev/blog/replay-attacks-javascript-hackrf/)
- [2FA en la línea de comandos](https://www.cyberciti.biz/faq/use-oathtool-linux-command-line-for-2-step-verification-2fa/)

### LLMs
- [Resumen de charla de Enrique Chaparro y Fernando Schapachnik sobre regulación de IA](CTTE-texto-charlas-ia)
- [Hacer un agente con LangGraph](https://www.youtube.com/watch?v=nK9K8UPraXk) - [Código fuente](https://github.com/samwit/agent_tutorials/tree/main/agent_write)
- [¿Cómo construimos UIs?](https://www.youtube.com/watch?v=c5OPn_dyNcc) - [UI Shadcn](https://ui.shadcn.com/)
- [Amica](https://github.com/semperai/amica) y [Open-LLM-vtuber](https://github.com/t41372/Open-LLM-VTuber) - Avatares para interactuar mediante voz con LLMs
