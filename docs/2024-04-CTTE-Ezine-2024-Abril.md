---
title: Abril 2024
---

# Cambá CTTE Ezine Abril 2024

<div style="text-align: center;">

![Libros](@img/abril2024.jpg)

</div>

```
Llueve y el cielo y la tierra se mojan entre sí.
Se atragantan las alcantarillas.
Llueve, llueve y en todos los balcones de Madrid
se está mojando la ropa tendida.

Si llueve la gente se pone a cubierto.
Si llueve, el pasto se pone contento.

Llueve y parece que mañana va a seguir así:
lo asegura Meteorología.
Llueve, llueve y en todos los rincones del país
la tierra está agradecida.

Llueve, la gente se pone a cubierto
y el pasto se pone contento.

Llueve sobre el río.
Llueve sobre el mar.
Llueve y no parece
que vaya a parar...
que vaya a parar.

Llueve y el cielo y la tierra se tocan entre sí.
Se escucha el rugir del aguacero.
Llueve, llueve y en todos los rincones del jardín
se alborotó el hormiguero.

Llueve, la gente se pone a cubierto.
Si llueve, el pasto se pone contento.

Llueve y parece que mañana va a seguir así,
cuando ya ganaba la sequía.
Llueve, llueve y en todos los rincones del país
la tierra está humedecida.

Si llueve la gente se pone a cubierto
y el pasto se pone contento.

Llueve sobre el río.
Llueve sobre el mar.
Llueve y no parece
que vaya a parar.

Llueve sobre el río.
Llueve sobre el mar.
Llueve y no parece
que vaya a parar...
que vaya a parar.

¡Llueve!
¡Llueve!
¡Mira cómo llueve!

¡Llueve!
¡Llueve!
¡Mira cómo llueve!
¡Llueve!

Si llueve la gente se pone a cubierto
y el pasto se pone contento.
```

Letra del tema [Llueve](https://www.youtube.com/watch?v=eV7NCaBftKU) de [Jorge Drexler](https://es.wikipedia.org/wiki/Jorge_Drexler).

## Contribuciones Software libre
- [Typos en Langchain](https://github.com/langchain-ai/langchain/pull/18519)
- [Agregando a PATIO en recopilación de cooperativas tecnológicas](https://github.com/hng/tech-coops/pull/198)


## Herramientas de software
- [Puter](https://github.com/HeyPuter/puter) - El sistema operativo de Internet!
- [Observablehq](https://observablehq.com/framework/) - Generador de sitios estáticos para dashboards, apps, reportes y más.  [Código fuente](https://github.com/observablehq/framework)
- [USBvalve](https://github.com/cecio/USBvalve) - Expose USB activity on the fly
- [Netdata](https://github.com/netdata/netdata) - The open-source observability platform everyone needs!

## Informes/Encuestas/Lanzamientos/Capacitaciones
- [¡Lanzamos HUAYRA LINUX 6.5!](https://huayra.educar.gob.ar/)
- [Archivo de Telam](https://archivo-pirata-antifascista.partidopirata.com.ar/2024/03/04/archivo-de-telam-com-ar.html)
- [Webtunnels bridges](https://www.bleepingcomputer.com/news/security/tors-new-webtunnel-bridges-mimic-https-traffic-to-evade-censorship/) - Nueva forma de evadir censura de Tor
- ["Antes que nada hacelo, luego hacelo bien, luego hacelo mejor"](https://www.linkedin.com/posts/addyosmani_softwareengineering-productivity-motivation-activity-7049625987721957377-CQRt/)

## Noticias, Enlaces y Textos

### Sociales/filosóficas
- [¿CONTRA la TECNOLOGÍA? | Cuestión de CONTROL o LIBERTAD](https://www.youtube.com/watch?v=e3S37k_cjvo)
- [PENSAMIENTO LIBERTARIO: Propuestas económicas del anarquismo clásico: MUTUALISMO](https://www.youtube.com/watch?v=a2f7DG_fT8o)
- Algunos números: [Pareto](https://es.wikipedia.org/wiki/Principio_de_Pareto) - [Dunbar](https://es.wikipedia.org/wiki/N%C3%BAmero_de_Dunbar)
- [Hackea el sistema! Los grupos anarquistas de hacking](https://hispagatos.org/post/historiadegruposanarquista/)

### Tecnología
- [Node.js: El documental | Origened](https://www.youtube.com/watch?v=LB8KwiiUGy0)
- [El origen de GIT](https://www.youtube.com/watch?v=s5YMdrj6gGc) - [Leer](https://github.com/git/git/blob/master/README.md)
- [Linus Torvalds: Impacto de la IA en la programación](https://www.youtube.com/watch?v=VHHT6W-N0ak)
- [Fuckoffgoggle](https://fuckoffgoogle.de/) - No dejes que tome el control de tu vida y espacio.

### LLMs

- Tutoriales de Langchain [Repo](https://github.com/justingrammens/LetsLearnLangChain) - [Video Tutorial](https://www.youtube.com/watch?v=QT3wALFDZBo)
- Lanzamiento de LLM de X, [Grok-1](https://github.com/xai-org/grok-1)
- [Yann Lecun: Meta IA, Open Source, Limites de los LLMs, AGI & el futuro de la IA](https://www.youtube.com/watch?v=5t1vTLU7s40)
- [Open Interpreter 01 Lite - El primer dispositivo Asistente personal de IA](https://www.youtube.com/watch?v=Q_p82HtBqoc) - [Código fuente](https://github.com/OpenInterpreter/open-interpreter) - [Web](https://www.openinterpreter.com/01)
