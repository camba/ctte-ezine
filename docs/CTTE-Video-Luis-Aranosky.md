# Entrevista a Luis Aranosky en Hacedores del Aires - Radio Atomika 08/03/19

> [Video](https://www.youtube.com/watch?v=eMHlr6xYfzU)

- Generación que quieren a lo  inmediato que se entrenan para estar ya en los medios masivos (artistas)
- También se han ido creando islas en donde se van formando la propia manera de ver la vida, de hacer las cosas
- ¿Que no se puede hacer en radio? No se puede no pensar, no se puede no decir, no se puede no entender, no se puede no vivir
- El artista se va haciendo en conjunto con su vida. En la medida que tenes una vida interna propia, a partir de que la vida no hace de vos, sino que vos haces tu vida, tenes la tela para expresarla.
- Existen las radios [triac](https://www.facebook.com/FM-triac-233187380053484/) y [tinku](https://radiotinku.blogspot.com/) en traslasierra
- Pienso, produzco, propongo y estoy presente

