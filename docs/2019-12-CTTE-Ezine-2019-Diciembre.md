---
title: Diciembre 2019
---

# Cambá CTTE Ezine Diciembre 2019

## Tema

[Juguetes Perdidos](https://www.youtube.com/watch?v=IFOeXbB-g4Y)
Patricio rey y sus redonditos de ricota

```
Banderas en tu corazón,
yo quiero verlas!
ondeando, luzca el sol o no
Banderas rojas! Banderas negras!
de lienzo blanco en tu corazón.

Perfume al filo del dolor,
así, invisible
licor venéreo del amor
que está en las pieles,
sedas de sedas
que guarda nombres en tu corazón.

Son pájaros de la noche
que oímos cantar y nunca vemos.
Cuando el granizo golpeó,
la campana sonó,
despertó sus tristezas atronando sus nidos.

Esperando allí nomás,
en el camino,
la bella señora está desencarnada.
Cuando la noche es más oscura
se viene el día en tu corazón.

Estás cambiando más que yo.
Yira! Yira! Yira!
Asusta un poco verte así.
Yira! Yira! Yira!
Cuanto más alto trepa el monito
(así es la vida) el culo más se le ve.

Yo sé que no puedo darte
algo más que un par de promesas...
ticks de la revolución
implacable rocanrol
y un par de sienes ardientes
que son todo el tesoro.

Tan veloces son!
Como borrones (así, veloces)
hundiendo el acelerador,
atragantados por los licores,
soplando brasas en tu corazón.

Vas a robarle el gorro al diablo, así,
adorándolo como quiere él, engañándolo.
Sin tus banderas
sedas de sedas
que guardan nombres en tu corazón.
Este asunto está ahora y para siempre en tus manos, nene
oh - oh - oh -
Por primera vez vas a robar algo más que puta guita

Cuando la noche es más oscura
se viene el día en tu corazón.
Sin ese diablo que mea en todas partes
y en ningún lado hace espuma.
```

**Como cierre del año, Dedicada de corazón de CTTE para toda Cambá**


## Aportes a proyectos de Software libre

- Repo del Equipo de Cambá ganador en Juslab. [Juslab](https://github.com/Cambalab/juslab)
- Nuevo proyecto libre de Fiqus [Coophub](http://coophub.io/)
- [PR de @Santi](https://github.com/fiqus/coophub/pull/32) colaborando con código en el proyecto Coophub de Fiqus
- Nueva versión de [ra-data-feathers](https://github.com/josx/ra-data-feathers/releases/tag/v2.5.0)


## Informes/Encuestas

- [Manual para saber como encarar una entrevista técnica de trabajo](https://github.com/yangshun/tech-interview-handbook)
- ¿Dondé se muda neto? [Croquis](./img/casa_de_neto.png) - [Zona](https://www.openstreetmap.org/#map=17/-32.24214/-65.03584)

## Noticias, Enlaces y Textos

### El deseo de cambiarlo todo

- [Quien quiere género pudiendo tener sexo - Leonor Silvestri](https://www.youtube.com/watch?v=yLMtUKkXFHI)
- [BiotransLab: El laboratorio ginecológico casero](https://www.youtube.com/watch?v=tjHomBM5deU) - [Paulapin](http://paulapin.net)
- [Gynepunk](https://www.youtube.com/watch?v=JpIx93p0pfY) [tumblr](https://gynepunk.tumblr.com/) [riseup](https://crabgrass.riseup.net/gynepunklab)

### Más sociales/filosóficos

- [¿Por qué hacer evaluaciones colectivas?](CTTE-texto-evaluaciones-colectivas)
- [Documento Definición y líneas estratégicas de facttic](./assets/Definicion y Linieas Estrategicas- FACTTIC-V2.pdf)
- Informe sobre plenario de 6 y 7 de diciembre de FACTTIC
- [Bombozila](CTTE-texto-bombozilla)
- [The Rise Of Open-Source Software](https://www.youtube.com/watch?v=SpeDK1TPbew)

### Más técnicos

- [React: Concurrent mode and suspense](https://reactjs.org/blog/2019/11/06/building-great-user-experiences-with-concurrent-mode-and-suspense.html)
- [Vuejs - Suspense](https://vueschool.io/articles/vuejs-tutorials/suspense-new-feature-in-vue-3/)
- [Las mejores práctica en Node](https://github.com/goldbergyoni/nodebestpractices)
- [Taller mecánico de Teslas](https://www.youtube.com/watch?v=3Ytm_GnTkl0)
- Disco de RAM (Muy útil cuando queremos usar espacio como si fuera en disco pero usando de soporte solo la RAM)
Las diferencias son muy grandes, ej usando un disco de 8gb (45seg contra 3seg y 190 mb/s contra 3.2 gb/s)
```
mkdir -p /mnt/ram
mount -t tmpfs tmpfs /mnt/ram -o size=8192M
```
- [¿Como hizo la comisión de verano para no saber los nombres que le tocaban a cada persona?](CTTE-texto-comision-verano)
- [Hacer un tetris desde 0 con JS](https://medium.com/@michael.karen/learning-modern-javascript-with-tetris-92d532bcd057)

### Otras

- Primicia versión beta del audiovisual de LTC, [Ver](https://www.youtube.com/watch?v=A-_EHKfWC5Y)
- Fondos de pantalla de Cambá - Artista @Santi - [Descargar](http://nubecita.camba.coop/s/3nDoSPJZMMZbgXe)
- Luego de *10 años y un mes*, recuerdo, busco y comparto este programa de radio en [Oveja Electrónica](http://ovejafm.dreamhosters.com/) en donde el Payaso **Neto** habla sobre Software Libre. [AUDIO](./assets/jue19112009-neto.ogg)
- Nuevo nodo de la red del trueque en Ameghino en Bernal - [Info](http://redglobaldetrueque.blogspot.com/2019/11/inauguracion-del-nodo-ameghino-en-bernal.html)
