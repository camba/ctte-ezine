# ¿Por qué hacer evaluaciones colectivas?

> Leído y traducido de [Why Do Collective Evaluations - and How?](https://institute.coop/sites/default/files/resources/Evaluations-Why-and-How.pdf) del [Democracy at work Institute](https://institute.coop/)

## Articulo

La ausencia de personas que supervisen, quienes son lo encargadas de rápidamente identificar y resolver problemas de rendimiento, convierten en un punto critico para el colevtivo que se realicen periodicamente evaluaciones de trabajadorxs. La ausencia de esas evaluaciones genera que las comportamientos o conductas controversiales se vaya acumulando hasta que estallan; alguien finalmente se cansa de la situación y lo pone en la agenda de colectio para discutir (quizás luego de meses y meses de callarse las quejas y generando también un clima de tensión público); desafortunadamente, cuando el colectivo focaliza en el problema, el hecho ha llegado a convertirse en algo profundo, arraigado y con una carga emocional que es díficil de remediar. Uno de los objetivos de las evaluaciones periodicas es identificar y poner sobre la mesa problemas antes de que lleguen a arraigarse o que resulten en estallidos de ira y resentimiento.
Otro objetivo es dar afirmaciones a las personas sobre su buen rendimiento. Nuevamente, esto es particularmente importante por la ausencia de personas supervisoras que (bajo las mejores cirscunstancias) que  ven como parte de su trabajo castigar y recompensar el buen rendimiento. Habitualmente en un colectivo, las trabajadoras no se sienten empoderadas para charlar sobre el buen rendimiento de otras (¿Quien soy yo para juzgar?) o, al menos no encuentran formas para elogiarlas. La ausencia de feedback puede dejar a las miembras del colectivo sentirse  no apreciadas o inseguras.

Finalmente, la evaluación puede documentar el progreso de las trabajadoras y los problemas mientras identificamos necesidades para la posterior contención y capacitación. Los sistemas más comunes de evaluación usados en los colectivos es lo que se llamaría el modelo de la "reunión general", la trabajadora es evaluada en una reunión por todas las compañeras. Este modelo apela a la simpleza y al igualitarismo (todas, teoricamente, tienen un mismo rol).

Este modelo no es tan simple como parece, y debido a eso no es muy efectivo. Las evaluaciones no pueden simplemente liberarse a la voluntad general, al menos se deberia tener la responsabilizar para organizar las evaluaciones en el calendario y delegarla a un individuo o un pequeño comite. Para una evaluación efectiva, me atreveria a decir que es necesaria mucha preparación.



