# Resumen - [La salvación de lo bello](https://www.youtube.com/watch?v=hBRGOaBsZno)

> Resumen de La salvación de lo bello, libro escrito por el filósofo coreano [Byung-Chul Han](https://es.wikipedia.org/wiki/Byung-Chul_Han). Aquí enlace a video resumen [1](https://www.youtube.com/watch?v=hBRGOaBsZno) por Claudio Alvarez Teran.

## ¿Cómo se entiende el concepto de belleza actualmente?

- Lo púlido
- Lo pulcro
- Lo liso

## Positividad de lo que no daña

- La estética de la no resistencia
- De las superficies
 - Limpia
 - Satinada
- De la comunicación
 - Mensajes de complacencia
 - Mensajes Positivo (el me gusta)

## Simple, relajado, vanal

- Nada para interpretar
- Nada que descifrar
- Nada que pensar
- Vaciado de profundidad

## Resultado

- Las superficies púlidas activan el deseo por lo táctil
- El tacto desmitifica la relación con lo concreto

## En contraposición [Gadamer](https://es.wikipedia.org/wiki/Hans-Georg_Gadamer) dice

- Lo escencial del arte es la negatividad
- Requiere de Cuestionar, interpelar, sacudir
- Agradar vs. Herir

## ¿Cómo es la Aceleración?

- Sin fricción
- Sin resistencia de lo otro, lo distinto
- Comunicación de lo igual
- Más Consumible

## La selfie

- Rostros vacíos de mundo
- Se pierde el contexto
- Se pierde la interioridad

## [Roland Barthes](https://es.wikipedia.org/wiki/Roland_Barthes)

- **punctum:** Daña, estremece, hiere, esconde (no inmediato, afectivo, inconciente)
- **studium:** provoca el me gusta (no despierta ni amor, ni pasión)

## Calocracia (imperio de la belleza)

- Vuelve absoluto a lo liso y lo púlido
- Termina eliminando a lo bello
- Genera imperativos
- Botox, bulimia, cirugías, dietas...

## Consumo

- Excitación vs. contemplar/reflexión
- La constancia y lo durable van en contra del consumo, se excluyen
- El sujeto ideal es:
  - Sin carácter
  - Sin firmeza
  - Sin constancia

## Big data

- La teoría reemplazada por los datos es el fin de la verdad
- Son de utilidad pero no generan conocimiento, no generan verdad

## [Aristóteles](https://es.wikipedia.org/wiki/Arist%C3%B3teles)

- Es libre quien es independiente de las precariedades de la vida y sus imperativos
- Se expresa en 3 formas en confluencia con lo bello:
  - La vida enfocada en el disfrute de las cosas bellas
  - La vida que produce actos bellos en la polis
  - La vida Contemplativa es la belleza perfecta

## Lo bello

- Invita a la demora
- A suspender el deseo e interes
- La ausencia de deseo detiene el tiempo
- Es acontecimiento, y descrubre más tarde a la vista de otre

