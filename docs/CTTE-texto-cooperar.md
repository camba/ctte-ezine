# Programa de Innovación y Autogestión en TIC

> Entidad responsable de la propuesta: Federación Argentina de Cooperativas de Trabajo de Tecnología, Innovación y Conocimiento Ltda. [(FACTTIC)](https://facttic.org.ar/)

**Ocho claves** para generar políticas públicas que generen un sector TIC independiente y competitivo a nivel nacional e internacional.


1) **Sin apropiación tecnológica, no hay posibilidad de desarrollo - Soberanía tecnológica para software y hardware**

La soberanía, o independencia, tecnológica es la capacidad que tiene un país o una organización de apropiarse de sus herramientas de tecnología de la información. Sin apropiación tecnológica, no hay posibilidad de desarrollo; y sin desarrollo local no hay futuro.

Es necesario apostar a políticas públicas que apunten a la apropiación de las herramientas informáticas. La principal estrategia para lograr este objetivo es la difusión del uso y desarrollo del Software Libre y el apoyo de integraciones libres en mercados verticales. De este modo se potencian las posibilidades de todas las industrias nacionales, no sólo las ligadas a a tecnología.

En este escenario, las personas podrán apropiarse de esas herramientas y saber cómo están hechas, no comprarlas y usarlas. Podrán modificarlas para adaptarlas a sus necesidades y gestionar de manera independiente y soberana la infraestructura tecnológica, ya sean las computadoras como los programas (Software y Hardware).

2) **Los datos de los argentinos, en manos de los argentinos - Soberanía de datos**

Soberanía de datos significa que es fundamental tener control y acceso al lugar, virtual y real, en el que guardamos nuestra información, personal, laboral, de todo tipo. En nuestro caso, eso significa que debe estar resguardada en territorio argentino y bajo la ley argentina. Lo contrario, significa que otros países pueden entrometerse en nuestra política interna, y que, por ende, nuestra información esté a merced de leyes de otros países.

En este sentido son necesarias políticas de mejora de la conectividad a nivel país, especialmente en manos de las entidades de la Economía Social y Solidaria y de las PYMEs, con apoyos concretos a las políticas de almacenamiento local, es decir, la generación de servidores nacionales.

Para lograr soberanía de datos es necesario contar con muchos espacios de guardado de información, servidores y “nubes” argentinos. Porque no queremos ser más blanco de la vigilancia global, los datos de los argentinos, en manos de los argentinos.

3) **La tecnología en manos del cooperativismo resuelve necesidades reales de la gente - Democratización del mercado de tecnología**

El nivel de concentración en las empresas de tecnología es brutal y el desarrollo de estas compañías está más ligado a la especulación financiera que al desarrollo de tecnología. Esto provoca que la innovación y el desarrollo de software termine siendo una gran "timba" de proyectos que tiene, como principal objetivo, la concentración de capital.

Las políticas públicas debieran tender a alentar la creación de entidades de la economía social, con características diferenciadas que apunten a darle lugar a la diversidad, de visiones y objetivos.

La tecnología en manos de la economía social, del cooperativismo, tiene
como fin resolver necesidades reales de la gente, ya que las conoce y tiene las herramientas para incidir de modo concreto en ellas.

4) **La programación no es cosa sólo de programadores - No a la matriculación obligatoria**

Creemos que el trabajo de desarrollo de software es interdisciplinario. La programación no es un tema que sólo desarrolle un grupo de "iluminados", sino que se basa en los conocimientos, las habilidades y los recursos de trabajadores con distintos saberes y experiencias.

Son necesarias políticas que alienten que más personas decidan acercarse ala programación y hacer de ello su trabajo.

Circunscribir esta tarea sólo a los que pueden matricularse genera una barrera artificial para trabajar en el rubro y desalienta la participación de más personas.

5) **El voto electrónico no garantiza que sea seguro y secreto a la vez - No al voto electrónico.**

A partir de numerosa evidencia científica y de la opinión de profesionales renombrados de todo el mundo, se sabe que no es posible hacer un sistema de voto electrónico que sea íntegro y auditable y que a la vez garantice el secreto del voto.

Las elecciones populares son uno de los pilares de la democracia y no tiene sentido ponerlas en riesgo a partir de una necesidad de marketing politico.

6) **Internet expone la intimidad de las personas - Resguardo del anonimato y privacidad**

La información personal que queda en la red al navegar en ella es mucho más grande de lo que imaginamos. Es un derecho de las personas tener control sobre eso, a la vez que conocer cómo resguardar nuestros datos.

Son necesarias políticas educativas que generen conciencia en las personas sobre los riesgos a los que se exponen al ejercer ciertas prácticas en la red, y a su vez, informen qué recaudos se deben tomar para evitarlos.

Es cada vez más importante y necesario resguardar la esfera de lo privado e íntimo de cada persona, y creemos que garantizar el anonimato y la privacidad es un deber indelegable del Estado.

7) **No a los proyectos que buscan cerrar las libertades que Internet permitió - Neutralidad de la red**

La Internet que supimos conseguir y que hoy habitamos es como es por la lucha de muchos y muchas que buscaron desde el principio mantenerla como un espacio de intercambio libre. Hoy hay intereses empresariales y comerciales que influyen en el tráfico de la red, haciéndolo más lento o más rápido; más caro o gratuito, en función de los sitios o plataformas que se quiere favorecer o perjudicar. Rechazamos los proyectos que buscan cerrar las libertades que Internet permitió en sus primeros años de vida. Y trabajamos por seguir abriéndola aun más. Pedimos la neutralidad de la red, en eso sí que no somos neutrales.

8) **El saber nos hace más libres - Alfabetización digital ciudadana**

Se trata de que todos los ciudadanos, independientemente de cuándo hayan nacido, qué posibilidades tuvieron en su trayectoria de vida, o con qué recursos personales cuentan, puedan entender, evaluar y analizar información utilizando tecnología digital. Que comprendan cómo trabaja la tecnología hoy y cómo puede ser utilizada correctamente.

Es necesario desde el Estado que se implementen políticas públicas que generen espacios donde las personas puedan capacitarse en estos temas.

Las personas digitalmente alfabetizadas pueden comunicarse y trabajar más eficientemente. El saber nos hace más libres y nos brinda nuevas herramientas para construir una mejor ciudadanía.
