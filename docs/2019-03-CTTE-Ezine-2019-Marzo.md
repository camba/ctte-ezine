---
title: Marzo 2019
---

# Cambá CTTE Ezine Marzo 2019

## [¿Qué es un Hacker?](http://www.interorganic.com.ar/josx/llaneros.htm)

```
De los chicos rebeldes
que no van a crecer
hay que ocuparse.
De las chicas rebeldes
que no van a sentar cabeza
hay que ocurparse.

Un mamporro en la cabeza
es lo que conseguís si no
preguntás

y un mamporro en la cabeza
es lo que conseguis si
preguntás

La barbarie empieza por casa.
```

Traducción de canción ["Barbarism Begins at home"](https://www.youtube.com/watch?v=oEOpUqpETcU) - *Morrisey, The Smiths*


## Aportes a proyectos de Software libre

- *Santiago Botta* hizo el Release inicial de [Fake data generator](https://github.com/Cambalab/fake-data-generator)
- *Santiago Botta* sigue trabajando en avances en [Vue-Admin](https://github.com/Cambalab/vue-admin), ahora le toco a la autenticación.
- Mergearon el código *Scroll3D Transition Mode* hecho por  *Juan Manuel Rodriguez Defago* e hicieron nuevo release del software [nuka-carousel](https://github.com/FormidableLabs/nuka-carousel/pull/506)
- Mergeraon el código *TextInput: Add icon to right or left* hecho por *José Luis Di Biase* en el proyecto [Aragon-ui](https://github.com/aragon/aragon-ui/pull/265)
- Ex asociadoo de Cambá *Adrián Pardini* lanzá [parser para protocolo LX200 escrito en python](https://github.com/telescopio-montemayor/python-lx200)

## Herramientas de software

> Conjuntos de herramientas de software que sirvan para comprender y/o sean consideradas interesantes para nuestra cooperativa.

### Novedades

- [WebAuthN](https://www.w3.org/2019/03/pressrelease-webauthn-rec.html): Web Standard para logins seguros y sin contraseña
- [Hamstergame](https://github.com/MauriBian/HamsterGame) <br>
  Juego hecho con godot3, lmms y musescore creado por los hermanos Bianchi (al menos uno alumno de la tecnicatura en la UNQ)

## Informes/Encuestas

- [State of Vue 2019](https://cdn2.hubspot.net/hubfs/1667658/State_of_vue/State%20of%20Vue.js%202019.pdf)<br>
  Agregar Vue.js al tu stack:
  - *La razón más importante:* Vue.js is pretty easy to start with **59%**
  - *Duda más imporante:* Carencia de experiencia entre los empleados **49%**, Incertidumbre acerca de su futuro **36%**
  - *Venatja más grande:* *fácil integración*: **76%**, Buena documentación **75%**, Performance **51%** (voto múltiples)

- Resumén de [Encuesta Cambá capacitación 2019](CTTE-Encuesta-capacitacion)

## Noticias, Enlaces y Textos

> Conjunto de información que sirvan para comprender y/o sean considerados interesantes para nuestra cooperativa.

### Más sociales

- [Contributor covenant](https://www.contributor-covenant.org/) creado [Coraline Ada Ehmke](https://en.wikipedia.org/wiki/Coraline_Ada_Ehmke) ([Español](https://www.contributor-covenant.org/es/version/1/4/code-of-conduct.txt)) <br>
  Código de conducta o Convenio de contribuyentes que adoptaron muchos proyectos libres. [Code of condcut del kernel de linux](https://kernel.googlesource.com/pub/scm/linux/kernel/git/torvalds/linux.git/+/refs/tags/v5.0/Documentation/process/code-of-conduct.rst) y su [interpretacion](https://kernel.googlesource.com/pub/scm/linux/kernel/git/torvalds/linux.git/+/refs/tags/v5.0/Documentation/process/code-of-conduct-interpretation.rst)

- [Liderando una empresa sin jefes - Jorge Silva](https://www.youtube.com/watch?v=fMfn6sCpEcw) ([Resumen](CTTE-texto-10pines))
- [Entrevista Fantino/Sztajnszrajber](CTTE-entrevista-dario)
- **Sol Verniers** participó con su opinión en la Revista Acción *Críticas al algoritmo*, [Interrogantes que plantea](CTTE-revista-accion-algo)

### Otros

- [Convence a tu jefe](https://paper.dropbox.com/doc/Convince-Your-Boss-VueConfUS-2019-OnsTp8wwJZoGRFR4ZEu3x) <br>
  Texto para convencer a tu jefe para ir a un evento, en este caso uno de Vue. Interesante para utilizar para otros fines.
  inspirado en [este otro texto](https://smashingconf.com/sf-2019/convince-your-boss)
- [Inventos de steve johnson](https://codex.core77.com/users/Steven_Johnson)
- [Martelli Open](https://www.worldcubeassociation.org/competitions/MartelliOpenOtono2019) - Torneo de cubo de rubik en Buenos Aires
- **Neto** si vas a Alemania trae [club-mate](https://en.wikipedia.org/wiki/Club-Mate) - *la bebida de los hackers*


## Dossier Especial - Mujeres en tecnología

- [Mujeres Argentinas en Ciencia, Tecnología, Ingeniería, Arte y Matemáticas](https://proyectos.chicasentecnologia.org/mujeresensteam/)
- [What if Women Built the Internet?](https://irlpodcast.org/season4/episode7/)

### Grupos que apoyan conferencias de Vue (Mujeres)

- [Vuevixens](https://www.vuevixens.org/) <br>
  Vue Vixens are foxy people who identify as women and who want to learn Vue.js to make websites and mobile apps.
  En Argentina "la lider" es [Giselle Romina Perez](https://github.com/GisellePerez)

- [Womenwhocode](https://www.womenwhocode.com/) <br>
  Por el momento no hay capitulo local (Argentina) de esta organización

### Algunos grupos Locales

- [Chicas Programando Args](https://github.com/chicasprogramando?tab=followers) <br>
  Se podrian hacer busquedas dirigidas para busqueda de trabajo
- [Django girls Buenos Aires](https://djangogirls.org/buenosaires/)
- [Linuxchixar](http://linuxchixar.org/)
- [Chicas en tecnologia](https://www.chicasentecnologia.org/sobre)
- [Las de sistemas](https://medium.com/lasdesistemas)
- [ADA ITW](https://www.linkedin.com/company/ada-itw/) [Maria celeste Medina](https://anitab.org/profiles/abie-award-winners/maria-celeste-medina/) [1](https://www.linkedin.com/in/mariacelestemedina/)

## Propuestas de contribuciones a proyectos

> La idea es mensualmente publicar un conjunto de propuestas para colaborar en el tiempo libre, reutilizar en trabajo de la universidad, etc.

Los proyectos libres que venimos haciendo siempre necesitan y tienen issues en los que se puede colaborar y aprender. Pueden también invitar a sus conocidos/amigos de la universidad u otros lares.

- [ns-vue-radio](https://github.com/Cambalab/ns-vue-radio)
- [vue-admin](https://github.com/Cambalab/vue-admin)
- [fake-data-generator](https://github.com/Cambalab/fake-data-generator)
- [vue-dynamic-table](https://github.com/Cambalab/vue-dynamic-table)
- [proyectos adopteitor](https://github.com/adopteitor)
- [ra-data-feathers](https://github.com/josx/ra-data-feathers)
- [Aplicativos libres](https://github.com/sgobotta/aplicativos-libres-client)
