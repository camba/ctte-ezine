---
title: Noviembre 2024
---

# Cambá CTTE Ezine Noviembre 2024

<div style="text-align: center;">

![Día de los muertos](@img/noviembre2024.png)

</div>

```
Voy caminando por las calles
sin rumbo y sin lugar
No hay trabajo y el ajuste
No parece aflojar

Este sistema está fallado
Tenemos que reaccionar
Una esperanza en que algo cambie
Quiero poder salvar

Para vivir hoy necesito
El mundo digital
Para estudiar, para pensar
Y también para jugar

Somos cibercirujas
combatimos la obsolescencia
Reciclando circuitos
despertando conciencias

Puede tener vida nueva
Esa vieja computadora
Usando software libre
Sin tecnología represora

Antes de desechar
Tenés que reflexionar
Si lo podés rescatar
una vida puede cambiar

Somos cibercirujas
combatimos la obsolescencia
Reciclando circuitos
despertando conciencias
```

Canción [Somos Cybercirujas](https://www.youtube.com/watch?v=H-rM23qKqfw) de [Uctumi](https://www.uctumi.com/) con [Tablartura](https://www.uctumi.com/index.php?option=com_content&view=article&id=200:somos-cybercirujas&catid=9&lang=es&Itemid=118)

## Contribuciones software libre
- Mínima mejora de descripción - [PR a LangFuse](https://github.com/langfuse/langfuse/pull/3980)

## Cooperativas/Economía Popular
- [Cierre de ReinventarTec 5ta Edición - Cooperativismo y Tecnología](https://www.youtube.com/live/GPkudlxxzQs)
- [La pelicula sobre "Los pioneros de Rochdale"](https://www.youtube.com/watch?v=j7ArZJrY7Wg)
- [Cambá expone su visión sobre la Inteligencia Artificial](https://blog.camba.coop/inteligencia-artificial/)
- La [red FOQSI (Fibra Óptica de Quintana y San Isidro)](https://altermundi.net/2024/10/15/foqsi/) , ha comenzado su despliegue para crear una red que sea accesible, estable y orientada al a comunidad.
- [Lugar cooperativo esquina owen y rochdale](https://nominatim.openstreetmap.org/ui/reverse.html?format=html&lat=-34.647530&lon=-58.388429&zoom=)

## Herramientas de software
- [Mise](https://github.com/jdx/mise) - Herramienta para desarrolladores, variables de ambiente, gestionar tareas
- [Waveform-Playlist](https://github.com/naomiaro/waveform-playlist) - Editor y reproductor web de audio Multitrack.
- [Kamal deploy](https://kamal-deploy.org/) - Desplegar aplicaciones web en caulquier lugar
- [Formbricks](https://github.com/formbricks/formbricks) - Plataforma de encuestas


## Informes/Encuestas/Lanzamientos/Capacitaciones
- [Ganadores del js13kgames](https://js13kgames.com/2024/blog/winners-announced) - [Juego Ganador 13th Floor](https://github.com/js13kGames/13th-floor)
- [Lanzamiento Beta de Compilador de REACT](https://react.dev/blog/2024/10/21/react-compiler-beta-release)
- [Radar de tecnología Octubre 2024](https://www.thoughtworks.com/content/dam/thoughtworks/documents/radar/2024/10/tr_technology_radar_vol_31_es.pdf)
- [Resultados de flahparty](https://flashparty.ar/images/results_fp24.png) - [Stream del Evento](https://www.twitch.tv/videos/2269049688)
- [Estandar SQL 2023](https://peter.eisentraut.org/blog/2023/04/04/sql-2023-is-finished-here-is-whats-new)
## Noticias, Enlaces y Textos

### Tecnología
- [¿Cómo crear una intro de 4k? Experiencia del 1er Puesto de Flashparty](https://0x705h.com/demoscene/2024/10/14/iSS-4k-intro.html)
- [DEF CON 32 - Tu Asistente de AI tiene una gran boca: Un nuevo ataque de canal lateral - Yisroel Mirsky](https://www.youtube.com/watch?v=I1RqhGGRmHY) - Repos [Keylogger](https://github.com/royweiss1/GPT_Keylogger) y [UltraChat](https://github.com/thunlp/UltraChat)
- [Contracultura Maker con Roni Bandini en Nerdearla 10mo aniversario](https://www.youtube.com/watch?v=Z3RgJ25l9YQ) - [Repo](https://github.com/ronibandini/)
- [Documental - Debian "The Distro"](https://www.youtube.com/watch?v=r_uBslaBN88)
- [Entrevista con Simon Willison (Co-Creador de Django) - Herramientas de IA para ingenieros de software engineers, pero sin el hype ](https://www.youtube.com/watch?v=uRuLgar5XZw)

### LLMs
- [Definición de **OPEN SOURCE AI** versión RC1](https://opensource.org/blog/the-open-source-ai-definition-v-1-0-rc1-is-available-for-comments)
- [Deconstruyendo los Transformers: La Matemática detrás de ChatGPT - Lautaro Borrovinsky](https://www.youtube.com/watch?v=y_QyHfJLm6E)
- [Ariel Vercelli: Entrevista en Radio Con Vos Patagonia: regulación de las inteligencias artificiales](https://arielvercelli.org/2024/10/07/entrevista-en-radio-con-vos-patagonia-regulacion-de-las-inteligencias-artificiales/)
- [Documento sobre IA Publica de Mozilla](https://assets.mofoprod.net/network/documents/Public_AI_Mozilla.pdf)
