# Isthmus - engineering & Manufacturing

Este texto es un breve resumen de alguno punto interesante del [video](https://youtu.be/NrYzieaXiwY) que esta basado en un [caso de estudio](https://www.smu.ca/webfiles/IsthmusEngineeringCS202107.pdf) sobre gobernanza en cooperativas de trabajo. [más info](https://geo.coop/articles/isthmus-engineering-case-study-worker-co-op-governance).

## Asamblea
> Board of Directors (todos los dueños/(socies) - Reuniones 2 veces por mes (1hs)
- Hace todas las decisiones administrativas (a quien contratar, a quien hechas, si se hace un nuevo edificio)
- No se hacen decisiones sobre los proyectos
- Aprueba los proyectos y crea los comite adhooc

## Consejo de administración
> Executive Committee (Elegido Anualmente)
- Presidente y Tesorera (habitualmente duran más tiempo en el cargo, porque se necesita mucho tiempo para aprender)
- Presidente es un facilitador
  - No hace decisiones
  - Se encarga de que las decisiones se lleven a acabo.
  - Pone la agenda
  - Determina el calendario de reuniones
  - Facilitador de las asambleas y de las reuniones de comittees.
- Vice Presidente
  - Persigue gente para que la gente este presente en las reuniones
  - Seguimiento de tareas de la asamblea (Active o completada)
  - Seguimiento a comittee

## Sobre decisiones
Es clave para decisiones en asambleas:
- Entrenamiento profesional en PM
- Hablar con otras coops
- Experiencia en que funciona y que no
- Ir a conferencias de co-op

## Comisiones/Comites
- Todo el trabajo que no es de proyectos lo hacen los committees (se reestructuran anualmente, pero se aseguran que esten las personas necesarias en cada cual)
- Se crean con objetivos claros y entregas claras (scope, deliverables) para la Asamblea
- Trabajan para la asamblea

## Modelo de ingreso
- Son empleados durante los 1eros dos años y luego pueden aplicar a ser dueños.
    - 80% de votos para ser miembro,
    - 50% de votos para echar a alguien

## Proyectos
- Existe Scheduling Comittee (Asigna personas a proyectos)
- Cada proyecto tiene trabajardores responsables de terminar el trabajo

## Restricciones
- El comercial o el gestor general no puede ser parte de la asamblea, es un empleado

## Otras
- Horas totales que utilizan son: 70% en producción y 30% es gestión
- 50% de los trabajadores son dueños
- Ofrecen lo mismos en dinero que el mercado
- Algunas personas necesitan ser gestionadas
- Algunas personas no quieren las responsabilidades de un negocio
