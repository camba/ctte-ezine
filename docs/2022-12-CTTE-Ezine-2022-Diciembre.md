---
title: Diciembre 2022
---

# Cambá CTTE Ezine Diciembre 2022

<div style="text- align: center;">

![Pecesycasitas](@img/pecesycasitas-zine.jpg)

</div>

```
- No ofenderse con el que piensa distinto y abrazar a quien nos contradice
- No hablar para convencer sino para disfrutar. Apreciar el ejercicio de razonamiento
- Hablar desde la voz propia y no de una repetición enciclopédica de citas
- Dudar de uno mismo y recordar que siempre podemos estar equivocados
- Usar la conersación como un espacio vital para juzgar nuestras propias ideas
- Valorar las ideas solo por el impacto que causan cuando las ponemos en práctica, igual que respetamos aun cirujano por sus operaciones o a un músico por su concierto
- Conservar un pensamiento crítico vivo
- No confundir lo bello con lo cierto
- Evitar prejuicios, distinguiendo atentamente los ejemplos concretos de las generalizaciones
- Encontrar el buen orden de nuestras ideas y revisar cuidadosamente nuestros argumentos
- Reflexionar sobre lo que aprendimos del otro en la conversación
```

Los principios del **arte de conversar** de [Michel de Montaigne](https://es.wikipedia.org/wiki/Michel_de_Montaigne), extraido del libro [**El Poder de las palabras** - Cómo cambiar tu cerebro (y tu vida) conversando](https://books.google.com.ar/books/about/El_poder_de_las_palabras.html?id=NI6FEAAAQBAJ&printsec=frontcover&source=kp_read_button&hl=es&redir_esc=y#v=onepage&q&f=false), escrito por Mariano Sigman. [Video](https://www.youtube.com/watch?v=DHIX5MM_FF4)


## Herramientas de software
- [Remg](https://github.com/danielgatis/rembg) quita el fondo a imagenes automáticamente.
- [Flet](https://flet.dev/) -  Permite crear facilmente aplicaciones web, moviles y desktop en Python sin experiencia en fronted.
- [RedBean](https://redbean.dev/) - Webserver en solo un archivo que funciona en 6 sistemas operativos.
- [Refine](https://github.com/refinedev/refine) - Construí tu Aplicación de ABMs con React sin limitaciones.
- [Deck](https://github.com/sfx101/deck) es un entorno de desarrollo web local potente y performante como nigun otro.


## Informes/Encuestas/Lanzamientos/Capacitaciones

- ¿Libernado colores? [Freetone](https://culturehustle.com/products/freetone), [Discusión en GIMP](https://gitlab.gnome.org/GNOME/gimp/-/issues/8885), [FreieFarbe](https://www.freiefarbe.de/en/ )
- [Resultados de la "State of developer nation"](https://developers.slashdot.org/story/22/11/13/2340221/survey-of-26k-developers-finds-java-python-kotlin-and-rust-growing-rapidly) - [Descargar](https://www.slashdata.co/free-resources/state-of-the-developer-nation-23rd-edition)
- [A pedido de Estados Unidos en Cordoba detienen a dos Rusos que mantienen Z-Library](https://www.justice.gov/usao-edny/pr/two-russian-nationals-charged-running-massive-e-book-piracy-website) -  [Z-Library](https://es.wikipedia.org/wiki/Z-Library)
- [Guía tecnológica ética para el día de acción de gracias](https://www.fsf.org/givingguide/v13/)
- [La biblia de la fechas](http://ss64.net/merlyn/index.html), [fechas criticas](http://ss64.net/merlyn/critdate.htm)


## Noticias, Enlaces y Textos


### Sociales/filosóficas

- [Entrevista a Mariana Mazzucato por Jorge Fontevecchia](https://www.youtube.com/watch?v=MdenM67Ymnk)
- [La insoportable levedad de la conducción táctica](https://revistacrisis.com.ar/notas/la-insoportable-levedad-de-la-conduccion-tactica)
- [Proyecto Ballena 2022 | CONFERENCIA CONVERSADA CON FRANCO BIFO BERARDI](https://www.youtube.com/watch?v=EkSwrBhgu7s)
- Echan a miles de empleados de empresas tecnológicas
    - [Amazon](https://www.nytimes.com/2022/11/14/technology/amazon-layoffs.html)
    - [Meta/Facebook](https://about.fb.com/news/2022/11/mark-zuckerberg-layoff-message-to-employees/)
    - [Twitter](https://www.washingtonpost.com/technology/2022/11/04/twitter-layoffs-explained/)
    - [Google](https://www.inc.com/nick-hobson/googles-plan-to-lay-off-10000-poor-performing-employees-is-based-on-a-big-lie-according-toharvard-professor.html)


### Tecnología

- [Soporte de docker para Wasm](https://www.docker.com/blog/docker-wasm-technical-preview/)
- [Comparación de Performance entre WASM/Yew y Javascript/React](https://medium.com/@0x4ndy/performance-comparison-between-wasm-yew-and-javascript-react-part-1-5accafce6315)
- [Pat Gelsinger y Linus Torvalds charlan sobre tecnología](https://www.youtube.com/watch?v=0m4hlWx7oRk)
- [Modelo para evitar cyberflashing de Bumble](https://medium.com/bumble-tech/bumble-inc-open-sources-private-detector-and-makes-another-step-towards-a-safer-internet-for-women-8e6cdb111d81) - [Código fuente](https://github.com/bumble-tech/private-detector)
- [Ventajas de un flujo de trabajo con git vía email](https://drewdevault.com/2018/07/02/Email-driven-git.html) - [Tutorial](https://hispagatos.org/post/git-send-mail/)

### Cooperativas/Economía Popular

- [Conversatorio sobre Cooperativismo Tecnológico y nuevos paradigmas en la Juventud](https://youtu.be/gbQ3s8PHu1Eh)
