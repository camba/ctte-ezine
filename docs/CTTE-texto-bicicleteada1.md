## Bicicleteada por Avellaneda: Costa Conurbano Sur

El Río de La Plata en Buenos Aires, se siente más de lo que se ve. Se huele en el aire, te llena los pulmones, te hiela los huesos, pero Buenos Aires tiene al Río de la Plata medio escondido. Sin embargo, hay muchísimos lugares donde poder verlo, tocarlo o, simplemente, estar mucho más cerca. Algunos son conocidos y otros, muy poco tenidos en cuenta.

En la zona sur del Gran Buenos Aires (área también conocida como "el conurbano"), casi todas las ciudades y localidades que intersecta el ramal del tren Roca que se dirige hacia La Plata tienen su salida al río, su costanera, su parque o su playa.

Nuestra cooperativa de trabajo, Cambá, dedicada al desarrollo de software y a la educación, tiene su base en Bernal, que es una localidad perteneciente al partido de Quilmes, que se ubica en la zona sur del conurbano. Nos gusta descubrir, recorrer y disfrutar del patrimonio natural argentino. Y en especial, del que tenemos tan cerca de nuestros lugares de trabajo y vivienda. Zonas que se encuentran bastante maltratadas por la intervención humana y,  a su vez, son auténticas joyitas escondidas de nuestros paisajes.

Es por eso, que nos propusimos como actividad deportiva y cultural salir a pedalear por la costa de zona sur. Esta es la crónica de nuestra primera salida. El pasado sábado 24 de febrero exploramos la costa de Sarandí y de Villa Domínico.

Nos encontramos a las 9 am en Estación Sarandi (partido de Avellaneda) y desde allí, pedaleamos hasta el Barrio Nuñez. Mucho antes de llegar al Río, antes inclusive de cruzar la autopista, este barrio ya poseé una reserva natural con su propia laguna, conocida como [Laguna La Saladita](https://www.openstreetmap.org/way/26590838). En esta reserva funciona la [Escuela Municipal de Canotaje de Avellaneda](https://emca.com.ar/), cuya misión consiste en la inclusión social a través del deporte y la democratización de las actividades náuticas y en un ambiente natural.

Le dimos la vuelta entera a la laguna y tuvimos la suerte de que justo hubiera un entrenamiento de [Bote Dragón](https://emca.com.ar/bote-dragon). Así que pudimos ver cómo muchas personas, en una embarcación angosta pero muy larga, remaban coordinadas al ritmo de un tambor que hacía sonar otra persona que iba sentada en la proa.

Luego, cruzamos la autopista y nos dirigimos hasta el arroyo Sarandí. Lo recorrimos hasta encontrar un camino rural, hacia el sur y lo tomamos. Salimos al arroyo Santo Domingo: el mismo que podemos observar desde las vías del tren, a la altura de la estación villa Domínico. Yendo por la calle al costado del arroyo, hacia el Río de La Plata, encontramos un camino que se metía hacia el norte y nos aventuramos.

Hicimos algunos metros y descubrimos a unos niños tirando el mediomundo y sacando peces al instante, como si estuvieran pescando en una pecera pero en uno de esos pequeños arroyitos (o zanjas) que abundan cuando nos vamos acercando al río y sacando peces al instante que estaba plagado de peces. Nos acercamos un poco más y del otro lado del arroyito, atrás de un alambrado, lo encontramos a Mario.

![Mario](@img/con_mario.jpg)

Le preguntamos qué es lo que pasó, ¿por qué está todo tan plagado de peces? Nos cuenta que es porque había habido una crecida importante del río. Vemos en su remera, que pertenece a la [UST](https://cooperativaust.com.ar/wp/): una cooperativa de trabajo cuyas personas asociadas se encargan de realizar las labores del mantenimiento post-cierre del Complejo Ambiental Villa Domínico.

Como cuentan en su sitio web: "Dicho predio fue, entre finales de 1978 y principios de 2003, el Centro de Disposición Final Villa Domínico para residuos sólidos urbanos. Mientras estuvo operativo se constituyó como el relleno sanitario más grande de Sudamérica, recibiendo, transfiriendo y disponiendo alrededor de 12.000 toneladas de basura por día".

El Complejo Ambiental Villa Dominíco, que en algún momento próximo abriría sus puertas al público, se encuentra al otro lado del arroyo Sarandí, de este lado, Mario nos cuenta que tienen un [Centro agroecólogico y de ecoturismo rural](https://cooperativaust.com.ar/wp/agroecologico/). Nos invita cuando queramos a hacer un asadito y a pasar el día.

Seguimos por el mismo camino, tras este lindo encuentro. Y tomamos hacia la derecha por un camino mucho más angosto y lleno de vegetación. Vamos a comprar el no tan conocido [Vino de la costa](https://es.wikipedia.org/wiki/Vino_de_la_costa). Vino que se produce, según Wikipedia, a lo largo de la costa del Río de La Plata, desde el Tigre Hasta Magdalena.

![Calle](@img/calle_estrecha.jpg)
![Vino](@img/vino_de_la_costa.jpg)

Llevamos unos envases y nos llevamos, cada uno, un litro y medio de vino de la costa, tinto y dulce. También había rosado, que es más dulce. El muchacho que nos atendió, nos dio de probar los dos. No nos quedamos charlando demasiado con él porque los mosquitos, en esa zona de vegetación tupida, se estaban haciendo un festín con nuestras venas. Vino en mano, nos fuimos lo más rápido que pudimos.

Desandamos lo recorrido y volvimos al camino al costado del arroyo Santo Domingo. Desde allí, encaramos para el río. Llegamos a conocer el nuevo [Parque del Río](https://www.openstreetmap.org/way/1221893802). Desde allí, se puede disfrutar de una hermosa postal de la ciudad de Buenos Aires. Y además, del Río de La Plata en toda su inmensidad.

Estuvimos un ratito en el parque, tomamos un poco de agua, inflamos las bicis. Y volvimos por el camino al costado del arroyo hasta llegar al primer puente que lo cruza. Del otro lado, se encuentra el [CEAMSE Complejo Ambiental Villa Domínico](https://www.openstreetmap.org/way/1214698917) . Vimos que la puerta estaba abierta y quisimos ver si el vigilante nos dejaba pasar y pedalear por unos caminos internos que nos hubieran permitido recorrer esta futura reserva natural y salir cerca de La Ribera de Bernal. Pero no lo conseguimos.

![Santo_Domingo](@img/santo_domingo.jpg)

Entonces, fuimos encarando la vuelta, esta vez hacia la estación de Villa Domínico, donde terminó nuestra aventura y donde nos quedamos un rato pensando cuál va a ser la próxima.
