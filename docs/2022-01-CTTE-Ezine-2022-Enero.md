---
title: Enero 2022
---

# Cambá CTTE Ezine Enero 2022

![estrategia](@img/lunar3.jpg)

```
Vengo picando, hace días, de flor en flor, perdiendo la razón
Tocando las fibras que vibran
Entre suspiros y gemidos, vas comiéndome, vida
Tengo clavada la idea de verte en la vereda
Pero, siempre estás más allá
Más allá, ah-ah-ah
Y, aquí me tienes, china
Escuchándome la vida
Yéndome al río en tren
Y, aquí me tienes, toda china
Escuchándome la vida
Yéndome al río en tren
Gusto de las flores que me vas dejando
No le temo al cambio de piel
Gusto de las flores que me vas dejando
No le temo al cambio de piel
What a low, low day
(Oh-oh-oh-oh) What a low, low day
What a low, low day
What a low, low day
Voy pendiendo la idea de verte en el andén
De verte en el andén
Afinando los sentidos, vengo a verte
Apretemos el vaivén
Exprimámonos el sol
Como el veneno sos
Que viene a romper mi calma
Y, aquí me tienes, china
Escuchándome la vida
Yéndome al río en tren
Y, aquí me tienes, toda china
Escuchándome la vida
Yéndome al río en tren
Gusto de las flores que me vas dejando
No le temo al cambio de piel
Gusto de las flores que me vas dejando
No le temo al cambio de piel
What a low, low day
(Oh-oh-oh-oh) What a low, low day
What a low, low day
What a low, low day
```

Aquí la letra de [Perotá Chingó](https://www.perotachingo.com/) el tema se llama [China](https://www.youtube.com/watch?v=fWNfe7pC_JQ)


## Aporte a proyectos de Software Libre
- [Nuevo PR para facebook-scraper](https://github.com/kevinzg/facebook-scraper/pull/603)


## Herramientas de software

- [OpenRGB](https://gitlab.com/CalcProgrammer1/OpenRGB) - Control de luces RGB para PCs independiente de proveedor
- [Organic Maps](https://github.com/organicmaps/organicmaps) - Organic Maps es una app libre de mapas offline para Android y iOS.
- [JsRestrictor](https://polcak.github.io/jsrestrictor/) - Plugin para browser para restringir el acceso a algunas APIs
- [Turborepo](https://github.com/vercel/turborepo) - Sistema de hacer builds para js y ts especialmente para Monorepos.

## Informes/Encuestas/Lanzamientos/Capacitaciones

- [Lanzamiento de GCompris 2](https://gcompris.net/news/2021-12-17-es.html)
- [Encuesta: State of CSS](https://2021.stateofcss.com/es-ES/)
- Rust para el kernel Linux ( [lista](https://lkml.org/lkml/2021/12/6/461), [Organización](https://github.com/Rust-for-Linux), [charla](https://linux.slashdot.org/story/21/12/11/0334210/the-linux-kernels-second-language-rust-gets-another-step-closer))
-  Vulnerabilidad de [Log4J](https://logging.apache.org/log4j/2.x/) ([Comprobación](https://log4j-tester.trendmicro.com/), [Explicación](https://hackernoon.com/log4j-vulnerability-a-legacy-of-cybersecurity-from-java-to-blockchain-and-minecraft), [VideoExplicación](https://www.youtube.com/watch?v=Opqgwn8TdlM))

## Noticias, Enlaces y Textos

### Sociales/filosóficos

- [Herbert Marcuse: La libertad puede convertirse, en un poderso instrumento de dominación](https://www.bloghemia.com/2020/01/las-nuevas-formas-de-control-por.html)
- [Nicolás Mavrakis analiza No-cosas, nuevo libro de Byung-Chul Han](https://www.pagina12.com.ar/387351-adios-a-las-cosas-el-smartphone-se-devora-el-mundo)
- [¿Existe la naturaleza humana? | Noam CHOMSKY vs Michel FOUCAULT | Análisis del debate](https://www.youtube.com/watch?v=ZaKpoyac6NQ)
- [Radahouse: Hernán Casciari](https://www.youtube.com/watch?v=yrQBk_CylN8)


### Tecnología

- [Enrique Chaparro habla sobre la instalación de una granja de bitcoins en Córdoba](https://ar.radiocut.fm/audiocut/enrique-chaparro-habla-sobre-instalacion-una-granja-bitcoins-en-cordoba/)
- [Jamstack, Stencil y Supbase](https://ionicframework.com/blog/dynamic-jamstack-with-stencil-and-supabase/)
- [Usando webassembly para crear componentes react rápidos](https://www.joshfinnie.com/blog/using-webassembly-created-in-rust-for-fast-react-components/)
- [Malware embebido en NPM: Coa, Rc, Ua-parser](https://fossa.com/blog/embedded-malware-npm-coa-rc-ua-parser/)

### Otras

- Argentinazo - 20 años desde diciembre de 2001: [Poesia de Mari para Gastón](CTTE-argentinazo) y
[Documental de Alejandro Bercovich y César González (2021)](https://www.youtube.com/watch?v=ZX0lEAtObog)
- [Dialogos entre un sacerdote y un moribundo - Marques de sade](https://biblioteca.org.ar/libros/1784.pdf)
