---
title: Septiembre 2024
---

# Cambá CTTE Ezine Septiembre 2024

<div style="text-align: center;">

![Sueños](@img/septiembre2024.jpg)

</div>

```
Salú, salú, que vivan los inadaptados!,
los que no encajan en la regla establecida.
Salú, salú, los descosidos y los rotos.
Salú, salú, las rotas y las descosidas.

Vivan los locos que no escapan de sí mismos,
los que a sí mismos se persiguen y se encuentran,
los que enderezan el manubrio del triciclo
cuando la rueda de adelante se descentra.

Tan anormales como cualquiera,
brujas y herejes,
su sagrado fuego fatuo
encendiendo nuestra hoguera.

Vivan la almas que se fugan de su jaula
y con sus alas cercenadas igual vuelan.
Los revoltosos, malhablados, insolentes
que los echaron a patadas de la escuela.
Las damiselas que abandonan sus castillos
para embarrarse y construir mundos sin dueños.
Los soñadores que han perdido su fortuna,
afortunados de encontrarse con sus sueños.

Tan indomables como cualquiera,
brujas y herejes,
su sagrado fuego fatuo
encendiendo nuestra hoguera

Salú, salú, que vivan los inadaptados,
que están al fondo y al principio de la fila.
Los caballeros con enaguas de princesas,
las principistas en planetas de gorilas.
Salú, salú, que sigan los inadaptados
sin adaptaRse a su eslabón en la cadena,
mientras el mundo sea un gran fotomontaje,
mientras la vida sea una puesta en escena.

Tan inmorales como cualquiera,
brujas y herejes,
su sagrado fuego fatuo
encendiendo nuestra hoguera.

Inadaptados (inadaptadas) como cualquiera,
brujas y herejes,
su sagrado fuego fatuo
encendiendo nuestra hoguera.
```

[Inadaptados](https://www.youtube.com/watch?v=btEwiaIAhfo) Canción de [Lu Ferreira](https://www.instagram.com/luchitaferreira/) y [Tabaré Cardozo](https://es.wikipedia.org/wiki/Tabar%C3%A9_Cardozo)

## Cooperativas/Economía Popular
- [Cambá - Fabrica de inventos](https://archive.org/details/2024.07.17-belen-sanchez-integrante-de-la-cooperativa-camba-y-profe-de-la-fabric): Belén Sánchez: "Queremos que los pibes se vinculen con la tecnología desde lo creativo".
- Plenario FACTTIC - Agosto 2024
    - [Video Saludos](https://drive.google.com/file/d/1kvac1rSET4qcbCthZSvNuDlvy5y9ylhN/view)
Panel IA
    - [Video Panel IA](https://www.youtube.com/watch?v=XfuQi8OXZGw)
- [Revolucionando Redes Mesh](https://altermundi.net/2024/08/01/revolucionando-las-redes-mesh-con-tecnologias-de-codigo-abierto-un-vistazo-al-progreso-del-proyecto-apoyado-por-a-r-d-c/)
- [La autogestión como forma de soberanía: Cooperativa Cambá](https://www.youtube.com/watch?v=-klXYKvQV2w)


## Herramientas de software
- [Zen Browser](https://www.zen-browser.app/) - Diseño hermoso, con foco en la privacidad y empaquetado con funcionalidades.
- [Deep-Live-Cam](https://github.com/hacksider/Deep-Live-Cam) - Intercambio de rostros en tiempo real y generación de deepfake con solo una imagen.
- [OnLook](https://github.com/onlook-dev/onlook) -  Diseña directamente en vivo tu aplicación React y publica los cambios a tu código
- [Porffor](https://porffor.dev/) - Motor JS experimental ahead-of-time desde cero
- [Seek-tune](https://github.com/cgzirim/seek-tune) - Implementación del algoritmo Shazam para encontrar canciones

## Informes/Encuestas/Lanzamientos/Capacitaciones
- [Guia de trabajo: La ansiedad de las revisiones de código](https://developer-success-lab.gitbook.io/code-review-anxiety-workbook-1)
- [Controla tu uso de tecnología](https://www.humanetech.com/take-control)
- [Estandarización de la definción de "Open source AI"](https://opensource.org/deepdive/drafts/open-source-ai-definition-draft-v-0-0-9)
- [Debian cumple 31 años](https://bits.debian.org/2024/08/debian-turns-31.html)
- [Estadistícas del mercado de Sistemas operativos en Argentina](https://gs.statcounter.com/os-market-share/desktop/argentina)

## Noticias, Enlaces y Textos

### Tecnología
- [Escondiendo texto en unicode](https://embracethered.com/blog/posts/2024/hiding-and-finding-text-with-unicode-tags/)
- [¿Cómo hice un juego en 13kb?](https://frankforce.com/space-huggers-how-i-made-a-game-in-13-kilobytes/)
- [23 años de Drupal en 230 Segundos para celebrar el lanzamiento de Drupal 11](https://www.youtube.com/watch?v=RGmqYVYOOUY)
- [Node finalmente soporte TypeScript](https://www.youtube.com/watch?v=2l_mMvO3tbs)
- [Camino al 2do Encuentro Federal Cyberciruja - Técnicas de autodefensa digital contra trolls estatales](https://tube.undernet.uy/w/f9fQ17QVnip9nXNxx8LQYw)

### LLMs
- [Aider](https://aider.chat/) - IA "pair programming" en la terminal
- [Langflow](https://www.langflow.org/) - visual framework para crear aplicaciones multi-agentes y RAG
- [Entrena facilmente Llama 3.1](https://www.youtube.com/watch?v=V6LDl3Vjq-A)
- [Regulación de la Inteligencia Artificial | Enrique Chaparro](https://www.youtube.com/watch?v=_MOyU9rRpa4)
- [Como uso la IA](https://nicholas.carlini.com/writing/2024/how-i-use-ai.html)
