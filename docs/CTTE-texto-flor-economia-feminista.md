# Economía Feminista

Concepto del que se parte para plantear una economia alternativa: **el trabajo doméstico no remunerado**.

> Lo personal es político


## Perspectiva de género

- Existe una constitución androcéntrica y sexista de la sociedad. Y está estructurada por un sistema de género que se encuentra jerarquizado

> La perspectiva de género implica la adopción de un punto de vista que nos permite observar los diferentes fenómenos de la realidad según las implicaciones y efectos que tienen las relaciones sociales entre los géneros.

Implica adoptar una mirada feminista, porque es inequívocamente un producto feminista y una mirada que surge desde el movimiento feminista.

Las diferencias biológicas son transformadas en diferencias sociales.

Tomar consciencia de la discriminación estructural de las mujeres en las sociedades supoe una manera distitna de ver el mundo. Podemos adquirir asi, una conciencia critica  fundamental para develar  y buscar revertir las condiciones de nuestra opresión. Esto es perspectiva de género

Feminismos clásicos no tuvieron siempre en cuenta la múltiples intersecciones de las mujeres. En respuesta, la teoría queer propone un enfoque sobre cómo el poder modela nuestra comprensión de la feminidad/masculinidad en la sociedad, así como en el feminismo también - y como ésto determina la exclusión.

Judith Butler sostiene que **el género es una construcción sostenida por estructuras de poder dominantes**. Las personas adoptan “géneros inteligibles” y con ellos, comportamientos coherentes con las expectativas sociales de género.

Plantea desarmar la configuración hegemónicamente establecida acerca de la biología en relación con una orientación, con un rol social.

El concepto de género pone en crisis que necesariamente nuestros cuerpos sexuados tengan que dar lugar a ciertos roles sociales.

La genitalidad, la sexualidad, en realcion con las conductas sexuales, el género y el deseo son érdenes tortalmente independientes. Invita a pensarlo como una construcción compleja.


### Lucha colectiva

Ahora bien, sin la formación de una comunidad, de un “nosotras”, la liberación es imposible. La sororidad, entonces, es un concepto político. En otros términos, es un modo feminista de hacer política. La sororidad, en definitiva, es un pacto político entre pares, en donde quienes pactan son, justamente, quienes nunca antes habían podido pactar y que, a causa de eso, quedaron por fuera del terreno de lo público y de la arena política.

---

## Economía Feminista

Qué leyes rigen la distribución de la riqueza?
Cuál es la naturaleza de las leyes que rigen el mercado?

**Conceptos clave**:

trabajo | mercancía | dinero | capital

Los debates actuales de la economía política abordan la discusión sobre la desigualdad en la distribución de la riqueza.

Lo que marca el inicio de la Economía como Ciencia es la aparición de su objeto de estudio: la sociedad capitalista.

El capitalismo tiende a la concentración de la riqueza en las manos de un pequeño grupo social.

Ahora bien, las teorías clásicas económicas carecen de perspectiva de género.

La **Economía Feminista** retoma debates históricos del feminismo en torno al **trabajo doméstico como una de las bases del funcionamiento del mundo en el que vivimos**. Estas tareas, tan fundamentales como ineludibles, llevan muchísimas horas de esfuerzo y si bien es posible pagar por ellas, en general se hacen gratuitamente como parte de una actividad familiar. La carga de su ejecución está asimétricamente distribuida y su peso recae mayoritariamente sobre las mujeres.

Esto es, la teoría de la reproducción social.

Cuando el trabajo mercantil - al ser la fuente de ingresos - es el símbolo de la autonomía individual, la distribución asimétrica del trabajo doméstico tiene como consecuencia que las mujeres tengan menos posibilidades de incorporarse en el mercado laboral y, cuando lo hacen, es en peores condiciones, con salarios menores y mayor informalidad.

En este sentido, **Marilyn Waring** planteaba que el sistema de medición del PBI, es directamente arbitrario y desconoce en absoluto el aporte que las mujeres hacen dia a dia a la economía de un país. En esta contabilidad se omiten las tareas que se hacen en los hogares, y así se subestima la contribución económica de las mujeres, posicionandolas en las filas del trabajo no productivo.

Al no incorporar esta medición, no podemos evaluar el real impacto de las medidas económicas, y perpetúa la situación de desigualdad en la que se encuentran las mujeres. Además, la variable invisible de un modelo es la que primero se ajusta (por omisión).

**Silvia Federici** continua esta línea y pone el acento en el rol de la mujer  en el proceso productivo: el hecho de que el trabajo doméstico aparezca como un atributo de la feminidad lo convierte en un trabajo que se hace por amor.

> El simple hecho de reclamar un salario para el trabajo doméstico significa rechazar este trabajo como expresión de nuestra naturaleza y a partir de ahí, rechazar el rol que el capital ha diseñado para nosotras.

A su vez, señala que el ingreso masivo de la mujer en el trabajo productivo, no supuso ningún cambio en la jerarquía establecida. Por el contrario, el segundo trabajo no sólo aumenta nuestra explotación sino que reproduce nuestro rol en diferentes formas.

Dónde inician y terminan nuestros trabajos? Dónde comienzan y acaban nuestro deseos?

Argumentos clave de la economía feminista coinciden en la necesidad de medir y asignarle un lugar en las cuentas nacionales a los trabajos de cuidados.

Por su parte, **Amaia Orozco & Cristina Carrasco**, plantean que en la sociedad capitalista el conflicto fundamental es entre el capital y la vida, y que no es posible garantizar la sostenibilidad de la vida en el marco de las relaciones capitalistas de producción.

Así, la Economía Feminista constituye una crítica a la economía hegemónica en tanto no se enfoca en los procesos de mercado, sino que amplía la mirada a los procesos de sostenibilidad de la vida. Argumentan que la naturaleza y los trabajos domésticos y de cuidados son las bases del sistema económico actual. Este enfoque advierte que no es posible un sistema en el cual se anteponen los beneficios económicos al sostenimiento de la nturaleza y de la vida de las personas.

**Federici** decía:

> no hay nada más asfixiante para la vida que ver transformadas en trabajo las actividades que satisfacen nuestros deseos. De igual modo, es a través de las actividades cotidianas por las que producimos nuestra existencia que podemos desarrollar nuestra capacidad de cooperar, y no solo resistir a la deshumanización, sino aprender a construir el mundo como un espacio de criaza, creatividad y cuidado.

El género, así, estrucutra la división en el seno del trabajo asalariado: ocupaciones industriales y profesionales mejores pagas son ocupadas por hombres. Se trata de una estructura político-económica que explota, marginq y priva según el género.

Con todo esto, vemos que el conflicto de la redistribución de la riqueza esta atravesado por el género.

El resultado es un círculo vicioso de subordinación cultural y económica. Por tanto, para combatir la injusticia de género hay que cambiar tanto la economía como la cultura.

El feminismo, a su vez, interactúa con todas las luchas del pueblo, integrando la necesidad de avanzar hacia la descnolonización efectiva de nustros territorios y cuerpos, sueños y proyectos. Se trata de superar las jerarquías sociales existentes desafiando las fuentes estructurales de la dominación de género en la sociedad capitalista.


#### Trabajo doméstico y de cuidados

*¿Podemos ponerle precio al tiempo consumido en trabajo reproductivo?*

Si lo hicieramos, las personas pueden liberar su carga de trabajo reproductivo vía:

* Centros de cuidados (guarderías, jardines maternales, casas de retiro).
* Servicios particulares (servicio doméstico, personal de cocina, enfermerxs, niñerxs).

Si no lo hacemos, el costo para quienes no pueden pagar ese precio es:

* Disponer de menos tiempo para estudiar, formarse, trabajar fuera del hogar.
* Tener que aceptar trabajos más flexibles (a veces precarios y peor pagados).
* Enfrentar una doble jornada laboral.

> La jornada laboral (…) es eterna. Los hogares están cambiando pobreza de ingresos por pobreza de tiempo. **Salen a trabajar para ganar más dinero pero pierden en términos de calidad de vida**. **¿Cuántas cosas más podrías hacer con una o dos horas extra por día?**

**Así, podríamos considerar el tiempo como una dimensión invisible de la pobreza.**

Diferentes **alternativas para valorar económicamente los trabajos** incluyen:


* Salarios y jubilaciones para quienes hacen trabajo doméstico
* Cobertura universal o servicios públicos de cuidados para niños y mayores, o personas con discapacidad, entre otras.


---

## Aportes de la Economía Feminista en la ESS

`La Economíá será solidaria si es feminista`

Más allá de la visión práctica que hay sin duda en la solidaridad humana y en la cooperación entre personas (posibilidad de resistir frente al deterioro de las condiciones de trabajo y de vida, así como de crear y administrar recursos de uso común, por  ejemplo),  tanto por el modo de organización interna —basada en la horizontalidad del grupo— como por la democracia en sus procesos y los valores que promulga, muchos de los cuales se hacen eco del análisis feminista, la Economía Social y Solidaria tiene el potencial  no  sólo  de  generar  y  mantener  empleo  por cuenta propia, sino de hacerlo de manera colectiva y en base a criterios feministas.

**Implica la identificación del conjunto de actividades que permiten la sostenibilidad de la vida humana en condiciones dignas de forma sostenible en el tiempo.**

Una de las aportaciones feministas la constituyen las **reflexiones en torno al concepto del trabajo, entendiendo este como el conjunto de actividades que constribuyen al bienestar personal y colectivo, que superan el estrecho concepto de actividad y empleo utilizado en general en economía**.
En esta perspectiva, se incluye en la base de las actividades mercantiles, aquellas tareas básicas para la reproducción social que no pasan por el mercado y que han sido realizadas principalmente en la esfera de las unidades familiares y comunitarias.

Es necesario **recuperar, visualizar y reconocer las lógicas vitales que se encuentran en la base de las relaciones socioeconómicas, entre las que se destacan las actividades vinculadas con los valores de reciprocidad, colaboración y el apoyo mutuo, comunmente invisivilizadas**, para poder comprender las condiciones de vida de mujeres, hombres y disidencias insertas en los procesos sociales, con sus conplejidades, jerarquías y posibilidades de cambio.

### Consensos y encuentros entre ambas

La **Economía feminista** tiene como objeto de análisis el conjunto de vínculos que establacen las personas para organizar sus relaciones sociales ligadas con la satisfacción de sus necesidades y la reproducción material de la vida, haciendo hincapié en que éstas son también relaciones afectivas, de apoyo mutuo y de colaboración. Sostiene la importancia del reconocimiento de los ciclos vitales, y de la necesidad de cuidados y atenciones que tenemos unos de otros para desarrollar una vida de calidad.
Por otro lado, la **Economía Social y Solidaria**, tiene sus orígenes en en los movimientos sociales del siglo XIX (corrientes ideológicas como socialista, social-cristiana, anarquista) que pretendían superar las relaciones de opresión generadas por las estructuras capitalistas.
REAS Euskadi elabora una lista de **principios**:
**equidad** | **trabajo** | **sostenibilidad ambiental** | **coperación sin fines lucrativos** | **compromiso con el entorno**.

**Ambas ponen a las personas y sus condiciones de vida en el centro del análisis y visibilizan aquellos valores colaborativos intrínsecos en las relaciones entre las personas a la hora de llevar a cabo los trabajos socialmente necesarios para la reproducción social**.

El siguiente es un **manifiesto** difundido por el grupo de trabajo **EkoSolFem**:

> **¿Cómo construimos entre todos vidas más habitables?**

> El capitalismo heteropatriarcal nos presenta un mundo compuesto por dos esferas dicotómicas -productiva y reproductiva-, aparentemente armónicas, que, sin embargo, está plagado de tensiones.
>
> Cuando nos dicen que sólo el ámbito productivo genera riqueza y aumenta el PIB, nosotras respondemos que los hogares son productores de bienes y servicios esenciales para la vida y que han sido sistemáticamente olvidados en los análisis económicos, con todos los efectos perversos que esa constante invisiblización tiene para la vida de las mujeres. Cargas ingentes de cuidados escasamente repartidas que nos enferman y empobrecen y que se ven como una limitación en un mercado laboral excluyente que nos relega a sectores feminizados muy mal remunerados.
>
> Cuando nos cuentan que el crecimiento económico revierte en toda la sociedad y que la mejor manera de crecer es producir más, nosotras les recordamos, por un lado, que el funcionamiento del libre mercado no responde a las necesidades de la mayoría de las personas ni de los pueblos; y, por otro, que la idea de que el crecimiento es deseable e, incluso, posible en un mundo finito, no tiene ningún sentido.
>
> Cuando insisten en que el mercado es autónomo, que se autoregula y que las personas son libres para elegir y moverse en él, nosotras decimos que se ha dejado en manos de las mujeres la responsabilidad de la subsistencia y el cuidado de la vida, y nos preguntamos quién cuidaría de la vida si mujeres y hombres nos comportáramos con absoluta libertad, dando prioridad absoluta a la participación en el mercado capitalista.
>
> Y cuando nos dicen que la Vida no puede estar en el centro de la economía, nosotras respondemos que **la economía es la gestión de la Vida y que es precisamente la sostenibilidad de esta vida lo que debería estar en el centro de cualquier análisis o toma de decisión en el ámbito económico**.
>
> Tenemos claro que el cuestionamiento y la confrontación con el patriarcado y el capitalismo son elementos clave para la construcción de alternativas. Y, también, que **para construir una economía solidaria y feminista debemos transformar no sólo nuestras formas de hacer, sino también a nosotros y nosotras mismas, y a nuestras organizaciones**.


> **¿Por dónde empezar? Avanzamos y compartimos (al menos) doce propuestas…**
>
> 1. Sensibilizar y generar conciencia crítica a nuestro alrededor. Formarnos y capacitarnos.
>
> 2. Que nuestros compañeros de la economía solidaria comiencen con esa revisión de la masculinidad hegemónica que exige cuestionar y renunciar a privilegios de género.
>
> 3. Mirarnos hacia lo interno. Realizar diagnósticos de la situación de mujeres y hombres en nuestra organización para identificar dónde están las desigualdades y establecer acciones concretas que nos permitan ir eliminándolas.
>
> 4. Promover procesos de empoderamiento de las mujeres. Preguntarnos ¿dónde están y dónde participan las mujeres de nuestra organización? ¿existe el techo de cristal, dificultades para acceder a los espacios de toma de decisiones? ¿Qué modelos de participación tenemos?
>
> 5. Transformar nuestros procesos de trabajo. Diseñar planes operativos que incluyan TODAS las tareas (productivas y reproductivas) que hacen sostenibles nuestras causas y preguntarnos quién hace cada una de ellas, cómo se traducen en términos de valor monetario, etc.
>
> 6. Revisar  nuestra cultura organizacional, analizar los valores que predominan en nuestras organizaciones, muchas veces, contaminadas por la idea de "militancia heroica" valorando y premiando a aquellas personas que trabajan muchas horas, que nunca dicen que no, que nunca están cansadas y que no son emocionales. A aquellas para las que el empleo es el centro de sus vidas y de su proyecto vital.
>
> 7. Poner encima de la mesa el debate sobre los salarios; si consideramos la retribución como un reconocimiento al valor de lo aportado; o como una forma de cubrir las necesidades vitales de las personas. ¿Hay tareas que se reconocen y pagan, y otras que no?
>
> 8. Integrar una mirada hacia la diversidad; visibilizando la diversidad sexual, de orientación y de identidad, la diversidad funcional, cultural... E incorporar una mirada que tenga en cuenta la superposición de desigualdades (de clase, etnia, raza, orientación sexual, diversidad funcional...) que complejizan las identidades, las condiciones de vida y las distintas (y desiguales) posiciones en el sistema socioeconómico.
>
> 9. Cuidar el lenguaje y las imágenes y hacer un uso inclusivo de las mismas.
>
> 10.  Generar protocolos y sistematizar respuestas ante las violencias machistas en nuestras organizaciones. Desde las más sutiles e invisibles (comentarios machistas, ningunear opiniones, invisibilizar tareas...) hasta comportamientos relacionados con el acoso sexual y sexista.
>
> 11. Estar pendientes del cuidado del entorno, de nuestra huella ecológica.
>
> 12. Tejer redes y alianzas entre feministas de las organizaciones de la economía solidaria, pero también, alianzas con organizaciones del movimiento feminista para poner en marcha estrategias comunes.




---
### Pensamientos que surgen:

* Esto me lleva a preguntar, si podemos identificar en Cambá aquellas tareas y o acciones que suceden y sostienen el trabajo productivo en sí? Se realizan tareas de cuidado que aseguran el desarrollo económico de la cooperativa?
* Si identificamos algunas, estan equitativamente distribuidas?
