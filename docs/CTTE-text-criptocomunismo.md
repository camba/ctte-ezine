# Criptocomunismo - Mark Alizart

> Resumen hecho a partir de los renglones subrayados del libro leído durante diciembre de 2020.

## Intro
Bitcoin es un algoritmo de confianza y fé, que permite liberarse matemáticamente de los "terceros de confianza".
Quiere restaurar la confianza, no destruirla. Restaurar las instituciones en las que podemos creer, no quemarlas.
Nos liberá uniéndonos unos a otros. Es una institución del libertad, no la libertad de todas las instituciones.

### Comparación
Al compararlo por ejemplo con el protestantismo, su idea en introducir aún más rigor en la religión que el catoliscismo. Lutero no queria derrocar la ley de Dios, queria cumplirla. Rosseau no quiera que la ley de la naturaleza reemplazará la ley de los hombres, queria asegurarse de que se observara la ley de los hombres.
La libertad, paradojicamente, en ambos casos era la mejor manera de hacer cumplir la ley de Dios y el gobierno de los hombres, ya que la liberta no consiste en ser libre de toda ley, sino en imponerse libremente leyes a uno mismo, como la palabra "autonomía"  lo dice claramente una "ley" (nomos) impuesta sobre "unx mismx" (auto).


## El estado sin el estado
La ambición de Marx como filosófo y político era encontrar un medio de proteger la libertad. No confiaba en la capacidad del merado de seguir siendo un mercado si era dejado a su propia cuenta. El estado no es aquello que se opone al mercado, sino un invento de los capitalistas para proteger su propiedad privada, para hacer avanar sus intereses, para disuadir el desarrollo de la competencia. El estado nunca es más que el interés privado dominante disfrazado de interés público.
La única diferencia entre Marx y los libertarianos ("libertarios de derecha") pasa por la estructura encargada de regular el mercado. Para Marx esa estructura pasaba por los "consejos populares", encargados de los mismos poderes, para los libertarianos era un consejo de sabios (tecnocratas no electos).
¿Quien puede paliar las disfunciones del mercado?
Para el autor "Bitcoin" es lo que le falto al comunismo para llevar a cabo su destrucción organizada del Estado.

## Del centralismo democrático al consenso descentralizado

Bitcoin es un protcolo que permite producir consenso de manera descentralizada. Antes de la blockchain era necesario escoger entre consenso y descentralización. O bien se estaba del lado de la anaquía, o bien de la sociedad de control. LLegar a un consenso significaba que una instancia central conotrolará y validara la expresión de los miembros de la colectividad.
La blockchain resuelve el problema de la teoría de juegos llamado ["El problema de los generales bizantinos"](https://medium.com/@freddy.abadl/bitcoins-y-el-problema-de-los-generales-bizantinos-54b067dca501).
Bitcoin es un protocolo de intercambio perfectamente transparente (cada quien tiene el registro total), descentralizado (nadie tiene su control), infalsificable (validado por pruebas de trabajo), indescifrable (información encriptada) e inviolable (integridad asegurada).

## [Automatización completa del comunismo de lujos](https://www.versobooks.com/books/3156-fully-automated-luxury-communism)

Hay varias tipos de organizaciones colectivas y pueden ser jerarquizadas en función de la [perecuación](https://es.thefreedictionary.com/perecuaci%C3%B3n) que operan entre "inclusión" y "selección": las más selectivas son las más óptimas pero también menos inclusivas. Por ejemplo en la democracia la eficiencia es mediocre porque la confianza entre las partes es débil y los individuos muy heterogéneos, pero lo que se pierde en eficacia se gana en inclusión, debido al efecto masa.
En la blockchain se recompensa el compromiso de sus miembros, su adhesión a lo común, somos funcionarios, funcionarios de la red. La carga de la prueba está invertida, dado que es la prueba de trabajo la que produce el valor y no el valor percibido el que debe remunerar el trabajo.

## Termocomunismo

El es que si el Estado es privatizado, segun Marx, es porque el aparato productivo es privatizado previamente y sus propietarios extorsionan al Estado para fortalecer sus rentas.
Lo que propone Bitcoin para escapar de la arbitrariedad de los bancos es una "apropiación colectiva de los medios de producción monetaria".
En su reflexión acerca de la socialización de los medio de producción, Marx no pensaba realmente en el dinero. De hecho nunca se interesó mucho en el asunto del dinero. Nunca creyó que una politica monetaria fuera capaz de hacer advenir, por si misma, a la sociedad comunista, a diferencia de Proudhon, por ejemplo, quien sostenia que la emancipación de los proletarios pasaba por la emancipación de la moneda emitida por los bancos burgueses e incluso por la abolición total del dinero, o del inglés Robert Owen, quien inventó la primera moneda complementaria destinada a obreros. Marx nunca vio en esas monedas más que "contraseñas de teatro", billetes del monopoly que no cambiaban nada la relación de dominación entre patrone y obreros, ni el proceso de extracción de la plusvalís sobre el cual se funda la acumulación del capital. AL contrario, Marx pensaba que Proudhon y sus amigos cedían a la fascinación de un " fetiche" que caían en la trampa capitalistas de la "fiebre del oro".
Para Marx el dineo es una abstracción, porque el valor en si mismo no existe, solo existe el trabajo. Solo existe el trabajo acumulado en una mercancía que le da valor.
Podemos pensar, sin embargo, que si Marx hubiera tenido acceso al concepto de información habría pensado de una manera muy diferente la superación del capitalismo y que quizás lo que habría pensado bajo la perspectiva del dinero, que no es nunca solamente la medida de la información económica.

## Las instituciones monetarias del capitalismo

Si se incluye la reflexión sobre el dinero, debería darse como objetivo impedir la privatización del dinero, más que la abolición de la propiedad privada, ya que la privatización del acceso a la inversióm es in fine la responsable de la existencia de esta última. La privatización de la causa de la ineficiencia de los mercados y de su tendencia a secretar Estado.

## El oro de los locos
Una moneda deflacionista desalienta el consumo.
El problema del patrón oro es que limita la liquidez a disposición de los mercados.
Basta con que dos personas se pongan de acuerdo sobre aquello que es dinero para que exista.
El problema no es que exista demasiado, sino que el acceso al que existe es vuelto cada vez más díficil por quienes lo poseen y obtienen beneficios, aquellos que alteran la compotencia y manipulan los precios para llenarse los bolsillos.

## Todo el mundo es banquero
Cada cual, es un burbuja algorítmica. Internet no hizo que naciera un medio comunicación global, sino que volvío a cada uno capaz de transformarse en su propio medio de comunicación.
Los bitcoiners dicen que la blockchain permite que cada quien se transforme en su propio banquero, es verdad a condición de interpretar esto en todo el sentido del término: no significa que de ahora en adelante cada quien posea sus valores en su propia caja fuerte, sino que cada uno puede acuñar moneda, como un banco central o estado soberano.

## Tercera parte
- La información es compartida en tantos ejemplares como personas hay para recibirla.
- El ambiente cripto, pretende ser pragmático. No cree más que en lo que funciona.
- Generalmente se piensa que las maquinas serán inteligentes cuando den muestra de consciencia. Esto es invertir la relaión de causa y efecto. La consciencia de sí precede a la inteligencia.

## Conclusión
La izquierda todavía no se ha apropiado bien de la blockchain, mucho menos de Bitcoin. Las razones son múltiples, ya hemos citado algunas: una cultura política que no la vuelve curiosa por el dinero en general, ni por las innovaciones financieras; una relación con la informática complicada, que favorece una relación con la energía; el fracaso del cibercomunalismo de los años 70; finalmente, la inclinación libertariana personal de Satoshi Nakamoto, que suscribe de facto Bitcoin a la derecha.
Es un error. Si los socialistas realmente buscan un medio para superar el capitalismo, para destruir el estado, para hacer progresar la causa ecológica, es allí que se encuentra y no en la vanas vociferaciones contra el sistema financiero, en los sit-in deltante de wall-street, sobre todo cuando se trata de las luchas sociales por la justicia social.
Por su puesto, no prentendemos que la revolución vaya a hacerse en un dos por tres. Cada día que pasa es testigo de cómo el mundo muestra signos de una fragilidad cada vez más grande. Cada día nos acerca al colpaso de un país bajo el peso de su deuda, económica y ecológica. Antes de que sea posible retomar el control de la moneda enegética de la tierra y de las sociedades correrá mucha agua bajo el puente, incluso sangre. En especial mientras tanto siguen habiendo muchos problemas por resolver.
Para volver a hablar solamente de bitcoin, sigue estando limitado por el número de transacciones que puede tratar por segundo, su descentralización esta amenzada por las multinacionales de la minería, su mercado está infectado de abusos de información privilegiada en abundancia y de productos financieros adulterados. En cuando a su uso mismo, que supone dominar un mínimo de herramientas informáticas, está amenazado por la fractura tecnológica que aún separa a ricos y pobres.
La reforma dio lugar a 30 años de una guerra cívil que causo cuentos de millones de muertes antes de que la nueva situación espiritual que ella portaba se impusiera en occidente. Las revoluciones fueron seguidas por casi un siglo de conflictos munidales que oponían a nostálgicos del antigua régimen y progresistas. Quizás haya que esperar que la cripto, que finaliza estas dos transformaciones históricas, no se realice sin dolor.
Por esto solo quiere decir que tenemos que hacerlo todo por apropiarnosla y por acelerar el movimiento. ¡Criptoproletarios del mundo, uníon!
