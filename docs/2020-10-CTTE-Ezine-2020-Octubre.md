---
title: Octubre 2020
---

# Cambá CTTE Ezine Octubre 2020

## Texto de Coyuntura


```
Soy unx chicx paciente
Espero, espero, espero, espero
Mi tiempo es como agua por un desagüe

Todes se están yendo
Por favor, no me dejen quedar

En la sala de espera
No quiero las noticias
No puedo usarlas
No quiero las noticias
No viviré por ellas

Sentades fuera de la ciudad
Todes siempre están deprimidos
Decime ¿por qué?
Porque no pueden levantarse
Vamos, levantense

Pero no me quedare con los brazos cruzados

Estoy planeando una gran sorpresa
Voy a luchar por lo que quiero ser

Y no cometeré los mismos errores
Porque sé cuánto tiempo desperdicia
Tener un proposito es clave

¿Dime por qué?
Porque no pueden levantarse
```

Canción llamada [Waiting Room](https://en.wikipedia.org/wiki/Waiting_Room_(song)) escrita por [Ian MacKaye](https://en.wikipedia.org/wiki/Ian_MacKaye) para [Fugazi](https://en.wikipedia.org/wiki/Fugazi). [Video con subtitulos](https://www.youtube.com/watch?v=ijnhdfqKRV0).

Queriendo asegurar la longevidad del nuevo proyecto, estaba absolutamente determinado a que no iba a cometer los mismo errores nuevamente esta vez. Es una canción sobre esperar las personas correctas en el momento correcto.

## Contribuciones al Software libre de Cambá

- Contribuciones y creación de proyectos libre de Santiago Botta:
    - [Coopcycle Web PR-1784](https://github.com/coopcycle/coopcycle-web/pull/1784)
    - [Coopcycce Web PR-1785](https://github.com/coopcycle/coopcycle-web/pull/1785)
    - [Coopcycle docs PR-46](https://github.com/coopcycle/coopcycle-docs/pull/46)
    - Nuevo Proyecto [Metabase-compose](https://github.com/Cambalab/metabase-compose)

- Contribuciones y creación de proyectos libres de José Luis Di Biase:
    - Proyecto [cd-drive](https://pypi.org/project/cd-drive/): [PR-1](https://github.com/matheusbsilva/cd_drive/pull/1), [PR-2](https://github.com/matheusbsilva/cd_drive/pull/2)
    - Nuevo proyecto [oodocument](https://github.com/Cambalab/oodocument)
    - Nuevo proyecto [publicador](https://github.com/Cambalab/publicador)

- Creación de nuevo proyecto libre de Carlos Cuocco
    - Nuevo proyecto [Python-uno-dockerfile](https://github.com/Cambalab/python3-uno-dockerfile)


## Herramientas de software

- [Lepton](https://github.com/dropbox/lepton) - Compresión de archivos JPEGs (promedio 22%)
- [Headless recorder](https://github.com/checkly/headless-recorder) - Extensión de Chrome que graba las interacciones con el browser y genera código Puppeteer o Playwright
- [Caprover](https://github.com/caprover/caprover) - Sistema para tener tu propio sistema de "Plataforma como servicio"
- [Zealdoc](https://zealdocs.org/) - Documentación offline para desarrolladores de software.
- [Present](https://github.com/vinayak-mehta/present) - Herramienta para generar presentaciones en terminal con colores y efectos especiales.
- Herramientas de extracción y análisis de datos de redes sociales:
    - Twitter - [Twint](https://github.com/twintproject/twint)
    - Instagram - [Osintgram](https://github.com/Datalux/Osintgram) y [Instaloader](https://github.com/instaloader/Instaloader)
    - Tiktok - [Tiktok-scraper](https://github.com/drawrowfly/tiktok-scraper)


## Informes/Encuestas/Lanzamientos/Capacitaciones

- [Muchas líneas de Notebooks Lenovo se comienzan a vender con  Ubuntu preinstalado a partir de septiembre 2020](https://ubuntu.com/blog/lenovo-expand-enterprise-desktop-range-preinstalled-with-ubuntu)
- [Ganadores de concurso js13games](https://medium.com/js13kgames/js13kgames-2020-awards-ceremony-f85bbe0863e5)
- Evento internacional de redes tecnológicas comunitarias [BattleMesh](https://www.battlemesh.org/BattleMeshV13)
- [Ionic anuncia soporte estable para Vue](https://ionicframework.com/blog/announcing-ionic-vue/)

### Libro "Estado y Software Libre"
> Se presento  el libro “Estado y Software Libre, Aportes para la construcción de una comunidad colaborativa y soberana. Se recuperan textos vinculados a la lucha por la Ley 13139 de Software Libre, de la provincia de Santa Fe.
- [Más información](http://observatoriolitoral.com.ar/presentamos-el-libro-estado-y-software-libre-aportes-para-la-construccion-de-una-comunidad-colaborativa-y-soberana/)
- [Descargar Libro](http://observatoriolitoral.com.ar/wp-content/uploads/2020/10/EstadoySL-2020.pdf)

### Encuesta de Sueldos 2020.02 - Julio/Agosto
> [Encuesta](https://sueldos.openqube.io/encuesta-sueldos-2020.02/
) que lleva a cabo [openqube.io](https://openqube.io/), que es una plataforma colaborativa en la que empleados y ex empleados pueden calificar y escribir reseñas anónimas para brindar información de calidad sobre las empresas

- [Contribuye al open source/software libre](https://sueldos.openqube.io/encuesta-sueldos-2020.02/#Perfil-de-participantes-Roles-Contribuis-a-proyectos-Open-Source): 14%
- [Programa por hobbie](https://sueldos.openqube.io/encuesta-sueldos-2020.02/#Perfil-de-participantes-Roles-Programas-por-hobbie): 53%
- [Salarios](https://sueldos.openqube.io/encuesta-sueldos-2020.02/#Salarios) - Mediana salarial en Argentina $96000, U$S 1314 (Al 15/08/2020)
  - **Developer** Junior: 62k - Semisenior: 93k - Senior: 120k
  - **Project manager** Junior: 63k - Semisenior: 133k - Senior: 140k
  - **Sysadmin** Junior: 76k - Semisenior: 85k - Senior: 100k
  - **Lider Técnico** Junior: 122k - Semisenior: 128k - Senior: 156k
  - **Arquitecto** Junior: 96k - Senior: 160k

La encuesta tiene múltiples aristas y pueden hacerse diferentes análisis a partir de ella.

# Noticias, Enlaces y Textos

### El deseo de cambiarlo todo

- [Manifesto por algoritmias hackfeministas](https://utopia.partidopirata.com.ar/zines/manifiesto_por_algoritmias_hackfeministas.html)
- [Mujeres en STEAM - Historias sobre mujeres en Ciencias, Tecnología, Ingeniería, Artes y Matemáticas](https://medium.com/mujeresensteam)
    - Ex Socia de Cambá - [Sol Verniers](https://medium.com/mujeresensteam/mujeres-en-steam-qui%C3%A9n-es-mar%C3%ADa-sol-verniers-3c9136c4c18f)

### Sociales/filosóficos

- [¿Cómo reconstruir la subjetividad cuando hay miedo al acercamiento de los labios?](http://lobosuelto.com/bifo-berardi-labios-buenosaires-sztulwark/)

- Nuevo libro ["El umbral. Crónicas y meditaciones", Crónicas y Meditaciones, editado por Tinta Limón](https://tintalimon.com.ar/libro/el-umbral/) escrito por [Franco Berardi Bifo](https://tintalimon.com.ar/autorxs/#franco-berardi-bifo)
    - [Video de la presentación](https://www.youtube.com/watch?v=M3SX4gRbrKc)

- Documental ["The social Dilemma" o "El dilema de las redes sociales"](https://www.thesocialdilemma.com/)
  Interesante documental de alcance masivo distribuido por Netflix, sobre las redes sociales, que esta generando muchos comentarios, posicionamientos y criticas partir de lo que plantea.  Algunas criticas interesantes en [Onezero](https://onezero.medium.com/where-the-social-dilemma-gets-us-1a9c91e2c48b),  [Jacobinmag](https://jacobinmag.com/2020/09/the-social-dilemma-review-media-documentary)


### Técnicos

- [¿Como evaluar proyecto de software libre?](https://www.functionize.com/blog/wheres-the-yelp-for-open-source-tools/)
- [La última fase de la guerra de escritorios](http://esr.ibiblio.org/?p=8764)
- [Revista Lupin](https://www.automatismos-mdq.com.ar/blog/2020/07/la-revista-lupin-y-la-electronica.html)
- [Vue Composition API: useState y useReducer](https://markus.oberlehner.net/blog/usestate-and-usereducer-with-the-vue-3-composition-api/)
- [La ley de Benford por Adrián Paenza](https://www.pagina12.com.ar/diario/contratapa/13-124245-2009-05-03.html)


## Facttic

- [Collective construction, the best strategy of the Argentine Federation of Technology, Innovation and Knowledge Worker Cooperatives (FACTTIC) to face current challenges](https://www.ilo.org/global/topics/cooperatives/news/WCMS_756586/lang--en/index.htm)
- [Entrevista a Maizal.coop en podcast Diseño y Diáspora](https://disenoydiaspora.org/2020/10/01/167-una-apuesta-al-cooperativismo-en-diseno-argentina-una-entrevista-con-agustina-silombra-y-gerardo-borras/)

- Jornadas de Cooperativismo, Género y Software Libre - Dirección de Economía Social del  Ministerio Producción Santa Fe
    - [Primer Encuentro](https://www.youtube.com/watch?v=1zO2EynVVnU)
    - [Segundo Encuentro](https://www.youtube.com/watch?v=62_XKdCzQu8)
    - [Tercer Encuentro](https://www.youtube.com/watch?v=MXCd2eH55VE)

- Presentación de Gcoop - Cooperativismo y Software libre en UTN - Ciclo de Charlas de Informática en casa de la Carrera de Informática Aplicada. [Video](https://www.youtube.com/watch?v=sZr2seOm77E) y [Descarga Material](https://www.gcoop.coop/pdf/coop-y-sl-bienes-comunes-2013.pdf)

- Cooperativa [Catalyst](https://catalyst.coop/) libera [Pudl](https://catalyst.coop/pudl/), [Proyecto en github](https://github.com/catalyst-cooperative/pudl) - Proyecto que ayuda a ver los datos gigantescos e inmanejables de las agencias de gobierno.


### Otras

- Columna de Tecnología de Neto en Sonámbules en [FM Las CHACRAS 104.9](https://radiolaschacras.blogspot.com/)
   - Charlando sobre el Proyecto Liberajus de Cambá - [Audio](https://archive.org/details/entrevista-camba-liberajus)

