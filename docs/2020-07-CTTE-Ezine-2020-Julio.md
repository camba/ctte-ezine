---
title: Julio 2020
---

# Cambá CTTE Ezine Julio 2020

## Poema XXIX

```
No soy igual en lo que digo y escribo.
Cambio, pero no cambio mucho.
El color de las flores no es el mismo bajo el sol
que cuando una nube pasa
o cuando entra la noche
y las flores son color de sombra.
Pero quien mira ve bien que son las mismas flores.
Por eso cuando parezco no estar de acuerdo conmigo
fijaros bien en mí:
si estaba vuelto para la derecha
me volví ahora para la izquierda,
pero soy siempre yo, asentado sobre los mismos pies.
El mismo siempre, gracias al cielo y a la tierra
y a mis ojos y oídos atentos
y a mi clara sencillez de alma.
```

Escrito por **[Fernando Pessoa](https://es.wikipedia.org/wiki/Fernando_Pessoa)** o mejor dicho de su [heterónimo](https://es.wikipedia.org/wiki/Heter%C3%B3nimo), pero a su vez [ortónimo](https://es.wikipedia.org/wiki/Ort%C3%B3nimo) **[Alberto Caeiro](https://es.wikipedia.org/wiki/Fernando_Pessoa#Alberto_Caeiro)**:
- [Otros poemas para leer de sus alteregos](https://www.poemas-del-alma.com/fernando-pessoa.htm)
- [Lectura del El banquero anarquista](https://www.youtube.com/watch?v=_rpZbic0GII)

## Aportes a proyectos de Software libre

- Contribución de Facundo Mainere a [ra-data-feathers](https://github.com/josx/ra-data-feathers/commit/96268b21eb165495668fa6718f2f46c8de2bbc86)
- Contribuciones de Gabriel Maljkovich a  [heic-to-jpeg-middleware](https://github.com/boutell/heic-to-jpeg-middleware/pull/3), [apostrophe CMS](https://github.com/apostrophecms/apostrophe/pull/2226) y [PostHog](https://github.com/PostHog/posthog/pull/1245)
- Nuevo proyecto de Florencia Otarola (Cambá) y Pablo Pissi (Gaia) - [Arduindoor](https://github.com/pablop94/arduindoor) (MQTT, Mongo, Telegram, Node, React)


## Herramientas de software

- Métricas:
    - [Perfume](https://zizzamia.github.io/perfume/) - Biblioteca para obtener metricas de rendimiento basadas en usuarios
    - [Web Vitals](https://github.com/GoogleChrome/web-vitals) - Métricas escenciales para un sitio saludable
    - [Posthog](https://github.com/PostHog/posthog) - Analitics libres para Productos
- Software libre para Participación ciudadana de gobiernos
    - [Decidim](https://decidim.org/) - Democracia participativa de código libre para ciudades y organizaciones (Barcelona)
    - [Consul](https://consulproject.org/) - Software libre para la participación ciudadana. (Madrid)
    - [Compartiva Decidim vs Consul](https://xabier.barandiaran.net/2019/01/14/comparativa-decidim-vs-consul/)
- [Tauri](https://tauri.studio/) - Crea con tus HTML, CSS y JavaScript, aplicaciones nativas más rápida y seguras con instaladores en Win, Gnu/Linux y macOS.
- [Reactvue](https://github.com/antfu/reactivue) - Usando Vue Composition API en Componentes React
- [Pose Animator](https://github.com/yemount/pose-animator) - Animación de ilustración de vectores 2D en tiempo real reconociento los resultados de PoseNet y FaceMesh.

## Informes/Encuestas/Lanzamientos

- [Jitsi Wiki RIU](http://jitsiwiki.riu.edu.ar/)
- [Arduino cli 0.11.0](https://github.com/arduino/arduino-cli/releases/tag/0.11.0) - [Los tres pilares](https://blog.arduino.cc/2020/07/06/the-three-pillars-of-the-arduino-cli/)
- [Vue 3 Release Candidate](https://github.com/vuejs/rfcs/issues/189)

# Noticias, Enlaces y Textos

### El deseo de cambiarlo todo

- [Cine bruto, El pato, Berazategui](https://www.facebook.com/cinebruto) lanzó la pelicula [Bajo mi piel Morena](https://play.cine.ar/INCAA/produccion/6337), acá pueden escuchar la [entrevista al Director José Celestino Campusano hecha por Radio Ahijuna 94.7](https://ar.radiocut.fm/audiocut/cine-campusano-vida-debe-ser-rodada-sin-causar-danos-con-compromiso-y-cercania/)
- Proyectos que eliminan la terminología ofensiva de sus proyectos: [Git branching name](https://sfconservancy.org/news/2020/jun/23/gitbranchname/), [Otros proyectos](https://www.linuxadictos.com/github-y-otros-proyectos-eliminaron-la-terminologia-maestro-y-esclavo.html)
    - [Mensajes Lista de correos del linux kernel](https://public-inbox.org/git/20200615205722.GG71506@syl.local/))
    - [Merge en linux kernel: Documentación coding style](https://git.kernel.org/pub/scm/linux/kernel/git/torvalds/linux.git/commit/?id=49decddd39e5f6132ccd7d9fdc3d7c470b0061bb)

- [Alex](https://github.com/get-alex/alex) - Encuentra contenido escrito insensible o no considerado.


### Sociales/filosóficos


- [Etalab, departamento de asusntos digitales de Francia, muestra como el software libre puede ser disponibilizado desde y para el sector público](https://fsfe.org/news/2020/news-20200609-01.en.html)
- [Receta para escapar de la matrix y conseguir un estado de iluminación, según PHILIP K. DICK](https://pijamasurf.com/2017/03/como_salir_de_la_matrix_segun_philip_k_dick/)
- [La desaparición de los rituales -  Buyng-Chul Han por Claudio Alvarez Teran](https://www.youtube.com/watch?v=rlWn-tgIAfY)
- [Diez mandamiento del estoicismo](https://www.youtube.com/watch?v=EUO0tXbR4Po)
- El 18 de julio Murio Lucio Urtubia. [Entrevista](https://www.youtube.com/watch?v=U7RaMNiU0A4), [Libro autobiográfico](https://img.txalaparta.eus/Archivos/La_revolucion_por_el_tejado.pdf)

### Técnicos

- [Un linter para gobernarlos a todos](https://github.blog/2020-06-18-introducing-github-super-linter-one-linter-to-rule-them-all) - [Super-Linter](https://github.com/github/super-linter)
- [¿Qués es y como usar Sysdig (herramiento para tener visibilidad de contenedores)?](https://hackernoon.com/sysdig-what-it-is-and-how-to-use-it-3tn3ukh)
- [Rust para programadores JS](http://www.sheshbabu.com/posts/rust-for-javascript-developers-functions-and-control-flow/)
- [Intro con Sierpiński en 27 bytes](http://0x705h.com/demoscene/2020/07/27/jaja-keH-27bytes-msdos-intro.html)
- Flashparty 2020: [Resultados](http://flashparty.dx.am/index.php?option=com_content&view=article&id=74:flashparty-2020-after-es&catid=10&lang=es&Itemid=107), [Archivos](https://files.scene.org/browse/parties/2020/flashparty20/)

#### Seguridad

- [Tiktok scraper](https://github.com/drawrowfly/tiktok-scraper)
- [Instagram Hacks](https://github.com/cyberkallan/inshackle-bot)
- Scanner de conferencia de Zoom: [Tangalanga](https://github.com/elcuervo/tangalanga)
- Scanner de Vulnerabilidades JS: [jshole](https://github.com/callforpapers-source/jshole), [retirejs](https://retirejs.github.io/retire.js/)

### Otras

- [Presentación de Neto "Cooperatives: A resilient model in time of crisis and beyond" de la International Co-operative Alliance](https://www.youtube.com/watch?v=JOm0HDBh9fk&feature=youtu.be&start=998&end=1765)
- [Presentación de Neto "Innovative Business models with Cooperative Values" en Global Youht Forum - Malasia 2020](https://gyf20.coop/sessions/innovative-business-models-with-cooperative-values/)

