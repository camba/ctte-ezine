---
title: Junio 2024
---

# Cambá CTTE Ezine Junio 2024

<div style="text-align: center;">

![Espera](@img/junio2024.png)

</div>

```
En tus manos
Descansa el camino a seguir
Vamos, somos lo que queramos ser

Las banderas claman por la redención
El recurso armado o la rendición?
La mentira polariza la cuestión
Es la sinfonía de la perdición

La miseria es mantener los pueblos sin comer
Ya tu manos dicen: Hoy no!

¡Vamos! Que queda un camino a seguir
¡Vamos! Tiempos de revancha se inscribe en tu ser

Fuerzas divididas se enturbaron, hacia donde hay que ir
Cuando simplemente debe resurgir la acción

La llama encendió, alli donde la otra se apago
En el fuego alado esta la convicción
Tu sueño encarno la continencia del valor
La impaciencia estalla y se pone de pie
Ya no se va a caer

La utopía es descreer que esto va a caer
Todo se resume:

¡EN TU MANOS!
Descansa el camino a seguir
VAMOS, tiempo de revancha reescribe lo que es....
```

Letra del tema [En tus manos](https://www.youtube.com/watch?v=-io8idb2YWA) [Rodia](https://es.wikipedia.org/wiki/Rodia).

## Cooperativas/Economía Popular

- Cambá: [Colmena: Herramienta para la comunicación comunitaria](https://blog.camba.coop/colmena-una-herramienta-para-la-comunicacion-comunitaria/)
- Cambá: [Crónica del taller de desarrollo web en la "Escuela de Tecnología de las Asamblea de la Ribera"](https://ltc.camba.coop/2024/05/29/taller-de-desarrollo-web-en-la-escuelita-tecnologica-de-la-asamblea-campo-de-la-ribera/)
- Origenes: [Guitarra Led + Ignacia + josx en 5to Festival Misionero de Cultura y Software Libre 2012](https://vimeo.com/55292775)
- Origenes: [Guitarra Led 2 Neto, Nico y Josx](https://vimeo.com/51946509)


## Herramientas de software

- [Hashcat](https://github.com/hashcat/hashcat): La utilidad más avanzada para recuperar contraseña
- [Streamlit](https://github.com/streamlit/streamlit): Construir y compartir aplicaciones más rápido
- [Flask-Htmlx](https://pypi.org/project/flask-htmx/): Extensión Flask para trabajar con HTMX.
- [Wagtail](https://github.com/wagtail/wagtail): Django CMS con foco en la flexibilidad y en la experiencia de usuario
- [Auto-Linkedin](https://github.com/Ranork/Auto-Linkedin): Automatización de Linkedin (con Puppeteer)

## Informes/Encuestas/Lanzamientos/Capacitaciones

- Lanzmiento de nueva versión: [Node 22](https://openjsf.org/blog/nodejs-22-available)
- Serán software libre: [Winamp](https://about.winamp.com/press/article/winamp-open-source-code) y [Athina Crisis](https://cpojer.net/posts/athena-crisis-open-source)
- Encuesta: [State of Html 2023](https://2023.stateofhtml.com/)
- Radar Tecnología: [Tendencias tecnologicas 2024](https://www.oreilly.com/radar/technology-trends-for-2024/)
- Encuesta IA e Argentina: [¿Qué piensa la gente sobre la inteligencia artificial generativa?](https://reutersinstitute.politics.ox.ac.uk/what-does-public-six-countries-think-generative-ai-news)

## Noticias, Enlaces y Textos

### Tecnología

- ¿Queremos que google nos devuelva resultados basados en una IA?: [Explicación](https://tedium.co/2024/05/17/google-web-search-make-default/) y [Web para no usarla](https://udm14.com/) ([Fuente](https://github.com/readtedium/udm14))
 Demo de Javascript: [Explicación Ciudad en 256bytes](https://frankforce.com/city-in-a-bottle-a-256-byte-raycasting-system/) y ([Fuente](https://www.shadertoy.com/view/7dccRj))
- Bundler js: [Kuto, nueva técnia](https://samthor.au/2024/kuto/)
- Reemplazando a sudo: [Run0 de systemd](https://tech.slashdot.org/story/24/04/30/219252/systemd-announces-run0-sudo-alternative)
- Cosas para hacer con SDR (Radio definida por Software): [50 Cosas](https://blinry.org/50-things-with-sdr/)
-
### LLMs
- Actualidad de las empresas de IA: [Microsoft MASSIVE Announcements: GPT-5, Copilot+ PC, Phi-3, Devin Partnership](https://www.youtube.com/watch?v=6H8NPVGC6Ak)
- LLMs en la línea de comandos: [Llm](https://llm.datasette.io/en/stable/index.html)
- [Seguridad de la información en el universo de la Inteligencia Artificial](https://www.youtube.com/watch?v=Nh16evnXzkM)
- Límites a la IA: [Guia para commits de NETBSD](https://www.netbsd.org/developers/commit-guidelines.html)
- IA Local: [Binding para Node de llama.cpp](https://github.com/withcatai/node-llama-cpp)


### Música
> Murieron dos personalidades de la música
- Javier Martinez
    - [¿Quién es?](https://es.wikipedia.org/wiki/Javier_Mart%C3%ADnez)
    - [Procer, poeta y alquimista](https://www.pagina12.com.ar/734073-javier-martinez-procer-poeta-alquimista-del-blues)
    - [Una casa con 10 pinos](https://www.youtube.com/watch?v=h-5X10s5opg), [Descripción](https://es.wikipedia.org/wiki/Casa_con_diez_pinos)
- Steve Albini
    - [¿Quién es?](https://es.wikipedia.org/wiki/Steve_Albini)
    - [Industria de la música democrática](https://jacobin.com/2024/05/steve-albini-believed-in-a-democratic-music-industry/)
    - [Su trabajo e instalaciones](https://www.youtube.com/watch?v=NwkF3-JmSeA)
