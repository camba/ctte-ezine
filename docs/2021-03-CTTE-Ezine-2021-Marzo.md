---
title: Marzo 2021
---

# Cambá CTTE Ezine Mazro 2021

## Texto
```
Un doble enigma en realidad: cuerpo y tiempo. Los cuerpos no sólo
están ahí –aquí– ocupando con contundencia mi espacio, sino que están
ahora, sujetos a la mudanza. Es más: la mudanza es parte de su
deslumbrante contingencia. Los cuerpos tienen historia y eso los hace aun más
determinados, más inasibles. Han llegado a ser, y pueden dejar de ser,
perecer, quebrarse, marchitar. Pero, mientras son, son por completo y sin
dudas, y eso quiere decir que han triunfado sobre la nada. Un triunfo
provisorio, precario, pero un triunfo. En eso, justamente, radica el asombro de
lo real, en ese triunfo provisorio sobre la nada.
```

Extracto del texto ["De lo que sucedió cuando la lengua emigró de la boca"](http://www.lecturayvida.fahce.unlp.edu.ar/numeros/a20n3/20_03_Montes.pdf/view) escrito por Graciela Montes que se público en la [Revista Latinoamericana de lectura "Lectura y Vida"](http://www.lecturayvida.fahce.unlp.edu.ar/), [año 20, nº 3, 1999.](http://www.lecturayvida.fahce.unlp.edu.ar/numeros/a20n3/sumario)


## Contribuciones al Software libre de Cambá

- [Lettertracer](https://github.com/glmaljkovich/lettertracer) - Nuevo proyecto hecho con Go y Ebiten por Gabriel Maljkovich con el fin de ayudar a niñes a mejorar sus trazos sobre letras basado en los principios de la actividad de Montessori "sandpaper".

- Nuevo proyecto lanzado por FACTTIC para realizar Marchas Virtuales ([Plataforma](https://github.com/facttic/mv-platform), [Admin](https://github.com/facttic/mv-admin/), [Frontend](https://github.com/facttic/mv-front/), [Cron](https://github.com/facttic/mv-cron/), [API](https://github.com/facttic/mv-api/))

- Cambá en conjunto con varias cooperativas de FACTTIC lanzá [IA²](https://www.ia2.coop/) - ([ia2-cli](https://github.com/instituciones-abiertas/ia2-cli), [ia2-desktop-app](https://github.com/instituciones-abiertas/ia2-desktop-app), [ia2-server](https://github.com/instituciones-abiertas/ia2-server))


## Herramientas de software

- [Coy.im](https://coy.im/) - CoyIM es cliente de chat con foco en la seguridad.
- Repos Watchman bis es una aplicación que usa diferentes APIs para auditar software en la busqueda de datos sensibles y credenciales. ([Versión Gitlab](https://github.com/PaperMtn/gitlab-watchman), [Versión github](https://github.com/PaperMtn/github-watchman))
- [Annotorious](https://github.com/recogito/annotorious) - Generar Anotaciones sobre imagenes
- [Nitter](https://github.com/zedeus/nitter) -  Alternativa de frontend web para Twitter con foco en la privacidad.
- [Git-toolbelt](https://github.com/nvie/git-toolbelt) - Conjunto de comandos git para simplificar su uso diario



## Informes/Encuestas/Lanzamientos/Capacitaciones

- [LISTADO DE BBS DE ARGENTINA Y LATINOAMÉRICA](https://bbs.docksud.com.ar/list) - Actualizada al 2021
- Articulo sobre lanzamiento de Proyecto de EryxLabs - [Mora](https://elgatoylacaja.com/mora)
- Articulo sobre lanzamiento de [Lettertrace](https://blog.camba.coop/haciendo-una-app-educativa-para-nines-con-go-ebiten/)
- Eventos de Lanzamiento de IA²
    - [Web IA²](https://www.ia2.coop/)
    - [Evento Presentación oficinal](https://www.youtube.com/watch?v=Tox5pqZj4yI)
    - [Evento Presentación Show and tell](https://youtu.be/U70h97dpGJoP)


## Noticias, Enlaces y Textos

### Sociales/filosóficos

- [Es el blockchain Estupido](https://revistacrisis.com.ar/notas/es-el-blockchain-estupido)
- [9M "El género en el trabajo: hablemos de datos y desigualdad económica"](https://www.youtube.com/watch?v=buLWLjiKv0k)
- Explicación sore [Cryptoarte NFT](https://www.youtube.com/watch?v=liSlThAG7Po)

### Técnicos

- [MSTG](https://github.com/OWASP/owasp-mstg) - La guía para testear la seguridad de aplicaciones móviles. [Más info](https://owasp.org/www-project-mobile-security-testing-guide/)
- [Hacete tu amplificados punga bueno y barato](https://rebelion.digital/2021/02/28/amplificador-de-audio-punga-bueno-y-barato/)- Código y tutorial que generamos hace 2 años de como hacer una Dapp con ethereum ([Documentación general](./assets/Ethereum.pdf), [Repo](https://recursos.camba.coop/camba/camba-token/))


### Cooperativas

- Creación de las zonas especiales coop.ar y mutual.ar - [Boletin Oficial](https://www.boletinoficial.gob.ar/detalleAviso/primera/241460/20210304?busqueda=2)
- Participamos en [Plantamos Memoria](https://plantamosmemoria.com.ar/) - [Nota página 12](https://www.pagina12.com.ar/330978-en-el-dia-de-la-memoria-la-marcha-volvera-a-ser-reemplazada-)
- [Los 10 años de Gcoop](https://osiux.com/2021-02-16-vivir-del-software-libre.html)


### Otras

- [Recordando parte del origen de Cambá (8 años atrás) en Frecuencia zero, Fabricando alternativas](https://www.youtube.com/watch?v=fT-QCGcUU4I)

