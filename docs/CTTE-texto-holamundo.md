# Hola Mundo en C

> Texto extraido del libro "La clase peligrosa", escrito por [Juan Grabois](https://es.wikipedia.org/wiki/Juan_Grabois)

## Texto en partes de página 102 y 103

La redes sociales también operan como una válvula de escape al aislamiento
y al bloqueo comunicacional que implica el régimen oligopólico de propiedad
sobre los medios masivos de comunicación audiovisual. Postear algo, aunque
tenga menos impacto que gritar en la calle, produce la sensación de cierta
conectividad, como si estuvieras hablando con el mundo. No importa que
repitas la más trillada de las frases. Decirla vos mismo a un público
indeterminado te da la misma sensación de que es tu propia posición. Se me
ocurre que eso está en el ADN de la informática porque cuando uno aprende
a programar, al menos en lenguaje C, el primer ejercicio de mi viejo manual
Kernighan-Ritchie es:

```
#include <stdio.h>
int main()
{
printf("Hola mundo");
return 0;
}
```

Hablarle al mundo es un sueño viejo, tan viejo como el grito o el arte.
Algo que ahora se puede hacer realidad firmando un contrato digital con
alguno de los pulpos de internet a cambio de tu alma, o para ser más
precisos, de parte de tu identidad, de tu personalidad. Luego del pacto
fáustico, tendrás la sensación de que tu <<"hola mundo">> está en algún lado
para siempre, siempre joven, y a medida que tu burbuja de contactos se
alargue y logres algunos signos de aprobación, pequeños estímulos
placenteros irán reforzando tu dependencia de ese mundo virtual del mismo
modo que las recompensas diseñadas para las ratas de laboratorio en los
experimientos conductista.


## Otra historias

> Este libro lo leí hace un tiempo, pero ayer 09/09/19 iba en el tren Roca y
cuando se corto la energía eléctrica un señor mayor que yo, saco un este
libro y justo pude releer de costado un cacho de código en C, que hoy busque
en mi libro.

> En la lectura atenta de este texto y al ver que el código fuente explicitado
no compilaba, envíe parche de código a Juan Grabois vía facebook, en donde
decía <<"Hola Mundo">> en vez del "Hola Mundo" necesario.
