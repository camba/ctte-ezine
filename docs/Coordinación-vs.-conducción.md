Muchas veces, nos vemos con dificultad de *nombrar* los roles de la cooperativa que no están asignados a proyectos o actividades específicas, pero que actúan en relación a todas las areas, muchas veces, definiendo rumbos o tomando decisiones "chicas" (no aquellas estratégicas definidas en asamblea). El espacio de los lunes por la mañana, donde confluyen varias areas y roles e intercambian el estado de las distintas iniciativas de Cambá, también muchas veces es un espacio de decision, que se pueden ejecutar directamente, u otras veces, de propuestas que se llevan a la asamblea.

En muchas organizaciones de la ESS, este espacio es el del Consejo de Administración, que se encarga de llevar a cabo el plan estratégico definido y aprobado previamente en asamblea. En organizaciones políticas existe muchas veces una "mesa chica" que es la que se encarga de resolver las cuestiones operativas (y muchas veces las decisiones estratégicas) de estos espacios. La palabra que define en general esto es **Conducción**.

Una de las búsquedas de Cambá, definida en su Misión es *"Desarrollar un modelo de empresa democrática y autogestionada guiada por valores solidarios."* En ese sentido, la búsqueda de la democracia real y la autogestión, choca con la idea de *conducción*, es decir, un *grupo* que *conduce* a otro *grupo* más grande, ya que impide la verdadera participación (entendida como conocimiento de los temas sobre los que hay que tomar una decisión + poder de decisión).

Sin embargo, la práctica nos demostró que es muy difícil tomar decisiones operativas en grupos muy grandes de personas (por ejemplo, una asamblea), y que muchas veces se necesita una velocidad de reacción alta ante ciertas situaciones o la capacidad de profundizar en estudios o debates. Es por eso que en Cambá estamos armando grupos pequeños de personas que trabajan sobre ciertos temas (las "comisiones") con un grado todavía no definido claramente de autonomía, pero que somenten el resultado de su trabajo a la asamblea.

En este sentido, en oposición a la idea de "Conducción", surge la de "Coordinación", que sugiere la capacidad de dar respuestas operativas a las directivas del colectivo.

Por otro lado, muchas veces cuando hablamos de autogestión, surge el problema de como nos aseguramos de que todxs estamos realizando el mismo esfuerzo, ahí aparece el debate acerca del "control", vs. la confianza en la autogestión. Quizás, algo que se puede oponer a esta idea de control cuasi "policíaco" (controlar horarios, controlar tareas, etc.), son indicadores que se deben cumplir, ya que este tipo de control también exige muchisimo tiempo de parte del "controlador": para definir que algo no se hizo, o se hizo mal, tengo que estar al tanto del detalle de esa tarea.

Aporto para complementar esta idea, la tesis de Veronica Xhardez, socióloga y activista del SL acerca de las ideas de "control" y "coordinación" según la forma en que se organiza el emprendimiento (empresa de capita, cooperativa, artesanos):

# Formas de coordinación de las divisiones del trabajo y redes sociales

Todo trabajo social y sus consecuentes divisiones del trabajo suponen el ejercicio
de alguna forma de coordinación a los efectos de su materialización. Ante la imposibilidad de ubicar un modelo que nos permita analizar los procesos de coordinación de las divisiones del trabajo en formas productivas diferentes de la capitalista, partiremos de la propuesta de Edwards referida a esta última, no sin antes aclarar que son las más comúnmente analizadas.

Edwards (1979 en Roldán, 2000) nos advierte que es posible encontrar diferencias
en el ejercicio de la coordinación de las divisiones del trabajo según sea su imbricación en distintas formas de producción social. “En la producción capitalista (...) la coordinación que siempre es necesaria, toma la forma de una coordinación impuesta, dirigida desde arriba hacia abajo, para cuyo ejercicio los de “arriba” (capitalistas) deben poder controlar a los de “abajo” (trabajadores/as). Por lo tanto —concluye Edwards— es más apropiado hablar de control que de coordinación aunque, por supuesto, el control es una forma de coordinación.

Esta última no necesita coerción pero el control sí.” (Roldán, 2000, pág. 78, citando a
Edwards). El control en la producción capitalista se explica porque en situaciones donde los trabajadores no dominan de manera directa el proceso de trabajo (como sí sucede en el caso de la PSM y en el ejemplo híbrido de esta tesis) “y no pueden hacer de eso una experiencia creativa cualquier esfuerzo más allá del mínimo necesario para evitar aburrimiento no sería en el interés de los propios trabajadores. (...) La discrepancia entre la fuerza de trabajo que el capitalista compra en el mercado y la que necesita para producir hace imperativo el control del proceso de trabajo y las actividades del trabajador.” (Ibídem, negritas en el original) 16 .

Desde esta concepción teórica, Edwards define el control como “la habilidad de los
capitalistas y/o de los gerentes de obtener el comportamiento de trabajo deseado por parte de los trabajadores.” (Ibídem). Según esta perspectiva, el ejercicio del control en este tipo de producción es a la vez necesario y problemático dado que los trabajadores se resisten a ser tratados como mercancía (a través de huelgas, trabajo a desgano, etc.)
No obstante, existen otros tipos de coordinación diferentes del control capitalista, “la tradición y la transmisión de conocimiento de maestro a aprendices es una de ellas; la
iniciativa de los mismos productores en una discusión directa sobre la armonización de sus tareas en formas cooperativas y comunales es otra.” (Ibídem, pág. 77-78).

En efecto, en todo proceso productivo donde existen divisiones del trabajo existe
algún grado de cooperación y es importante reconocer que la cooperación ya fue planteada en los trabajos de Marx (1991, pág. 395) como “la forma de trabajo de muchos obreros coordinados y reunidos con arreglo a un plan en el mismo proceso de producción o en procesos de producción distintos.” 17 Esta concepción de Marx presenta la idea que la práctica de la cooperación no sólo potencia la fuerza productiva del trabajo sino que se transforma ella misma en nueva fuerza productiva cuyo andamiaje es el uso social de los recursos disponibles.


Desde una perspectiva contemporánea también son de utilidad los aportes de Herskovitz para quien la cooperación es “una asociación voluntaria de un grado de hombres y mujeres que se proponen como objetivo llevar a cabo una tarea específica y definidamente limitada que a todos les incumbe simultáneamente” 18 (en Carozzi et al, 1991, pág. 140). Según Herskovitz la base de la cooperación está dada por las relaciones y lazos sociales que se generan —y que aseguran reciprocidad futura— más que por las ventajas materiales de esta asociación. Por su parte, Rheingold (2002) cita las definiciones del joven científico social Marc Smith quien advierte que los grupos de acción colectiva que cooperan voluntariamente
en comunidades a través de Internet, lo hacen en búsqueda de “capital de red social, capital de conocimiento y comunión” (Ibídem, pág. 58).

Al igual que el caso analizado en esta tesis, se trataría de objetivos dirigidos a consolidar las relaciones sociales generadas y no los intereses materiales potenciales que surgen de la misma cooperación. A los efectos de nuestra elaboración, cabe mencionar que en este mismo libro Rheingold indica —esta vez citando a Olson— que: “La reputación muchas veces es un leitmotiv recurrente en el discurso de la cooperación.” (Ibídem Pág. 63)

A partir de lo expuesto surge este interrogante: ¿Cómo analizar los procesos de
coordinación de las divisiones del trabajo en una organización voluntaria como la
estudiada? Este abordaje implica un ejercicio complejo según se elabora a continuación.

Acá la tesis completa:
[Trabajo_en_red_mediada_por_TICs__forma_productiva_y_representaciones_sociales.pdf](Trabajo_en_red_mediada_por_TICs__forma_productiva_y_representaciones_sociales.pdf)


# Notas

El control es una forma de coordinación.

Formas de coordinación:
- La tradición y la transmisión de conocimiento de maestro a aprendices
- La iniciativa de los mismos productores en una discusión directa sobre la armonización de sus tareas en formas cooperativas y comunales

> La práctica de la cooperación no sólo potencia la fuerza productiva del trabajo sino que de transforma ella misma en nueva fuerza productiva cuyo andamiaje es el uso social de los       recursos disponibles.

> Una asociación voluntaria de un grado de hombres y mujeres que se proponen como objetivo llevar a cabo una tarea específica y definidamente limitada que a todos les incumbe simultáneamente

> La base de la cooperación está dada por las **relaciones y lazos sociales** que se generan y que aseguran **reciprocidad futura** más que por las **ventajas materiales** de esta asociación.

> Trataría de objetivos dirigidos a **consolidar las relaciones sociales** generadas y no los **intereses materiales** potenciales que surgen de la misma cooperación.
