---
title: Febrero 2025
---

# Cambá CTTE Ezine Febrero 2025

<div style="text-align: center;">

![Febrero 2025](@img/febrero2025.jpg)

</div>

```
Amigo, no llores por las noches
Es hora de buscar lo esencial
Nena, ayer fueron muy duros tus reproches
No importa, más o menos, todo sigue igual
Ma, no te preocupes tanto
Todo va a estar más o menos bien
Pa, necesito un poco de plata
Para que todo siga más o menos bien
Más o menos bien Más o menos bien
Más o menos bien Más o menos bien
Amigos, formemos una banda de rock and roll
Guitarras guardadas en el placard
Ahora somos nuevos creadores de rock and roll
Tranquilos, todo va a estar más o menos bien
Más o menos bien Más o menos bien
Más o menos bien Más o menos bien
Desconocido, espero tus problemas se acaben
Y así volver a la senda del bien
Desconocido, dobla tu energía en partes iguales
Y todo va a estar más o menos bien
Mirando la comida ya fría
No creo que esté hecha con amor
No importa, hoy celebraremos como familia
Que, más o menos, sigue como quiero yo
Más o menos bien Más o menos bien
Más o menos bien Más o menos bien
Más o menos bien Más o menos bien
Más o menos bien Más o menos bien
Más o menos bien
```

Letra de la canción [**Más o menos bien**](https://www.youtube.com/watch?v=7ejQ_CL_dWA) de la banda [Él Mató a un Policía Motorizado](https://es.wikipedia.org/wiki/%C3%89l_Mat%C3%B3_a_un_Polic%C3%ADa_Motorizado)


## Herramientas de software
- [MCP framework](https://github.com/QuantGeekDev/mcp-framework): Framework para escribir servidores modelcontextprotocol (MCP) en Typescript
- [MitProxy2Swagger](https://github.com/alufers/mitmproxy2swagger): Ingenieria reversa para REST APIs capturando trafico
- [OhmJs](https://ohmjs.org/): Libreria Javascrpt para crear parsers, interpretes, compiladores y mucha más
- [No More Secrets](https://github.com/bartobri/no-more-secrets): Recrear el efecto de la pelicula sneakers de 1992
- [Maid](https://github.com/Mobile-Artificial-Intelligence/maid): Maid es una App en flutter para interactuar con modelos GGUF/llama.cpp locales, y con Ollama y OpenAI remotos

## Informes/Encuestas/Lanzamientos/Capacitaciones
- [Videos del evento 38C3: Illegal Instructions](https://media.ccc.de/b/congress/2024): La edición 38 del Chaos Communication Congress Hamburgo, 27-30.12.2024
- [Nuevo informe de la EFF ofrece orientación para garantizar la protección de los derechos humanos en el uso gubernamental de la IA en América Latina](https://www.eff.org/deeplinks/2024/10/new-eff-report-provides-guidance-ensure-human-rights-are-protected-amid-government)
- [Muere Christensen Ward el inventor de los BBS](https://arstechnica.com/gadgets/2024/10/ward-christensen-bbs-inventor-and-architect-of-our-online-age-dies-at-age-78/)
- [Inventario de Tecnología Lowtech](https://emreed.net/LowTech_Directory)
- [Libro Desobediencia tecnologica - La permanencia de lo temporal en Cuba](https://www.technologicaldisobedience.com/2025/01/12/libro-desobediencia-tecnologica-la-permanencia-de-lo-temporal-en-cuba/)

## Noticias, Enlaces y Textos

### Sociales/filosóficas
- Entrevista a Franco ‘Bifo’ Berardi por Jorge Fontevechia
    - Parte 1 ["Los votantes de Javier Milei son jóvenes frustrados e impotentes"](https://www.youtube.com/watch?v=LWAhRG0xnXo)
    - Parte 2 ["La libertad que habla Milei es la de mercantes de esclavos"](https://www.youtube.com/watch?v=k_TQ-Xwxl2g)
- Métodos de votación
    - [¿Cómo es el sistema de votaciones del proyecto Debian?](https://www.debian.org/vote/)
    - [Método Condorcet](https://es.wikipedia.org/wiki/M%C3%A9todo_de_Condorcet)
    - [Más Info](https://www.youtube.com/watch?v=IyAjvydddcw)
- [¿COMUNISMO LIBERTARIO? | ANARCOCOMUNISMO por Kropotkin, Bakunin y Malatesta](https://www.youtube.com/watch?v=ijULx84HAA0)
- [MAURICIO KARTUN | Con Lalo Mir en Doble Casetera #4](https://www.youtube.com/watch?v=GodHlpN63b4)

### Tecnología
- [Tetris en un PDF](https://th0mas.nl/2025/01/12/tetris-in-a-pdf/)
- [25 años de Inteligencia Artificial](https://austinhenley.com/blog/25yearsofai.html)
- [Insistir a un LLM que escriba mejor código escribe mejor código](https://minimaxir.com/2025/01/write-better-code/)
- [Proyectos de Software libre bombardeados por llamadas de los bots de entrenamiento de LLMs](https://pod.geraspora.de/posts/17342163)

### LLM
- [Destilación y Cuantificación de Modelos LLM](https://medium.com/aimonks/what-is-quantization-and-distillation-of-models-a67e3a2dc325) - [Comparación](https://medium.com/@aadityaura_26777/quantization-vs-distillation-in-neural-networks-a-comparison-8ef522e4fbec)
- [Corriendo llama en un windows 98](https://blog.exolabs.net/day-4/)
- [MidScene](https://midscenejs.com/): [Video explicativo de Uso](https://www.youtube.com/watch?v=fGZrurOHx70)
- [Deepseek R1](https://www.youtube.com/watch?v=X5adgxV0gBE): El modelo Chino de IA que conmociono a toda la industria
