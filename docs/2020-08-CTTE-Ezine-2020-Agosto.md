---
title: Agosto 2020
---

# Cambá CTTE Ezine Agosto 2020

## Canción

```
Ando un poco para adentro
Esperando que algo pase
Ando desilusionado
Y sin ganas de sentir
Ando buscando una canción
Que acorte las distancias
Entre este mundo y yo

Ando un poco paranoico
Y con miedo a que me toquen
Ando desilusionado
Y sin ganas de salir
Ando sintiendo que el dolor
Ya es parte de esta vida
Y lo aceptamos por que si

Es preciso que me saque
Toda la tensión de el alma
Es preciso que me afloje
Y te mire a los ojos
Es preciso que me arranque
Toda esta seriedad que ves
Ya no me deja ver

Salir afuera, que afuera sale el sol
Y no te olvides que no sos un robot
Ya no dejes que controlen más tu vida
El miedo esta en la mente.

Y te vas poniendo viejo
Y te vas endureciendo
El silencio te acompaña
Y tus sueños ya no son
Paste de un póster que quedo
Mal colgado en la pared del cuarto
Que te vio soñar

Y me niego a creerme
Que tengo que tener miedo
A la gente diferente
Y es que pienso que tu
Idea de seguridad
Es la cosa más absurda
Y egoísta que escuche

```
[Canción **Miedo** de la banda **Sancamaleón**](https://www.youtube.com/watch?v=joj6vgXv6aE)


## Proyectos de Software libre

- Nuevo proyecto de Leonardo Vaquel (Cambá) - [Kintun](https://github.com/TTIP-UNQ-Team10), plataforma web de tipo marca blanca que permite, de manera estandarizada, realizar el mapeo de distintas necesidades que una entidad u ONG requiera.


## Herramientas de software

- [Bpytop](https://github.com/aristocratos/bpytop) - Monitor de recursos para terminal
- [Dash](https://dash.plotly.com/introduction) - Framework para hacer hacer visualización de datos en web con Python y React.
- [Qawolf](https://www.qawolf.com/) Creación de test para browser 10 veces más rápido - Converti tus acciones a código Playwright/Jest.
- [Playwright](https://playwright.dev/) Playwright es una libreria Node.js para automatizar Chromium, Firefox y Webkit de manera unificada.
- [Bladecoder](https://github.com/bladecoder/bladecoder-adventure-engine) - Motor para hacer juegos de aventura (point and click)
- [Linux Fake background webcam](https://github.com/fangfufu/Linux-Fake-Background-Webcam) - ¿Cómo tener fondos en la webcam?

## Informes/Encuestas/Lanzamientos

- [Liberación de Aplicación **Cuidar**](https://github.com/argob/cuidar-android/issues/14) con una [licencia GPL3](https://github.com/argob/cuidar-android/blob/master/LICENSE)
- La versión del kernel de Linux 5.8 (la estable actual) es una de los mejores releases de todos los tiempos. Email de [Linus Torvalds](https://lore.kernel.org/lkml/CAHk-=whfuea587g8rh2DeLFFGYxiVuh-bzq22osJwz3q4SOfmA@mail.gmail.com/)
- Lanzamiento de primeras versiones de [Business Tracker](https://gitlab.com/novawebdevelopment/business-tracker), software libre para hacer facturas, contabilizar tiempos de trabajo, etc (Hecho por [NOVA Web Development Co-op](https://novawebdevelopment.org/), Video de la [ShowAndTell](https://na66.distancelearning.cloud/playback/presentation/2.0/playback.html?meetingId=9db03309b42d637e676c1417bdc165b466df2da8-1597845964729) )

# Noticias, Enlaces y Textos

### El deseo de cambiarlo todo

- [Breve intro al flasherito y reescritura de articulo de Donna J. Haraway](CTTE-texto-flasherito-haraway)
- [Entrevista a Rita Segato](CTTE-texto-entrevista-rita-segato)

### Sociales/filosóficos

- [El nuevo wikileaks](https://www.pagina12.com.ar/284041-el-nuevo-wiki-leaks) - [Ddosecrets](https://ddosecrets.com/data/south_america/)
- [Construyendo una nueva economía: Covid y las finanzas de los sectores claves de cooperativismo](https://www.thenews.coop/149328/sector/energy/building-a-new-economy-covid-and-the-finances-of-key-co-op-sectors/)
- Libro para descargar hecho en conjunto por Siglo xxi editores y revista crisis, [La vida en suspenso: 16 hipótesis sobre la Argentina irreconocible que viene](http://dev2020.sigloxxieditores.com.ar/wp-content/uploads/2020/07/Siglo-XXI_Crisis.-La-vida-en-suspenso.pdf)


### Técnicos

- Construi tu propio shell para gnu/linux [1](https://hackernoon.com/lets-build-a-linux-shell-part-i-bz3n3vg1) [2](https://hackernoon.com/building-a-linux-shell-part-ii-a-step-by-step-guide-pk203ywg) [3](https://hackernoon.com/building-a-linux-shell-part-iii-wzo3uoi) [4](https://hackernoon.com/building-a-linux-shell-part-iv-h21o3uwl) [5](https://hackernoon.com/building-a-linux-shell-part-v-k61a3uai) [Repo](https://github.com/moisam/lets-build-a-linux-shell)
- [Flipper Zero — Tamagochi for Hackers](https://www.kickstarter.com/projects/flipper-devices/flipper-zero-tamagochi-for-hackers)
- [Automatiza a tu alrrededor con Playpush](https://medium.com/swlh/automate-your-house-your-life-and-everything-else-around-with-platypush-dba1cd13e3f6) - [Repo](https://github.com/BlackLight/platypush)
- [Baño de realidad para afirmaciones técnicas de moda](https://github.com/hwayne/awesome-cold-showers)

#### Seguridad

- Presentación "Defendiendocontenedores como una ninja..." de [Sheila Berta @UnaPibaGeek en Black hat](https://i.blackhat.com/USA-20/Wednesday/us-20-Berta-Defending-Containers-Like-A-Ninja-A-Walk-Through-The-Advanced-Security-Features-Of-Docker-And-Kubernetes.pdf)
- [Cursed Chrome](https://github.com/mandatoryprogrammer/cursedchrome) - Extensión para Chrome/Chromium para utilizar el browser como un proxy por otras personas.
- [Filtrado de datos sensible sobre covid en Argentina](https://www.comparitech.com/blog/information-security/argentina-covid-permit-data-leak/) [1](https://www.infotechnology.com/online/Escandalo-internacional-se-filtraron-datos-sensibles-de-100.000-argentinos-con-la-app-del-coronavirus-20200807-0006.html)


### Otras

- Presentación: "Arte, Educación y Software Libre" por Belen Sanchez y Neto Licursi (Cambá, Argentina) en el marco de las Jornadas de Reflexión: ARTE ELECTRÓNICO Y EDUCACIÓN. [Video](https://www.youtube.com/watch?v=qG7MmBzBZug)
- Columna de Tecnología de Sonámbules en [FM Las CHACRAS 104.9](https://radiolaschacras.blogspot.com/)
  Entrevista de Neto Licursi a Alvaro Chaparro sobre Cooperativismo de Plataformas - [Audio](https://archive.org/details/sonambules_120820_alvaro_chaparro_platcoop)
