---
title: Agosto 2023
---

# Cambá CTTE Ezine Agosto 2023

<div style="text-align: center;">

![Libros](@img/agosto2023.jpg)

</div>

```
Hace un ratito que estás por venir
Ya me estoy por ir,
ya casi me quedo.

Me muero por oírte sonreír
No te quiero ver
a veces me pierdo.

Peleamos todo el tiempo por mostrar,
quién se quiere más
quién extraña menos.

Lo bueno de tenerte siempre ahí
es como sentir que te me estás yendo.

Nada mas puedo ser el vaivén entre pasión y sacrificio
suelo andar ya, me ves,
jugando al tentempié en el precipicio.

Sos todo lo que siempre soñé,
ya me desperté y sigo dormida.
Todo el futuro depende de vos,
sola estoy mejor puedo con el día

Nada más puedo ser el vaivén entre pasión y sacrificio.
Suelo andar ya me ves,
jugando al tentempié en el precipicio.

Suelo andar, ya me ves,
jugando entre pasión y sacrificio.
Balancearse , dudar,
sin caer y sin poder cambiar de sitio
```

Letra de la canción [Tentempié](https://www.youtube.com/watch?v=dzbb4q75RXs) de [Ana Prada](https://es.wikipedia.org/wiki/Ana_Prada)

## Cooperativas/Economía Popular
- [Plenario Facttic](https://facttic.org.ar/2023/07/06/un-encuentro-para-fortalecer-el-cooperativismo-tecnologico/) - Encuentro en RCT Chapadmalala, “Cooperativismo y tecnología para la democracia”
- [Cursos Ambientales del Centro Local de Innovación y Cultura (CLIC)](https://ltc.camba.coop/2023/07/06/cursos-ambientales-del-centro-local-de-innovacion-y-cultura-clic) - Cambá trabajo en conjunto con el Municipio de Quilmes y la UNQ durante 10 semanas.


## Contribuciones Software libre
> Proyecto Colmena una nueva oportunidad para ir colaborando con lo que vamos necesitando y encontrando al paso.
- [Openapi-client-axios](https://github.com/anttiviljami/openapi-client-axios/pull/165)
- [Django-organizations](https://github.com/bennylope/django-organizations/pull/250)
- [nextcloud-API](https://github.com/luffah/nextcloud-API/pull/11)
- [Nextcloud-async](https://github.com/aaronsegura/nextcloud-async/pull/15)

## Herramientas de software
- [Trogon](https://github.com/Textualize/trogon) - Converti tu CLI eb una aplicación de terminal
- [Gw-Basic](https://codeberg.org/tkchia/GW-BASIC) - Dialecto del lenguaje de programación BASIC paras PC (Inlcuido en MSDOS)
- [Mod_wasm](https://github.com/vmware-labs/mod_wasm) - Correr WebAssembly con Apache
- [Tarmac](https://tarmac.gitbook.io/tarmac/) - Construi microservicios Serverless con funciones en WebAssembly
- [Yew]( https://github.com/yewstack/yew) - Rust / Wasm framework para crear aplicaciones web clientes

## Informes/Encuestas/Lanzamientos/Capacitaciones
- [Obituario de "Kevin David Mitnick"](https://www.dignitymemorial.com/obituaries/las-vegas-nv/kevin-mitnick-11371668) - Hacker, cracker y phreaker Estadounidense - info [wikipedia](https://es.wikipedia.org/wiki/Kevin_Mitnick)
- [Lanzamiento de Thunderbird 115 "Supernova"](https://blog.thunderbird.net/2023/07/our-fastest-most-beautiful-release-ever-thunderbird-115-supernova-is-here/)
- [Freebsd Cumple 30 años](https://issue.freebsdfoundation.org/publication/?m=33057&i=794483&p=24&pp=1&ver=html5)
- [Vueconf-us-2023](https://www.vuemastery.com/conferences/vueconf-us-2023/) - Videos de todas charlas del evento


## Noticias, Enlaces y Textos

### Tecnología
- [Rust - Ficción y hechos](https://opensource.googleblog.com/2023/06/rust-fact-vs-fiction-5-insights-from-googles-rust-journey-2022.html)
- [Prediciendo partidos de futbol con Arduino y tinyml](https://blog.arduino.cc/2023/06/28/predicting-soccer-games-with-tinyml-on-the-uno-r4-minima/)
- [Tirando la toalla luego de 30 años de hostear mi email]( https://cfenollosa.com/blog/after-self-hosting-my-email-for-twenty-three-years-i-have-thrown-in-the-towel-the-oligopoly-has-won.html)
- [Wasmati: Escribiendo Webassembly en Typescrypt](https://www.zksecurity.xyz/blog/posts/wasmati/)


### Sociales/filosóficas
- [Juan Grabois: tecnología & cripto con presidenciables](https://www.youtube.com/watch?v=9s74dWh3D-w)- Juan Grabois: tecnología & cripto con presidenciables
- [Yuval Noah Harari con Lex Fridman](https://www.youtube.com/watch?v=Mde2q7GFCrw) - Naturaleza Humana, inteligencia, poder y conspiraciones


### LLM
- [Langchain](https://github.com/langchain-ai/langchain) - Crear aplicaciones con LLMs con estrategia diseño de composición.
- [OpenLLM](https://github.com/bentoml/OpenLLM) - Operación de LLMs en producción.
- [LLaMA 2](https://ai.meta.com/llama/) - Segunda versión de Meta del LLMs - [¿Es el ChatGPT Killer?](https://www.youtube.com/watch?v=E-WOR6jfBLo)
- [PentestGPT](https://github.com/GreyDGL/PentestGPT) - Herramienta para test de penetración con GPT
- [Gpt-Engineer](https://github.com/AntonOsika/gpt-engineer) - Que queres hacer, la AI te pregunta para clarifar y luego lo construye.
