---
title: Octubre 2023
---

# Cambá CTTE Ezine Octubre 2023

<div style="text-align: center;">

![Libros](@img/octubre2023.jpg)

</div>

```
Suena brisa de incertidumbre, nuestras hojas bailan.
alguna lluvia encontramos, mas no fué inesperada.
positiva expectativa, en sosiego nos mantiene.
caminamos un sendero, de colores entrelazados.
la primavera nos alerta, hay desafíos aguardando.

desde un segundo piso, las calles observamos.
distantes a la realidad, refugiades nos sentimos.
destellos en el horizonte, corazones iluminan.
el verano nos invita, fijemos nuestro rumbo.
sobre idilia discutimos, soslayando enfrentamiento.

bajo abrigo de otoño, nuestros hogares nos reciben.
tras la tormenta hojas caen, inestabilidad resuena.
desde lejanías inmensurables, despedimos en silencio.
frío viento en lo profundo, suspira lluvia que no llega.
de tempestades aprendimos, no bajamos nuestros brazos.

las nubes se despejan, nocturno el cielo que encontramos.
el amanecer comienza, un suspiro nos regala.
con amable calidez, el invierno nos alberga.
gozando lucidez, nuestras diferencias abrazamos.
nos queremos reencontrar, no estemos esperando.

volvemos a florecer, con la primavera comenzando.
```

**Texto escrito por nuestra compañera Estefania Prieto que utilizamos hace 3 años para abrirnos camino a las Jornadas de Reflexión de Primavera 2020.**

## Cooperativas/Economía Popular
- [La Escuela Alcal visita la casita de Cambá](https://ltc.camba.coop/2023/09/28/casita-a-puertas-abiertas-visita-de-la-escuela-alcal/)

## Herramientas de software
- [KeyDecoder](https://github.com/MaximeBeasse/KeyDecoder): App para decodificar las llaves en segundos
- [Drizzle ORM](https://github.com/drizzle-team/drizzle-orm): TypeScript ORM que se siente como escribir SQL
- [Gancio](https://gancio.org/): Agenda compartidad para comunidades locales
- [EventoL](https://github.com/eventoL/eventoL): Software para manejo de eventos (inicialmente hecho para los FLISoles)
- [Sli.dev](https://sli.dev/): Presentaciones con Slides para desarrolladores

## Contribuciones Software libre
- [GEF (GDB Enhanced Features): Reordenamiento de carga de configuración](https://github.com/hugsy/gef/pull/1004)

## Informes/Encuestas/Lanzamientos/Capacitaciones
- [Thoughtworks Radar de Tecnologias #29](https://www.thoughtworks.com/content/dam/thoughtworks/documents/radar/2023/09/tr_technology_radar_vol_29_es.pdf).
- [Videos del campamento CCC 2023](https://media.ccc.de/b/conferences/camp/2023)
- [Entrevista a Cory Doctorow, "La Estafa de Internet"](https://juanbrodersen.substack.com/p/entrevista-con-cory-doctorow-la-tecnologia)
- [Cambiamos de licencia el ezine a Licencia de producción de pares feminista](https://labekka.red/licencia-f2f/):
> El cambio fundamental con respecto a la anterior licencia es que "La explotación comercial de esta obra sólo está permitida a cooperativas, organizaciones y colectivos sin fines de lucro, y organizaciones de trabajadoras autogestionadas, que se identifiquen y organicen bajo principios feministas. Todo excedente o plusvalía obtenidos por el ejercicio de los derechos concedidos por esta licencia sobre la obra no pueden ser acumulados o utilizados para la especulación y deben ser reinvertidos en la lucha contra el cisheteropatriarcado y el capitalismo."

## Noticias, Enlaces y Textos

### Sociales/filosóficas
- **Hack&Pop**: Mini Documental de [LaVacaTV](https://www.youtube.com/@lavacaTV) de 5 capítulos sobre la Informática con testimonios locales. (en donde Cambá colaboro). [#1](https://www.youtube.com/watch?v=_4DA6K84tZM), [#2](https://www.youtube.com/watch?v=vnC2ofcZTuM), [#3](https://www.youtube.com/watch?v=7tGJNW2eyPg), [#4](https://www.youtube.com/watch?v=epP-FTOIIrk), [#5](https://www.youtube.com/watch?v=Y5xnwVDUjVk)
- [¿Qué Podemos Hacer?](https://www.youtube.com/watch?v=xu70pa2gMfM)
- Nuevo Libro de [Tiziana Terranova](https://en.wikipedia.org/wiki/Tiziana_Terranova) con Tinta Limón, [Cultura de la red, Información, política y trabajo libre](https://tintalimon.com.ar/libro/cultura-de-la-red/)
- [¿Cómo piensan las nuevas generaciones? - "GENERACIÓN POST ALFA". Franco "Bifo" Berardi](https://www.youtube.com/watch?v=4fHS2d2dOBY)


### Tecnología
- [Rust en mi aplicación Node](https://gal.hagever.com/posts/my-node-js-is-a-bit-rusty)
- [Introducción a las Monadas](https://www.youtube.com/watch?v=C2w45qRc3aU)
- [ZenBleed](https://www.youtube.com/watch?v=9EY_9KtxyPg)
- [Convertí un teclado en mouse](https://suricrasia.online/blog/turning-a-keyboard-into/)


### LLMs
- [¿Los LLMs sirven para todo?](https://explosion.ai/blog/against-llm-maximalism)
- [StarCoder](https://github.com/bigcode-project/starcoder): Modelo entrenado con código fuente
- [Spacy-llm](https://github.com/explosion/spacy-llm): Vinculación de Spacy con Modelos LLMs
- [Web especializada en LLMs](https://llm-tracker.info/)
- [CodeLlama: Código de Inferencia para modelos Llama](https://ai.meta.com/blog/code-llama-large-language-model-coding/)
