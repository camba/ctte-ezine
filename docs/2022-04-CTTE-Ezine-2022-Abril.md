---
title: Abril 2022
---

# Cambá CTTE Ezine Abril 2022

![desovillar](@img/desovillar.png)

```
Soy un amante de la libertad
un anarquista sin ningún ideal
no sigo a políticos ni al papa ni a dios
a mi nadie me dice como tengo que actuar

no soy como esos que viven del estado
ser un empleado es de fracasado
con criptomonedas voy a generar valor
me salgo del sistema siendo emprendedor, por eso

soy un criptoplanero
con energía subsidiada
soy un criptoplanero
minando no me importa nada

tarjetié un par de placas de video
y me armé un rig en la pieza de atrás
el vecino me dio su clave de wifi
y mi vieja paga la electricidad

yo los considero mis inversores
aquí no hay riesgo, todos van a ganar
no me importa que me vengan a criticar
si tenés envidia ponete a minar

soy un criptoplanero
con energía subsidiada
soy un criptoplanero
minando no me importa nada

soy un criptoplanero
con energía subsidiada
soy un criptoplanero
minando no me importa nada

este es un país que tiene mucho para dar
vengan todos a Argentina a minar
aunque baje el precio siempre voy a holdear
no tiren mala onda, esto va a repuntar, porque

soy un criptoplanero
con energía subsidiada
soy un criptoplanero
minando no me importa nada

soy un criptoplanero
no te metas con mi plata
todos criptoplaneros
vení a invertir, no seas rata…
```

[Uctumi](https://www.uctumi.com/), músico chiptune de Argentina que comenzó a componer con trackers, nos trae el éxito del otoño 2022, [Cripto planero](https://soundcloud.com/uctumi/uctumi-criptoplanero) y [video letra para cantar](https://www.youtube.com/watch?v=fmJmC2trFos)


## Herramientas de software

- [Njsscan](https://github.com/ajinabraham/njsscan) - Encuentra partrones de código inseguro en aplicaciones Node.js
- [Vanta](https://github.com/tengbao/vanta) - Fondos 3D animados para webs
- [Webvm/CheerpX](https://webvm.io/) - Maquina Virtual x86 solo en Navegador [Más info](https://medium.com/leaningtech/webvm-client-side-x86-virtual-machines-in-the-browser-40a60170b361)
- [Tilt](https://tilt.dev/) - Herramientas para mejorar desarrollo con microservicios (local + kubernetes)

## Informes/Encuestas/Lanzamientos/Capacitaciones

- [Encuenta de Openqube de sueldos 2022.01 DICIEMBRE - FEBRERO](https://sueldos.openqube.io/encuesta-sueldos-2022.01/)
- Hackeo al senado:  [Vice Socierty- With love!](http://vsociethok6sbprvevl4dlwbqrzyhxcxaqpvcqt5belwvsuxaxsutyad.onion/) obtuvo [datos del Senado Argentino](http://ecdmr42a34qovoph557zotkfvth4fsz56twvwgiylstjup4r5bpc4oad.onion/JhykowedsgX/Bbzx8Jk3aQp63r/). [Nota en Infobae](https://www.infobae.com/tecno/2022/03/14/hackeo-al-senado-el-grupo-de-ciberdelincuentes-vice-society-filtro-30-mil-archivos/)
- [Tech Dominatrix - Dispositivo de control como fetiche](https://media.ccc.de/v/rc3-2021-cbase-209-tech-dominatrix-devic)
- [FOSSA: Estado de vulnerabilidades Software libre 2022](https://go1.fossa.com/rs/246-JVA-804/images/The%202022%20State%20of%20Open%20Source%20Vulnerabilities.pdf)

## Noticias, Enlaces y Textos

### Sociales/filosóficas

- [Cabildo del software, la cultura y el conocimiento libre](https://www.eradelainformacion.cl/) - Propuestas constitucionales para Chile en la era de la información
- [Piratería y desarrollo: discursos, historias y política de un amor negado](https://www.youtube.com/watch?v=zeYxVry6g4E)
- [Criptomonedas y Peronismo](https://www.youtube.com/watch?v=wW_B65EL8QI)
    - [Peronio](https://peronio.ar/)
- [Whitepapers sobre COPILOT](https://www.fsf.org/news/publication-of-the-fsf-funded-white-papers-on-questions-around-copilot)

### Tecnología

- [Vulnerabilidades en el codigo fuente que son invisibles](https://trojansource.codes/index/)
    - [Repo General](https://github.com/nickboucher/trojan-source/), [Ejemplo en Javascript](https://github.com/nickboucher/trojan-source/tree/main/JavaScript)
- [Introducción al Hardware Hacking](https://www.twitch.tv/videos/1292820677) - [Presentación](https://github.com/indetectables-net/embedded/tree/master/hardware-hacking-ekoparty-20201)
- [Bug en código de "Hola mundo"](https://blog.sunfishcode.online/bugs-in-hello-world/)
- [Wordle hecho con Vue, alias VVordle](https://github.com/yyx990803/vue-wordle)

### Otras

- [En la Show & Tell de Noviembre de 2021, Camplight habla sobre la "Comunidad Internacional de cooperativas de tecnología"](https://youtu.be/PpEsEPTomGw)
- [El Manifiesto Telecomunista](https://endefensadelsl.org/manifiesto_telecomunista.html) - Dmytri Kleiner


