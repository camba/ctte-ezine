# Entrevista de Fantino con Sztajnszrajber - 08/03/19

> [Video](https://www.youtube.com/watch?v=lwTgWgsj7Os)

## Nietzsche

- No hay hechos sólo interpretaciones
- No hay absolutos

## Socrátes

- Me di cuenta que la verdad no existe en este mundo
- Se pelea con todos los que se creen dueños de la verdad

## El Poder

- La gran aliada del poder es la verdad, el poder instala verdades
- Naturalizar un lugar de sujeción a partir de una idea de verdad (que es natural)

## Dualidad

- ¿Cual es la diferencia entre el bien y el mal? <br>
  Bien y el mal son categorias para simplificar, son consecuencias.
  El binarismo es farmacologico (identificación ferrea) y te ordena, te tranquiliza.

- La ética esta al servicio del poder, lo conveniente <br>
  Spinoza: No queremos las cosas porque son buenas, Son buenas porque las queres.

## Conflicto e Incertidumbre

- ¿Quien soy yo? Un campo de batalla <br>
  Somos muchos a la vez, Todo es conflicto.
  Las contradicciones nos forman y tenemos que reflexionar sobre nuestras contradicciones

- Vivimos en la Incertidumbre (nacimos para morir)
  La Cultura es para tapiar ese abismo.

## ¿Qué hacemos?

Antes de tu naciemiento y muerte, estuvo la eternidad
Todo es tan grande e infinito y somos tan pequeños.

Dos grandes opciones:

- Nada tiene sentido lo que haces
- Obsesionarte por lo que haces

Recomendación de lectura de [Emil Cioran](https://es.wikipedia.org/wiki/Emil_Cioran)
