---
title: Octubre 2019
---

# Cambá CTTE Ezine Octubre 2019

## Letra de canción

```
¿Qué es lo que esta mal en este lugar?,
acá yo no lo entiendo,
y quien más que vos sabe lo que vos.. sos.
A mi no me hagas preguntas, y si inmune estás a toda represión.. la de hoy, se que te perdimos.
Pero si puedes abrazar, gritar y sonreir, aún tu haces falta aquí,
tu haces falta acá, tu me haces falta aquí...

Y que tan rápido, insípido, fatal todos es,
podrás soportarlo!
y volves a tener temor a lo incierto, porque?
abraza su brisa,
si la curiosidad ha caducado en ti, yo lo sé, sé que te perdimos.
Pero si puede abrazar, gritar y sonreir,
aún tu me haces aca, tu me haces falta aquí!
```

Tema  [Lo único feo es no tener porque vivir](https://www.youtube.com/watch?v=SsMRgzzGy5M), de la banda [Boom Boom Kid](https://es.wikipedia.org/wiki/Boom_Boom_Kid).


## Aportes a proyectos de Software libre

- Lanzamiento de [Vue-admin v0.0.6](https://github.com/Cambalab/vue-admin/releases/tag/v0.0.6)

## Herramientas de software

- [Vue 3 alpha](https://github.com/vuejs/vue-next) - Donde se empieza a cocinar vue v3
- [is web vulnerable](https://github.com/lirantal/is-website-vulnerable) - Vulnerablidades en libs JS de webs online
- [Knexjs](http://knexjs.org/) - Generador de consultas SQL flexible, portable y divertido. (Recomendación @facundo)
- [OpenStudio Project](https://www.openstudioproject.com/) - Software para Planificación de horarios (pensado para talleres, salas, cursos, pagosonline)
- [Yoga Layout](https://yogalayout.com/) - Generación visual de layouts flexibles y optimizados (Implementa flexbox, y es de facebook)
- [Ionic React](https://ionicframework.com/blog/announcing-ionic-react/) - Nueva versión de Ionic con soporte para React

## Informes/Encuestas

-  [Balance general](./assets/CTTE-texto-balance-gral.pdf) - Capacitación que brindo @Jesica

## Noticias, Enlaces y Textos

### El deseo de cambiarlo todo

- [Devenir PLANTA - BRUJA - MÁQUINA](CTTE-texto-devenir-planta-bruja-maquina) - texto que transcribio @natalia para compartir con nosotres
- [¿Cómo llegar a Kathleen Hanna?](CTTE-texto-kathleen)
- [El aborto ilegal asesina a mi libertad](CTTE-texto-musica-feminista)
- [Economía feminista: explotación y extracción](CTTE-texto-veronica-gago) - extracto del capitulo 4 del libro "La potencia feminista" escrito por Verónica Gago.
- Escuchen la Entrevista de 2012 a [Ivana Micic](./assets/ivana_micic.ogg) charlando en Oveja Electrónica sobre su experiencia con software libre. [Audio](./assets/ivana_micic.ogg)
- Leonor Silvesti en Casaclan, Lanús, Sabádo 23/11, Presentación del libro **Primavera con Monique Wittig, El devenir lesbiano con el dildo en la mano de spinoza transfeminista** , [Más info](https://leonorsilvestri.wordpress.com/adonde-vamos/leonor-silvestri-en-lanus/)

### Más sociales/filosóficos

- [Nos están robando nuestros sentimientos](https://stealingurfeelin.gs/) - Documental interactivo sobre las grandes empresas tecnológicas, la inteligencia Artificial y vos (7 minutos)
- [Cooperativas de Plataforma / ¿Democracia Digital?](CTTE-texto-coop-plataformas)

### Más técnicos

- Considerar uso de **rate limits** para accesos, rest, websockets, vistas de apliaciones.
  - [node-rate-limiter-flexible](https://github.com/animir/node-rate-limiter-flexible)
  - [django-ratelimit](https://github.com/jsocol/django-ratelimit)
  - [Throttling django-rest-framework](https://www.django-rest-framework.org/api-guide/throttling/#throttling)
  - [Ejemplo con feathers](https://blog.feathersjs.com/feathersjs-in-production-password-policy-and-rate-limiting-32c9874dc563)
- Lindos [Atajos de Bash](https://opensource.com/article/19/10/bash-history-shortcuts)
- [Cheat Sheet vue 3 composition api](https://www.vuemastery.com/download-vue3)
- @neto nos Comparte [MS101, la maquinita de Bazán](./assets/MS101LaMaquinitaDeBazán.pdf) Una de las 1era Microcomputadoras hechas en Argentina.

### Seguridad

- Código fuente de **Nos están robando nuestros sentimientos**. [Github](https://github.com/noahlevenson/stealing-ur-feelings)
- Se empiezan a entregar los [primeros telefonos Librem 5](https://puri.sm/posts/first-librem-5-smartphones-are-shipping/) que hace [Purism](https://puri.sm/) - Pensados con software libre y con la máxima seguridad.
- [Hackeando vending machines modernas- Paso a paso como se hizo](https://hackernoon.com/how-i-hacked-modern-vending-machines-43f4ae8decec)


