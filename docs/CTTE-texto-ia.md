# Inteligencia Artificial: Respeto de derecho humanos y valores democráticos

> A partir de [noticia en blog de Vía Libre](https://www.vialibre.org.ar/2019/05/23/fundacion-via-libre-suscribe-a-las-directrices-universales-para-la-inteligencia-artificial/) traducimos 2 documentos que van en la línea del respesto de los derechos humanos y los valores democráticos en relación a la Inteligencia Artificial.

## Directrices Universales para la Inteligencia Artificial (UGAI)

> Texto a continuación extraído y traducido de documento - [Original](https://thepublicvoice.org/ai-universal-guidelines/)

Los nuevos desarrollos en Inteligencia Artificial están transformando el mundo, desde la ciencia a la industria, a la administración pública y las finanzas. El advenimiento de las decisiones basadas en IA también tienen implicancias sobre los derechos fundamentales de imparcialidad, responsabilidad y transparencia. El analisis de datos actualmente produce resultados que tiene consecuencias reales en la vida de las personas, a nivel laboral, de vivienda, crediticio, comercial y criminal. Mucha las técnicas utilizadas son totalmente opacas, dejando a los individuos sin saber si las decisiones fueron precisas, justas, o si fueron sobre ellos.

Nosotros proponemos estas directrices universales para informar e mejorar el diseñoa y uso de IA. Estás directrices intentan maximizar los beneficios de las IAs, minimizar el riesgo y asegurar la protección de los derechos humanos. Además deberían ser incorporadas en los estandares éticos, adoptada en las leyes nacionales y los acuerdos internacionales, y utilizadas en los diseños de sistemas. Nosotros claramente declaramos que la resonsabilidad fundamental para los sistemas de IA debe aplicarse a esas instituciones que invierten, desarrollan y despliegan esos sistemas.

- Derecho a la transparencia. Todos los individuos tienen el derecho de conocer el basamento de la decisión de IA que les involucra. Esto incluye acceso a los factores, la lógica y técnica que produce el resultado.

- Derecho a la determinación humana. Todos los individuos tienen el derecho a que la última palabra la tenga una personas.
- Obligatoriedad de identificación. La institución responsable del sistema de IA debe ser conocida publicamente.
- Obligatoriedad de justicia. Las Instituciones deben asegurar que el sistema de IA no es sesgada o toma decisiones discriminatoria inadmisibles.
- Obligatoriedad de evaluación y responsabilidad. Un sistema de IA debería ser desplegado solamente luego de una adecuada evaluación de su propósitos y objetivos, sus beneficios, y tambiénsus riesgos. Las Instituciones deben ser responsables de las decisiones hechas por el sistema de IA.
- Obligatoriedad de precisión, fiabilidad y válidez. Las instituciones deben asegurar la precisión, fiablidad y validez de las decisiones.
- Obligatoriedad sobre la Calidad de datos. Las instituciones deben establecer la precedencia de los datos y asegurar la calidad y relevancia de esos datos para su uso en algoritmos.
- Obligatoriedad sobre seguridad pública. Las instituciones deben evaluar los riesgos a la seguridad pública que surjan del despliegue de los sistemas de IA que dirijan o controlen dispositivos físicos, y implementen controles de seguridad.
- Obligatoriedad de ciber seguridad. Las instituciones deben proteger sus sistemas de IA contra las ciber amenazas.
- Prohibición sobre generación secreta de perfiles. Ninguna institución debe establecer o mantener sistema secretos de generación de perfiles
- Prohibición sobre Unitary Scoring. Ningun gobierno nacional debe establecer o mantener sistema de puntuación de propósito general sobre sus residentes o ciudadanos.
- Obligatoriedad de terminación. Cada institución que haya establecido un sistema de IA tiene la obligación objetiva de terminar de usar el sistema si el control humano del mismo ya no es posible.

## Principios de la OCDE sobre Inteligencia Artificial

> Texto a continuación extraído y traducido de documento - [Original](https://epic.org/algorithmic-transparency/OECD-AI-Principles-flyer.pdf)

La recomendación identifica cinco principio complementarios basados en valores para la administración responsable de la confianza en la IA:

- IA debe beneficiar a las personas y al planeta desarrollando un crecimiento inclusivo, sostenible y de binestar.
- Los sistemas de IA deben ser diseñados de forma tal que respete las leyes, los derechos humanos, los valores democráticos y la diversidad, y que implementen las salvaguardias adecuadas (por ejemplo habilitando la intervención humana de ser necesaria) para asegurar una sociedad justa.
- Debe haber transparencia y un divulgación responsable acerca de los sistemas IA para asegurar que las personas entiendan los resultados y puedan criticarlos.
- Los sistemas IA deben funcionar de manera robusta y segura a lo largo de su ciclo de vida y los potenciales riesgo deben ser evaluados y gestionados continuamente.
- Organizaciones e individuos que desarrollan, despliegan o operan sistemas de IA deben ser responsables en función de los principios anteriormente explicitados.

### ¿Qué pueden hacer los gobiernos?

Consistentemente con estos valores basados en principios, la OECD también da cinco recomendaciones los gobiernos:

- Facilitar la inversión pública y privada en investigación y desarrollo estimular la innovación en IA confiable.
- Fomentar ecosistemas de IA accesible con infraestructura, tecnología y mecanismos digitales para compartir datos y conocimiento.
- Asegurar ambientes politicos que se abrán camino para desplegar sistemas de IA confiables.
- Potenciar personas con habilidades para IA y ayudar a trabajadores para una transición justa.
- Cooperar entre fronteras y sectores para progresar en la administración responsable de la confianza de IA.
