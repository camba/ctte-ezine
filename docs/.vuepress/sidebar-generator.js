const path = require('path');
const fs = require('fs');

function sidebarGenerator (folderName = './') {
  const directoryPath = path.join(__dirname, '../', folderName);

  const addFolderName = entry => path.join(folderName, entry)

  const avoidNotEzines = (entry) => {
    const char = entry.charAt(0)
    return (char > '0' && char < '9')
  }

  const orderByDate = (e1, e2) => {
    const firstDate = e1.slice(0, 7);
    const secondDate = e2.slice(0, 7);
    return (firstDate > secondDate) ? -1 : 1
  }


  const entries = fs.readdirSync(directoryPath);
  return entries
    .filter(avoidNotEzines)
    .map(addFolderName)
    .sort(orderByDate);
}

module.exports = sidebarGenerator;
