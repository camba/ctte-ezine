# Cambá CTTE Ezine

> Edición mensual de revista electrónica de Cambá hecha por la [Coordinación técnica, tecnológica y educativa](Coordinacion-TTE) alias CTTE y colaboraciones de otres socies.
> Cada edición cuenta con las compilaciones hechas durante el último mes de trabajo en donde se encontrará enlaces, noticias y textos interesantes.
> Durante la lectura se encontraran con diversas opiniones, extracciones de datos, traducciones, descripciones, pensamientos hechos por nosotros pensando que nos aporta más y mejor conocimiento.


## Contribuciones

Apelamos a la buena voluntad e interes de les lectores a enviar colaboraciones específicas para las temáticas que nos gustan, motivan, interpelan y/o interesan. Eventulamente aplicaremos criterios curatoriales para organizar el material.


## Tecnología

- Si queres aportar algo también podes ver el proyecto en gitlab: [https://gitlab.com/camba/ctte-ezine](https://gitlab.com/camba/ctte-ezine)
- Usamos [VuePress](https://vuepress.vuejs.org/), un generador de sitios estáticos escrito en [Vue](https://vuejs.org/).
- Mediante gitlab-ci se hace un deploy automatizado en las gitlab pages.

## Otras

Cualquier duda, pregunta, discusión que podamos generar a partir de estos materiales es bienvenida.

Por ahora consultas, puteadas, contribuciones a [josx@camba.coop](mailto:josx@camba.coop)
