# Stare into the lights my pretties...

> Es un documental sobre la *cultura de la pantalla* y sus consecuencias. Minetras en mundo explota, ¿donde estamos?

## Algunas ideas centrales

- Nace en parte por la conjunción de los hippies y las fueras armadas, en San Francisco.
- La tecnología no es neutral, ni accidental.
- Una determinada Mentalidad ve posible y genera un tipo de tecnología, y esa tecnología brinda una percepción del mundo, esto se retroalimenta.
- El ABC del cambío tecnológico esta centrado en la Financión (fuerzas armadas, burocracia y corporaciones), es la expresión de las elites.
- Si la mayoría del tiempo te vinculas con un telefono, entonce todo es una app.
- Procesos mentales: Todo es literal y nada trae consecuencias
- La aceleración trae un costo implicito que la superficialidad en el vinculo.
- El multitasking es malo pare el cerebro
- IQ vs. Empatía, respuestas concretas vs. comprender
- Burbuja de filtros generá mayor polarización
- Más profundamente se trata de la soledad
