# ¿Qué es la economía popular? Experiencias, voces y debates
> https://editorialelcolectivo.com/producto/que-es-la-economia-popular/

## Extracto de "Introducción: Pensar la economía popular" Miguel Mazzeo y Fernando Stratta

La economía popular debe ser una economía de la abundancia de bienes necesarios ("Naturalmente necesarios") y básicos, de bienes socialmente utiles.

¿Hasta que punto las diversas inicitaivas de la economia popular no paortan a la realización directa o indirecta de la plusvalia a bajo costo y en favor de los grandes grupos economicos? Los ejemplos abundan. La cooperativa que le genera insumos baratos a las empresas más grandes, o la cooperativa que ledisminuye el costo de distribución, en fin, que le realiza al capital la plusvalia a bajo costo.

¿ Esto significa que las cooperativas no sirven?
Significa que debe asumir sus ventajas relativas como subestructura social democratica (liberadas de las formas despóticas ed dirección del capital) y como ámbito reproductivo de un colectivo determinado y , desde esa condición doble trinchera y vanguardia, encarar la modificación del conjunto de las interrelaciones productivas como única firna de syoerar sus desventajas.

Las posibilidades de desarrollo de una concienci anticapitalista. La voluntad de un colectivo unido por fuertes lazos de solidaridad (y/o de hermandad) puede inflingirle derrotas a las leyes de mercado.

La "eficacia" de una experiencia de la economía popular de bería medirse, principalmente, en términos de: 1) sostenibilidad social, 2) capacidad de garantizar la base material para subsistencia del colectivo más inmediato, 3) capacidad de generar subjetividades contra-hegemónicas, 4) solidez de su organización interna, 5) claridad ideológica, 6) confianza en el camino/horizonte asumido (certeza en elobjetivo estratégico), 7) formación de líderes y lideresas "de servicio", con iniciativa estrategica.
Ahora bien, una vez constatada esa eficacia, la misma vale solo como punto de partida para desarrollar su perfil productivo e incrementar su peso político.
