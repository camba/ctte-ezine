---
title: Agosto 2024
---

# Cambá CTTE Ezine Agosto 2024

<div style="text-align: center;">

![Mundo](@img/agosto2024.png)

</div>

```
Un hombre del pueblo de Neguá, en la costa de Colombia, pudo subir al alto
cielo. A la vuelta, contó. Dijo que había contemplado, desde allá arriba,
la vida humana. Y dijo que somos un mar de fueguitos.

 -El mundo es eso – reveló-. Un montón de gente, un mar de fueguitos.

Cada persona brilla con luz propia entre todas las demás. No hay dos fuegos
iguales. Hay fuegos grandes y fuegos chicos y fuegos de todos los colores.
Hay gente de fuego sereno, que ni se entera del viento, y gente de fuego loco,
que llena el aire de chispas. Algunos fuegos, fuegos bobos, no alumbran ni
queman; pero otros arden la vida con tantas ganas que no se puede mirarlos
sin parpadear, y quien se acerca, se enciende.
```

Micro relato [El Mundo](https://www.youtube.com/watch?v=9V922yOgsXc) de [Eduardo Galeano](https://es.wikipedia.org/wiki/Eduardo_Galeano) del libro "El libro de los Abrazos".

## Contribuciones Software libre de Cambá
- [Nuevo Proyecto **Ex Finance**](https://github.com/sgobotta/ex_finance) - [Versión Online](https://finance.liveapps.com.ar/)
- [Traducciones faltantes de **Page-Assist**](https://github.com/n4ze3m/page-assist/pull/134)
- [Más extensiones de lenguajes de programación en **Open-Webui**](https://github.com/open-webui/open-webui/pull/3990)
- [Más extensiones de lenguajes de programación en **Rag-api**](https://github.com/danny-avila/rag_api/pull/59)


## Herramientas de software
- [Audapolis](https://github.com/bugbakery/audapolis): Editor de audio hablado con transcripción automática
- [Bottles](https://usebottles.com/) - Facilmente corre aplicaciones windows en Linux
- [Open-and-Shut](https://github.com/veggiedefender/open-and-shut) - Morse vía la tapa de las notebooks
- [Workrave](https://workrave.org/) - Tomate un rato y relaja (Recuperate y prevenite de las lesiones por esfuerzo repetitivo)
- [Farm](https://github.com/farm-fe/farm): Herramientas para hacer build Vite-compatible escrita en Rust (más rápida que vite)


## Informes/Encuestas/Lanzamientos/Capacitaciones

- [Resultados de la Encuesta 2024 de StackOverflow](https://survey.stackoverflow.co/2024/)
- [Resultados de la Encuesta "State of React"](https://2023.stateofreact.com/en-US)
- [Charla en la Pycon de Simon Willison sobre IA](https://simonwillison.net/2024/Jul/14/pycon/)
- [Boletín de la Free Software Foundation - Edición Primavera](https://magazine.fsf.org/2024-spring/)
- [DebConf24 del 28 de Julio al 4 de Agosto en Busan, Corea del Sur](https://debconf24.debconf.org/)
- [Suiza requiere que todo el software del gobierno sea "open source"](https://www.zdnet.com/article/switzerland-now-requires-all-government-software-to-be-open-source/)

## Noticias, Enlaces y Textos

### Sociales/filosóficas

- [Todo es plata y ansiedad - Mark Fisher](https://www.revistaanfibia.com/mark-fisher-todo-es-plata-y-ansiedad/)
- [Miguel Benasayag: Crítica a la IA y la ultraderecha | El impacto económico y social](https://www.youtube.com/watch?v=iFK9DzEM-3c)
- [El problema de la escala en el anarquismo y el caso del Comunismo cibernético](https://c4ss.org/content/52970)
- [Contra la POLÍTICA y la DERROTA | La política de la DESESPERANZA](https://www.youtube.com/watch?v=8H3EUY0ngb8)

### Tecnología
- [Conversación actual de Dirk Hohndel con Linus Torvalds](https://www.youtube.com/watch?v=cPvRIWXNgaM)
- [PySkyWiFi: Como obtener internet vía wi-fi en vuelos](https://robertheaton.com/pyskywifi/)
- [Booteando linux desde google drive](https://ersei.net/en/blog/fuse-root)
- [Openworm projecto dedicado a crear el primer organismo virtual en una computadora](https://openworm.org/)
- [Elixir: El documental](https://www.youtube.com/watch?v=lxYFOM3UJzo)

### LLMs
- [Llama 3.1 405B - Primer modelo "Open source" que llega al estado del arte de LLMs](https://www.youtube.com/watch?v=JLEDwO7JEK4)
- ¿Cómo hacer fine tuning de modelos?
    - [Ejemplo tuneando llama 2](https://www.datacamp.com/tutorial/fine-tuning-llama-2)
    - [Guía general](https://llama.meta.com/docs/how-to-guides/fine-tuning/)
    - [Ejemplo tunenado llama 3](https://pytorch.org/torchtune/stable/tutorials/llama3.html)
    - [Unsloth](https://github.com/unslothai/unsloth)
- [NEW Universal AI Jailbreak SMASHES GPT4, Claude, Gemini, LLaMA](https://www.youtube.com/watch?v=9IM5d-egZ7M)
