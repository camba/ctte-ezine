# Cooperativas de Plataforma / ¿Democracia Digital?

Resumen del Documento de Discusión solicitado por la ACI, y redactado por Ed Mayo (Coops UK y Coops Europa) en colaboración de otros autores, entre ellos Trebor Scholz.

[Link a Documento de Discusión](https://www.uk.coop/resources/digital-democracy-options-international-cooperative-alliance-advance-platform-co-ops)

### Surgimiento

El concepto surge como resultado de investigaciones sobre **trabajo e infraestructura digital** que se dieron desde el 2009 en la [New School](https://www.newschool.edu/) de Nueva York, una *universidad super moderna, liberal e innovadora*.

Trebor Sholz le pone nombre cuando publica en 2014 ["Platform Cooperativsm"](http://www.rosalux-nyc.org/wp-content/files_mf/scholz_platformcoop_5.9.2016.pdf), y en el 2015 se hace un evento en la New School donde "lanzan" la idea.

Algunos lo ven como un movimiento amplio que viene a desafiar el actual paradigma socio-técnico, done unos pocos obtienen beneficios de la cultura de la participación online.

### Proyección

Por un lado, es un modelo que podría potenciar la participación del cooperativismo en la economía digital, manteniendo un modelo equitativo, participativo y de propiedad colectiva.

Sin embargo, advierten que las experiencias prácticas que existen están limitadas en escala, y no hay muchos estudios cuantitativos de sus actividades. Además, ningún caso sirve de modelo para otros, debido a sus características únicas.

La ACI resuelve promover la *exploración* de este tema en 2017.

En 2018 hace un "útil aporte" (¿significará que puso plata?) a un programa liderado por Scholz y la New School, y financiado inicialmente por google.org, que entre otras cosas brinda apoyo para la [creación de cooperativas](https://platform.coop/blog/the-platform-co-op-development-kit/), patrocinado por [Platform Cooperativism Consortium](https://platform.coop/).

#### Aceleradoras: 

- USA: [start.coop](https://start.coop), tiene la característica que las cooperativas incubadas devuelven ingresos durante 10 años y se usan para que el programa siga funcionando.
- UK: [Unfound](http://unfound.coop/), desarrollado por [CoopsUK](http://uk.coop/) y la revista [Stir to Action](https://www.stirtoaction.com/) y financiado por [The Hive](https://www.uk.coop/the-hive) a través de [The Cooperative Bank](https://www.co-operativebank.co.uk/).

#### Ventajas Competitivas según CoopsUK

- Al poner a lxs creadorxs en control sobre como se generan y distribuyen los ingresos podría proporcionar un modelo empresarial más sostenible donde el contenido se monetiza sin necesidad de *terceros extractivos*
- Las platformas en línea favorecen el aumento de la actividad económica y social al permitir intercambio de valor creado en entornos formales e informales.

#### Impulsores y servicios

- [Cooperatives Europe](https://coopseurope.coop/), que señala que la economía colaborativa propicia la explotación.
- [Cecop](http://www.cecop.coop/), la red de cooperativas de trabajo.
- [Sharetribe](https://www.sharetribe.com/): te brinda la infraestructura para que te armes un marketplace.

### Definición

Hacen un ejercicio interesante, analizando esta definición *a priori* acertada:

> “Una cooperativa de plataforma es una empresa de propiedad compartida, gestionada democráticamente, que  utiliza una plataforma digital que permite a las personas satisfacer sus necesidades o resolver un problema por medio de personas y activos de red” 

Según esto, cualquier empresa que cotiza en bolsa puede decir que es de propiedad compartida entre los accionistas y democrática ya que si sos accionista podés votar... claro que se tiene en cuenta *cuántas* acciones tenés, donde entra en juego el *poder financiero*

Por eso, en el texto hace un *paseo* por los valores cooperativos y analizan las características (muy similares) de las cooeperativas:

- Son participativas
- Están conectadas en red
- Tienen un uso final claro
- Proporcionan beneficios tangibles para los miembros
- Se controlan democráticamente
- Crean o cambian valor

Entonces, definen a las cooeperativas de plataforma como: 

> una empresa que opera principalmente a través de plataformas digitales para la interacción o el intercambio de bienes o servicios y está estructurada de acuerdo con la Declaración de la Alianza Cooperativa Internacional sobre la Identidad Cooperativa 

### Recomendaciones para la ACI

Identifican 4 *retos* para potenciar a las cooperativas de plataforma:

- Gobernanza
- Tecnología
- Crecimiento
- Capital

En tecnología, proponen que la ACI tome estas acciones:

- Programas de innovación (o actualizacion) tecnológica en cooperativas establecidas
- Promoción del modelo cooperativo en redes tecnologicas
- "Investigar" sobre el uso de software libre y de código abierto.
- Desarrollar un *ecosistema* digital que apoye a las coops.
- Crear un informe anual sobre cooperación y tecnología.


