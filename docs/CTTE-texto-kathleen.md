# ¿Cómo llegar a [Kathleen Hanna](https://es.wikipedia.org/wiki/Kathleen_Hanna)?

Aproximadamente en el verano argentino del año 1998, pero en el barrio de
south central en la ciudad de los Angeles en una casa llamada "La Playa",
una ecuatoriana con nacionalidad estadounidense me regalo un cassette con
música que le gustaba, ahí como se ve en las inscripciones contenia canciones
de varias bandas:
- Lado A: Camisa de fuerza, Face to Face, MXPX
- Lado B: Social Distortion, Neds Atomic Dustbin, y otros

![Lado A](@img/k7-lado-a.jpg)
![Lado B](@img/k7-lado-b.jpg)

Ahí en ese **otros**, estaban dos bandas de mujeres [L7](https://es.wikipedia.org/wiki/L7) y [Bikini Kill](https://es.wikipedia.org/wiki/Bikini_Kill).

Luego, La semana anterior a la mitad de octubre de 2019 pero 21 años despues leyendo
un libro comprado de manera colaborativa llamado ["No te debemos nada"](https://indiehoy.com/libros/no-te-debemos-nada-el-libro-de-walden-editora-que-reune-entrevistas-de-punk-planet-magazine/) leo una entrevista a [Kathleen Hanna](https://es.wikipedia.org/wiki/Kathleen_Hanna) cantante de [Bikini Kill](https://es.wikipedia.org/wiki/Bikini_Kill).

Transcribo una pregunta y respuesta que brilla sobre otras:

**Si el éxito significa ser "masculina" o "capitalista", ¿Qué nos queda a
nosotros? Sin sueños y sin nada por lo que esforzarnos.**

Es frustrante, porque digo estas cosas y, sin embargo, conocí feministas
cuyo único objetivo es avanzar dentro del sistema tal como es. Siguen
defendiendo el éxito igual que siempre, es decir, en función del dinero y de
cuánto control pueden tener sobre su entorno. Me frustra el feminismo
que no hace un análisis del capitalismo, y lo mismo el anticapitalismo que
no hace un análisis racial, feminista o de clase.
En Yugoslavia, los obreros eran dueños de todos los medios de producción,
pero seguían teniendo que competir entre ellos porque la gente que compraba
los productos seguía queriendo comprar los más baratos. No hubo una
reconsideración de los valores, simplemente se pasó de la propiedad del
sector privado a la propiedad de los trabajadores. No hubo un gran
cuestionamiento al sistema de valores, las fábricas terminaron compitiendo
entre ellas a un nivel ridículo. Veo algo similar en el feminismo.
Si no combatimos las formas nocivas de competitividad que el capitalismo
engendra, o la manera en que nos enseña a vernos a nosotros mismos y los
demás como objetos, solo nos estamos vendiendo. Nada va a cambiar.
En Yugoslavia, solo cambiaron quién es dueño de los medios de producción,
pero no modificaron lo que se producía o cómo se producía o el sistema
de valores en todos lados y, en cambio, la cuestión se reduce a mujeres
blancas de clase de media que quieren una porción equitativa de la torta,
es aburrido.
Eso hay que tomar con pinzas en mi otro argumentos, que es importante que
las mujeres tengan sus negocios. Al menos tenemos que tratar de crear
nuevas estructuras y nuevo modos de lidiar con las cosas, pero no tiene que
ser por oposición. No debería ser cuestión de elegir entre manejar un
negocio corporativo de mierda que no piensa acerca de qué hacen sus productos
en el mundo, cómo se trata a sus trabajadores y por qué todos los altos
ejecutivos pertenecen a un grupo determinado, o maneja un negocio punk,
regalar los productos y no tener estructura. Eso solamente crea una nueva
idea falsa en la que seguimos definiéndonos de acuerdo con el orden actual,
porque nos definimos por oposición a eso. Estamos supeditados a que sigan
siendo una mierda.

Luego de leer la entrevista completa, fui rapidamente a buscar su documental.
Y dentro de [Peliculas feministas](https://www.peliculasfeministas.com/) un compendío de peliculas feministas en el uno puede pasar horas y horas buscando que mirar encontre [**The Punk Singer**](https://en.wikipedia.org/wiki/The_Punk_Singer).
