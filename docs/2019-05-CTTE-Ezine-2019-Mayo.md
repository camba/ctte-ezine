---
title: Mayo 2019
---

# Cambá CTTE Ezine Mayo 2019

## NEC SPE, NEC METU

```
Experimenten, pero no dejen de tener encuenta que para experimentar hace falta mucha prudencia.
Vivimos en un mundo más bien desagradable, en el que no sólo las personas, sino también los
poderes establecidos, tienen interés en comunicarnos afectos tristes.
La tristeza, los afectos tristes son todos aquellos que disminuyen nuestra potencia de obrar
y los poderes establecidos necesitan de ellos para convertirnos en esclavos...
No es fácil ser libre: huir de la peste, organizar encuentros, aumentar la capacidad de
actuación, afectarse de alegría, multiplicar los afectos que expresan o desarrollan un máximo
de afirmación.
```

> Escrito por [Gilles Deleuze](https://es.wikipedia.org/wiki/Gilles_Deleuze), extraído del libro [Foucault para encapuchadas](https://distribuidorapeligrosidadsocial.files.wordpress.com/2011/11/foucault-para-encapuchadas.pdf)

## Aportes a proyectos de Software libre

- Se mergea finalmente contribución a [Aragon-Apps](https://github.com/aragon/aragon-apps/) de la aplicación Payroll en la que participaron Santiago Botta y José luis Di Biase [PR](https://github.com/aragon/aragon-apps/pull/613)

## Herramientas de software

- [Saber](https://github.com/egoist/saber) - Generador de sitios estáticos super rápidas con Vue.js
- [Svgbob](https://github.com/ivanceras/svgbob) - Convertir diagramas en texto a svg [probarlo online](https://ivanceras.github.io/svgbob-editor/)
- [FairBnB](https://fairbnb.coop/) - Servicio cooperativo y con valores similar a AirBNB
- [CSSFX](https://cssfx.dev/) Web que en la última semana gano más estrellas en github +1.2k
- [Fusuma](https://github.com/hiroppy/fusuma) - Presentaciones con Markdown <br>
  Alternativa a lo que uso que durante las últimas semana vimos en las presentaciones de @neto hechas, con un viejo conocido, [Orgmode](https://orgmode.org/) + [org-reveal](https://github.com/yjwen/org-reveal/) + [revealjs](https://revealjs.com/)
- [Vue-qr](https://github.com/Binaryify/vue-qr) - Componente para usar en Vue, [AwesomeQR](https://github.com/SumiMakito/Awesome-qr.js)

## Informes/Encuestas

- [Está complicado: Reporte de Mozilla sobre la salud de internet](https://blog.mozilla.org/blog/2019/04/23/its-complicated-mozillas-2019-internet-health-report/) <br>
  Extenso reporte sobre como es la intersección entre humanidad e internet, su impacto sobre la sociedad y como influencia todos dias de nuestra vida.
- [Machete Vue](https://www.vuemastery.com/pdf/Vue-Essentials-Cheat-Sheet.pdf)
- [Salio beta](https://blog.ionicframework.com/announcing-stencil-one-beta/) de [Stencil](https://next.stenciljs.com/)

### Enterprise Javascript in 2019, NPM inc

> [La encuesta](https://javascriptsurvey.com/) la hace NPM, su sentido es aprender sobre las necesidades y comportamientos de la comunidad javascript para ayudar a NPM y a otros en la comunidad de NPM a hacer mejores elecciones técnicas y mejorar el servicio al usuario. Participaron más de 33k personas (usuaries de NPM). [Bajar](https://cdn2.hubspot.net/hubfs/5326678/Resources/JavaScript%20Surveys/2019_npm_survey_FINAL.pdf)

Algunos resultados:

- En promedio hay más gente experimentada usando Javascript
- A fin de 2019 el 99% de les desarrolladores de Javascript usuarán NPM
- 83% esta preocupado por la seguridad del código libre que usa
- 46% usa herramientas para scanear el código como `npm audit`
- 76% usa la Revisión de código como método para asegurar la seguridad
- 58% toma como importante a la hora de usar un código la licencia que tiene
- 63% dice que la empresa donde trabaja le prohibe usar software que no tenga licencia o una licencia no reconocida
- 46% usa javascript para el desarrollo de aplicaciones nativas (mobile y desktop), 12% dispositivos embebidos
- 63% usa React (el framework más popular)
- 62% usa typescript (36% lo hacen habitualmente o en todos los proyectos)
- ¿Como haces el deploy de tu aplicación javascript? 56% containers, 38% Paas, 35% VMs directamente, 33% serverless
- 72% usan o estan considerando usar GraphQL
- 54% estan considerando usar WebAssembly

### Especial I - Package Registries

Muchas veces publicamos y consumimos bibliotecas/librerias y programas de múltiples lenaguajes en nuestro día a día.
Un ejemplo común que usamos día a día es **NPM** *(Node package manager)* que nos brinda un cliente y una base de datos de paquetes javascript.
Otro ejemplo claro en la misma línea, solo para nombrar uno, es **APT** *(Advanced package tool)* que usamos en distros gnu/linux como debian, ubuntu, mint, etc para instalar programas en nuestras computadoras.

Obviamente siempre hay [alternativas](https://en.wikipedia.org/wiki/List_of_software_package_management_systems) para usar otras herramientas cliente y también como base de datos de donde ir a buscar esos paquetes.

Algunos ejemplos cercanos para ilustrar en javascript:

- Npm ([cliente](https://docs.npmjs.com/cli/npm) y [base de datos](https://www.npmjs.com/) gestionado por empresa npm inc.)
- [Yarn](https://yarnpkg.com/) (cliente)
- [Open registry](https://open-registry.dev/) (base de datos gestionada por la comunidad)
- [Verdaccio](https://github.com/verdaccio/verdaccio) (base de datos gestionada privadamente por vos)
- [Github package registry](https://github.com/features/package-registry) (base de datos gestionada por github, multilenguaje)


### Especial II - Pasos para probar distros de linux sin instalarlas

- Instala un [cliente VNC](https://help.ubuntu.com/community/VNC/Clients)
- Entra a [DistroTest](https://distrotest.net/)
- Selecciona Distro a probar, toca sobre START
- Copia los datos para conectarte, IP y PUERTO
- Abri cliente VNC y pone en dirección IP:PUERTO
- Usa la distro

## Noticias, Enlaces y Textos

### Eventos

- *"MUJER ARTISTA/MUJER TRABAJADORA"* <br>
  Belén nos invita vivenciar la muestra colectiva, con más de 25 artistas en la Biblioteca Pública y Complejo Mariano Moreno en Av Belgrano 450, Bernal.

### Más sociales

- [Agora 2.0 - Los límites de la conciencia](https://vimeo.com/66273646) <br>
  Natalia Zuazo entrevista a Mariano Sigman
- [Slavoj Zizek - Sobre el consumismo](https://www.youtube.com/watch?v=TawLAkoIF7Q)
- [Preguntas sobre la tecnología](CTTE-texto-preguntas-ellul) <br>
  ¿Podriamos utilzarlas con puntaje para evaluar y reflexionar sobre los proyectos que potencialmente podriamos hacer?
- [Las cooperativas también necesitan lideres](CTTE-texto-cooperativas-lideres)
- [Evaluación de pares en Cooperativas de trabajo](CTTE-texto-cooperativas-evaluacion-pares)
- Ejemplo de que pasa en escuelas de USA, [Kidoyo](https://kidoyo.com/) en una esucela pública de Mineola ( [Texto](https://www.linuxjournal.com/content/kids-take-over-0), [Entrevista en Audio](https://www.linuxjournal.com/podcast/episode-18-kidoyo) )
- [Hackeo con cambio de proveedor de SIM](https://medium.com/coinmonks/the-most-expensive-lesson-of-my-life-details-of-sim-port-hack-35de11517124)

### Más técnicos

- [Contruyendo aplicaciones de escritorio con Vue](https://www.vuemastery.com/conferences/vueconf-us-2019/building-desktop-applications-with-vue/) - Natalia Tepluhina 19'
- [Debugging Templates en Vue](https://vuedose.tips/tips/debugging-templates-in-vue-js/)
- [Compartiendo Componentes reusables en Vue.js con Lerna, Storybook, y npm](https://medium.com/js-dojo/sharing-reusable-vue-js-components-with-lerna-storybook-and-npm-7dc33b38b011)

## Otras

- [Nota de Betterz sobre Cambá/Fiqus](https://blog.betterez.com/blog/innovation-through-education-upskilling-our-developers)
- [El bandido de blockchain](https://www.securityevaluators.com/casestudies/ethercombing/)
- [Video Extracto de Asamblea Ordinaria de UST](https://www.facebook.com/CoopUST/videos/327432811271881/)

## Propuestas de contribuciones

- [Purple Mattermost](https://github.com/EionRobb/purple-mattermost/) necesita ayuda para probar y terminar el soporte para Mattermost api v4. [Ver discusión](https://github.com/EionRobb/purple-mattermost/issues/73) <br>
  Este plugin es necesario para poder utilizar desde clientes purple, Mattermost (ej desde [pidgin](https://pidgin.im/))
- Hacer plugin para [vuex-orm](https://github.com/vuex-orm/vuex-orm) del [cliente feathers](https://feathersjs.com/), para tomar en cuenta ej: [plugin-axios](https://github.com/vuex-orm/plugin-axios)
- [Vue-element-admin](https://github.com/PanJiaChen/vue-element-admin) es la administración en Vue más usada, sería muy bueno para nuestro proyecto [vue-admin](https://github.com/Cambalab/vue-admin) integrarlo con este proyecto.
- Contribuir la traducción de las [Hacker laws](https://github.com/dwmkerr/hacker-laws)
- [Local Light Field Fusion](https://github.com/Fyusion/LLFF) sería interesante, si alguien lo prueba y hace una devolución para publicar en el ezine próximo :)
