# Christophe Dejours

> Resumén de entrevista recomendada por @charlie [Sin posibilidades de sublimar a través del trabajo, es muy difícil conservar la salud mental](https://www.pagina12.com.ar/197853-sin-posibilidades-de-sublimar-a-traves-del-trabajo-es-muy-di)

Para ampliar un poco la perspectiva con el un casos especificos se puede leer este [articulo](https://www.pagina12.com.ar/diario/psicologia/9-219111-2013-05-02.html)

## Resumen

> ¿De qué manera el trabajo deja de tener un lugar marginal en la constitución de la subjetividad?

Hay *dos ejes* en la realización de uno mismo, en el *campo erótico* pasa por el amor y en el *campo social*, eso pasa por el trabajo.
Destindo para pulsión: Uno sexual y otro Sublimatoria.

Cuando uno está forzado a trabajar mal, porque las condiciones son malas, porque está la presión de la productividad cuantitativa contra la cualitativa, las condiciones de sublimación se rompen, y mucha gente se enferma. Donde no hay posibilidades de sublimar a través del trabajo, se torna muy difícil conservar la salud mental, y muchas veces hay que desarrollar estrategias muy complicadas para protegerse contra los ambientes deletéreos en el trabajo.

> ¿Qué sucede cuando no se tiene la posibilidad de trabajar, cuando se está desempleado/a?

Cuando uno no puede aportar una contribución a través del trabajo, se pierde el beneficio posible de esa retribución simbólica a través del reconocimiento, y en esta cuestión el porvenir desde el punto de vista de la salud mental se vuelve mucho más precario.

> ¿Qué patologías y qué posibilidades de encontrar placer en el trabajo encuentra en las condiciones actuales del neoliberalismo?

Pequeño problema (se ríe)... Consiste en comprender por qué, por un lado, pasamos a la desolación, la desesperanza, y por qué en algunos casos se vuelve felicidad. Y hay una razón que es muy precisa, que es el *rol decisivo en la organización del trabajo*.

Se introducen nuevos métodos, nuevos dispositivos, que cambian completamente la organización del trabajo: la evaluación personal de los desempeños; la noción de calidad total; la normalización o estandarización del trabajo, el tema de las normas como las ISO; la precarización; y también la manipulación comunicativa producida por las mismas empresas.

Esta manipulación es muy importante, no solamente respecto de lo externo, de la empresa para afuera, haciendo publicidad, por ejemplo mostrando los resultados de la empresa en la bolsa; sino que también es una comunicación que está destinada al interior, porque se vuelve un sistema de prescripciones, al cual los mismos trabajadores asalariados deben estar muy atentos, para poder utilizar las buenas formas del lenguaje, las maneras en las que hay que implicarse en las relaciones jerárquicas, lo que uno puede o no decir, todo eso está dictado por la comunicación interna.

> Este sistema de presión y control ideológico está vigente tanto en las empresas privadas como en organismos públicos, ¿verdad?

Ese giro de la gestión, de los números, se traduce por una voluntad de romper todo lo que sea colectivo, y romper las cooperaciones, para poder tener únicamente individuos que en la jerga de la gestión llaman “los individuos responsables”. Y los métodos en cuestión son muy fuertes, muy poderosos, y han logrado desestructurar esas cooperaciones.

Al hacer eso, al desestructurar esa cooperación, se destruyen cierto tipo de vinculaciones entre las personas, sobre todo las relaciones de convivencia, que tienen que ver con estar atento a lo que necesita el otro, la ayuda, el saber vivir juntos y la solidaridad. Todo está destruido por estos nuevos dispositivos.

Esta cuestión de lo colectivo y la solidaridad, esta cuestión de vivir juntos, es una cuestión social y política por supuesto, pero es también una cuestión que tiene que ver con la salud.

> ¿Por qué?

Porque la mejor manera de prevenir contra riesgos psicosociales, contra las patologías mentales del trabajo, es justamente esta convivencia, el vivir juntos, la solidaridad. Hay dos grandes fuentes en la salud en el trabajo, la primera es el vínculo individual con la tarea, que está relacionado con la sublimación, pero también está el hecho de poder entrar en un vínculo, en una relación de pertenencia en un equipo, pertenencia a un oficio, una profesión, porque todas estas pertenencias nos remiten siempre a sistemas de valores.

Cada profesión está estructurada por ciertas reglas, esas reglas de trabajo no son nunca únicamente normas para tratar la cuestión de la eficacia; esas normas de trabajo organizan también los vínculos y los lazos entre los miembros de un equipo. Al desestructurar esos colectivos, se les hace perder a los trabajadores todo el beneficio de la ayuda mutua, que no es solamente en favor de la eficacia, sino que también es una ayuda mutua respecto del sufrimiento.
