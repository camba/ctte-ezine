# 10 pines - Liderando una empresa sin jefes

- 70 personas
- Rotacion **5%** no **25%** como en el mercado,**3M** dolares facturación anual
- votacion, consenso, consentimiento (alguien demasiado afectado por una decisión)
- Advice process (expertos + afectados, cuando una decision tarda mucho)
- Open space para propuestas
- Información abierta
- Salario abierto, 4x dispersión de sueldos
- Repartición de ganancia (algoritmo: señority, horas, etc)
- Selección de personas (3 entrevistas: entrevista + ejercicio técnico, critica y evaluación , con todos los de la empresa)
- Áreas de soporte (diferentes roles): No tienen (cada uno se involucra)
- ¿Compromiso de los Millenials?
- Todos los días que quieran Home office
- Viernes de no cliente
- 1 reunión estrategica anual (2 días de retiro espiritual en casa del bosque)
- StandUp meeting una vez por semana de grupo de trabajo
- Correlación entre salud mental/organizacional y performance (diferentes métricas)
- Concepto de Seguridad psicológica
- *No solo altruista, mejores resultados*
- Regala una Ficha para cambiar el mundo (reacción en cadena)

