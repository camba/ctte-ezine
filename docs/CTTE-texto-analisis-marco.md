# Analisis del Marco Legal Cooperativo Argentino

[Este informe](./assets/informe_normativa_argentina.pdf) fue escrito por [Dante Cracogna](http://www.unlpam.edu.ar/articulos-principales/dr-dante-cracogna-profesor-honorario-de-la-unlpam), Profesor de Derecho Comercial en la Universidad de Buenos Aires; de Derecho Empresario en el Doctorado de la Universidad de Ciencias Empresariales y Sociales y en la Especialización en Negocios Societarios de la Universidad Notarial Argentina.

Forma parte del Grupo Asesor Jurídico de la Alianza Cooperativa Internacional.

## Contexto General

Nuestra legislación está contenida en la [ley Nacional n° 20.337](http://servicios.infoleg.gob.ar/infolegInternet/anexos/15000-19999/18462/texact.htm), sancionada en 1973. Es una ley Nacional, por lo que las provincias no pueden legislar esta materia.

Esta Ley rige para todas las coopes, no importa su tipo. En el caso de las cooperativas financieras y bancos cooperativos, también las regula la Ley de Entidades Financieras, lo mismo para las de servicios (electricidad, gas, telefonía, etc), les cabe además la legislación del rubro.

La Ley de Cooperativas no forma parte del Código Civil y Comercial, pero éste reconoce a las cooperativas como personas jurídicas. Adicionalmente, las coopes se rigen por la Ley General de Sociedades (n°19.550).

Si bien los principios cooperativos tal como los formula la ACI en la Declaración sobre la Identidad Cooperativa (1955) no están explícitos en la ley, están contenidos en sus artículos.

Casi todas las constituciones provinciales tienen un artículo o norma referida al reconocimiento o promoción de las cooperativas, no así la Constitución Nacional.



## Elementos específicos

- **Control Democrático:** La Ley Cooperativa (LC) determina que un asociado = un voto, cualquiera sea el número de sus cuotas sociales. Sin embargo en *organizaciones de grado superior el voto puede ser proporcional al número de asociados, al volumen de operaciones o ambos*.
- **Participación económica de lxs asociadxs:** Los excedentes se distribuyen en proporción a las operaciones realizadas por lxs asociadxs. Las reservas son irrepartibles, aún en caso de disolución.
- **Educación, capacitación e información:** Contempla la promoción de estas áreas al exigir destinar el 5% del excedente anual.
- **Integración Cooperativa:** La LC contempla distintas formas de integración: fusión, incorporación, formación de entidades de grado superior. También pueden realizar *"operaciones en común, determinando cuál de ellas será la representante de la gestión y asumirá la responsabilidad frente a terceros."*
- **La responsabilidad de lxs socixs es limitada**

> La LC diferencia claramente a las cooperativas de  las  sociedades  de  capital  por  cuanto  en éstas  el  voto  es  proporcional  al  capital  aportado  y  de  igual  manera  se  distribuyen  las utilidades;  las  reservas  son  repartibles  y  el capital sólo aumenta  mediante  decisión de  los socios. Estas sociedades cuentan con su régimen legal y registro propios

## Establecimiento, membresía y gobierno

Se registran en el [INAES](https://www.argentina.gob.ar/inaes) (Instituto Nacional de Asociativismo y Economía Social).

El principio de "puertas abiertas" establece pueden ser asociadxs todas las personas físicas y jurídicas que reúnan los requisitos establecidos por el estatuto. En general, en el estatuto establece que el ingreso de nuevxs miembrxs debe ser resuelto por el Consejo de Administración. El estado nacional, las provincias y municipios pueden utilizar los servicios de las coops sin asociarse, ser asociados o establecer que grado de participación tienen en conjunto con la coope.

### Estructura de gobierno

Consta obligatoriamente de trés órganos:

- **Asamblea:** Participan todxs lxs asociadxs con un voto. Decide los asuntos de mayor trascendencia. Se reune ordinariamente una vez al año, para evaluar memori y balance. Puede reunirse de manera extraordinaria por iniciativa de los órganos de administración, fiscalización o por un grupo de asociadxs.
- **Consejo:** Tiene a su cargo la administración y gestión de la coope. Es elegido por la asamblea. La duración en el cargo no puede superar los tres ejercicios. Debe reunirse por lo menos una vez al mes. Le Preseidente es le representante legal de la cooperativa.
- **Fiscalización:** Está a cargo de unx o más asociadxs, llamadxs *síndicos*. Pueden asisitir con *voz* a las reuniones de consejo. Deben validar la memoria y balance.

### Estructura financiera

El capital se divide en cuotas sociales y el estatuto determina la cantidad mínima que debe suscribir cada asociadx. Lxs asociadxs pueden voluntariamente aportar distinta cantidad de capital, sin límite individual.

En  caso  de  egreso  o  de  disolución  de  la  cooperativa  los  asociados  tienen derecho  a  la devolución  del  valor  nominal  de  sus  cuotas  sociales pagadas,  deducidas  las  pérdidas  que proporcionalmente  les  correspondiera  soportar. Hay algunas normas para organizar la devolución de capital integrado de manera que no afecte el funcionamiento de la coope.

> La LC considera excedentes repartibles exclusivamente aquéllos que surjan de la diferencia entre  el  costo  y  el  precio  de  los  servicios  prestados  a  los  asociados.  Los  excedentes  que provengan  de  servicios  prestados  a  no asociados  o  que  surjan  de  operaciones  ajenas  al objeto  social  (venta  de  un bien del activo  fijo,  por ejemplo)  deben  destinarse  a  reserva irrepartible.

El excedente repartible se destina de la siguiente manera:
- 5% reserva legal
- 5% estimulo del personal (coopes de servicio)
- 5% promoción y eduación cooperativa

El resto se distribuye entre lxs asociadxs en proporción a las transacciones realizadas con la cooperativa, a esta distribución se le llama **"retorno"**.

No se adminten Socios Inversores.

No se admite la transformación de las cooperatias en otra clase de organización. En caso de disolución, se devuelve el valor nominal del capital suscripto y lo que "sobra" se devuelve al estado para promoción de las cooperativas.

### Impuestos

Si bien no hay disposiciones en materia de impuestos ni una ley específica, el tratamiento impositivo se encuentra disperso en las distintas leyes que conforman el régimen tributario general.

Las cooperativas se encuentran exentas del impuesto sobre la renta, o "impuesto a las ganancias", pero lxs asociadxs deben tributar sobre los intereses y retornos que perciben de ellas.

### Otras características

Las cooperativas se hallan sujetas a la revisión estatal por parte del INAES:

>  El INAES  dispone  de  amplias  facultades  de  fiscalización  que  incluyen  la  aplicación  de sanciones,  pero  la  anulación  de  resoluciones o  el  desplazamiento  de  los  órganos  de  la cooperativa  en  caso  de  violación  del la  ley  o  el  estatuto  se  hallan  reservados  a  la  decisión judicial. Las sancionesque aplique el INAES, así como las resoluciones relacionadas con la autorización  para  funcionar  y con la  aprobación  de  reformas  de  estatutos,  son  recurribles ante la justicia.

### Puntos de conflicto

- Los aspectos impositivos no están incluidos en la LC, sino en las leyes tributarias. Esto hace que las coops no sean reconocidas en su naturaleza de empresas sociales, y se las trate como empresas de capital.
- La Constitución Nacional no las menciona, por lo tanto no reconoce su caracter solidario y de promoción económica y social.
- No hay legislación para favorecer las coopes en materia de compras públicas
- Los trámites para la cración de cooperativas son muy extensos.
- Hay conflictos entre la legislación cooperativa y las normas sectoriales, por ejemplo, con la Ley de trabajo, donde se confunde una relación de dependencia en lugar de asociadx.

