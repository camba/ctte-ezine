---
title: Septiembre 2023
---

# Cambá CTTE Ezine Septiembre 2023

<div style="text-align: center;">

![Libros](@img/septiembre2023.jpg)

</div>

```
Qué pasa con lo que está pasando
En cada ciudad, en cada pueblo
inmobilizarnos es el plan
Ellos nos tienen en la palma de sus manos

Cuando fingimos que estamos muertas
No pueden oír ni una palabra de lo que hemos dicho
Cuando fingimos que estamos muertas

Vamos, vamos, vamos, vamos, vamos

Demos la vuelta a la tortilla con nuestra unidad
No son ni morales ni mayoria
Despertate y huele el café
O decile no a la individualidad

Cuando fingimos que estamos muertas
No pueden oír ni una palabra de lo que hemos dicho
Cuando fingimos que estamos muertas

Vamos, vamos, vamos, vamos, vamos
```

Letra de la canción [Pretend We are dead](https://www.youtube.com/watch?v=g-UOTOqtU0M) de [L7](https://es.wikipedia.org/wiki/L7). Mes próximo [tocan en Argentina](https://indiehoy.com/recitales/l7-llega-por-primera-vez-a-la-argentina/) por primera a casi 40 años de juntarse por primera vez a tocar.

## Cooperativas/Economía Popular
- [Fabrica de inventos](https://ltc.camba.coop/2023/08/15/fabrica-de-inventos-edicion-invierno/): Cambá estuvo con el [LTC](https://ltc.camba.coop/) en los museos de Quilmes durante las vacaciones de invierno.
- [Modelo de gestión de la Cooperativa de trabajo Isthmus - engineering & Manufacturing](CTTE-texto-isthmus)

## Contribuciones Software libre
> Proyecto Colmena una nueva oportunidad para ir colaborando con lo que vamos necesitando y encontrando al paso.
- Múltiples contribuciones a **Nextcloud-async** (interfaz python para Api de nextcloud)
    - [Async Olvidados](https://github.com/aaronsegura/nextcloud-async/pull/16)
    - [NextcloudRichObjects](https://github.com/aaronsegura/nextcloud-async/pull/17)
    - [Talk Api](https://github.com/aaronsegura/nextcloud-async/pull/18)
    - [Más Async olvidados](https://github.com/aaronsegura/nextcloud-async/pull/19)

## Herramientas de software
- [Deno Python](https://github.com/denosaurs/deno_python): Bindings para Interpretar Python en Deno.
- [Pipx](https://pypa.github.io/pipx/): Instala y correo aplicaciones Python en ambiente aislados.
- [Text Paint](https://github.com/1j01/textual-paint): Paint en la terminal
- [Whisper.api](https://github.com/innovatorved/whisper.api): API con nivel de acceso de usuarios para transcribir voz a texto usando Whisper ASR.
- [Browser Password Stealer](https://github.com/henry-richard7/Browser-password-stealer): Consegui todoas las claves, tarjeta de credito y favoritos de los navegadores basados en chromium.

## Informes/Encuestas/Lanzamientos/Capacitaciones

- [Investigación sobre Mantenedores Open Source hecho por la Linux foundation.](https://project.linuxfoundation.org/hubfs/LF%20Research/Open%20Source%20Maintainers%202023%20-%20Report.pdf)
- Muerte de [Bram Moolenaar](https://es.wikipedia.org/wiki/Bram_Moolenaar) creador de VIM. Más info en su [web](https://moolenaar.net/). Extracto de [su última entrevista](https://evrone.com/blog/bram-moolenaar-interview)
> Esto (Programas construidos en base a pegar código de diferentes orígenes) suele suceder cuando quienes encargan el software no tienen conocimiento sobre cómo se hace software. He estado trabajando para una empresa donde bastantes gerentes, educados en física y mecánica, pensaban que el software era igual al que conocían y que podían decidir cómo hacerlo. Esa empresa fue cuesta abajo y finalmente fue absorbida. Lo mismo sucede en lugares donde los tomadores de decisiones pueden salirse con la suya, como en el gobierno. Las **personas que escriben el código probablemente solo se aseguran de que les paguen y luego huyen de la escena del crimen**. En el otro extremo de la escala están las **personas que quieren escribir un código hermoso, dedican mucho tiempo a él y no les importa si realmente hace lo que se pretendía que hiciera o cuál era el presupuesto**. *En algún punto intermedio, hay un equilibrio.*

### Próximos Eventos
- [Pyday 2023](https://eventos.python.org.ar/events/pyday-laplata-2023/): 9 de Septiembre de 2023 Ciudad de La Plata
- [Eko party](https://www.eventbrite.com.ar/e/registro-ekoparty-security-conference-2023-675521030587): 1, 2, 3 de Noviembre 2023 en Ciudad de Buenos Aires
- [Jornadas Regionales de Software Libre](https://eventol.flisol.org.ar/events/jrsl-cordoba-2023/): 13, 14, 15, 16 de Septiembre de 2023 en Ciudad de Cordoba
- [Flashparty](https://flashparty.rebelion.digital/index.php?option=com_content&view=article&id=117:flashparty-2023-es&catid=16&lang=es&Itemid=132): Sábado 28 de octubre en la Ciudad de Buenos Aires
- [Campeonato Nacionales Argentinas 2023 SpeedCubing](https://www.worldcubeassociation.org/competitions/NacionalesArgentinas2023):1, 2, 3 de septiembre en Ciudad de Buenos Aires

### Aniversarios

- [Debian cumple 30 años](https://bits.debian.org/2023/08/debian-turns-30.html)
- [Proyecto GNU cumple 40 años](https://www.gnu.org/gnu40/)
- [Slackware cumple 30 años](https://www.theregister.com/2023/07/20/slackware_turns_30/)

## Noticias, Enlaces y Textos

### Tecnología
- [Repo para aprender react](https://dev.to/livecycle/top-github-repositories-to-learn-modern-react-development-5d3h)
- [Como crear tu primer personaje en Godot](https://gamedevacademy.org/player-character-godot-tutorial/)
- [Grupo Rhysida hackeo a PAMI](https://www.clarin.com/tecnologia/hackeo-pami-ciberdelincuentes-publican-informacion-robada-historias-clinicas-estudios-datos-personales_0_oVEAPipTS0.html) - [Onion](http://rhysidafohrhyy2aszi7bm32tnjat5xri65fopcxkdfxhi4tidsg7cad.onion/)
- [Hackeo del protocolo Exactly](https://www.forbesargentina.com/innovacion/un-hackeo-us-7-millones-preocupa-industria-crypto-argentina-n39392)
- [Explicación sobre como el Argentino, Federico Ezequiel Jaime, consiguio 200M y los devolvio](https://www.coinage.media/s2/he-stole-200-million-he-gave-it-back-now-hes-ready-to-explain-why) - [IDM](https://etherscan.io/idm?tx=0x5e7646362e973152d27959ceed22ae45d6fe2674a33edaa0676e455cc34a3932)


### LLM

- [Introducción a LLMs](https://pattersonconsultingtn.com/content/intro_to_llms_ebook.html)
- [Poniendose a tiro con el extraño mundo de los LLMs](https://www.youtube.com/watch?v=h8Jth_ijZyY)
- [Ataques sobre modelos de lenaguaje alineados](https://llm-attacks.org/index.html#examples)
- [LLM CLI con modelos hosteados](https://simonwillison.net/2023/Jul/12/llm/)
- [Intregración Langchain con cubejs](https://cube.dev/blog/introducing-the-langchain-integration)
- [Corriendo mi propio LLM](https://nelsonslog.wordpress.com/2023/08/16/running-my-own-llm/)
