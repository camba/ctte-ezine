# [stateofjs](https://2019.stateofjs.com/)

> Encuesta anual a personas que programan javacript
> Con visualización de datos de [Amelia](https://wattenberger.com/)

## [Demografía](https://2019.stateofjs.com/demographics/)

Para analizar los resultados, en algunos casos me parecío interesante ponerlos en relación a quienes somos, a nuestras particularidades, como para poder pensarnos desde ahí.

- Contestaron la encuesta **195** persons de *Argentina* y representa solo el **1%** del total
- *Salarios Anual en dolares:* El **15.9%** entre **10K-30K** (nuestro nivel de retiros)
- Aproximadamente **64%** tiene una experiencia entre **2 y 10 años**
- Solo **13%** trabaja en una empresa con la cantidad de personas que nosotras tenemos
- Solo contestaron **6%** de mujeres, **0.8%** de no-binario/3er genero, **1.9%** prefirio no decirlo
- Casi el **50%** se autodenomina "Full stack developer"

## [Características](https://2019.stateofjs.com/features/)

- Más del **85%** usa *Destructuración*, *operador de Spread*, *funciones con flecha*, *Async/await* y *Promesas*
- Solo el **17%** usa *proxies* y **38%** *decoradores*
- Estrucuturas de datos: Maps **70%**, Sets **56%**, typed array **28%**
- BrowserApi: Fetch **81%**, i18n **41%**, localStorage **%88**, Service Workers **36%**, Web Animations **14%**, WebAudio **20%**, WebComponents **27%**, WebGL **16%**, WebRTC **11%**, Websockets **60%**, WebSpeech **7.5%**, WebVR **3%**
- Otros: Progresive Web Apps **48%**, WebAssembly **7%**

## Herramientas más populares

- [Frontend Frameworks](https://2019.stateofjs.com/front-end-frameworks/): React, Vue.js, Angular, Preact, Ember, Svelte
- [Data layer](https://2019.stateofjs.com/data-layer/): Redux, Apollo, GraphQL, Relay, Mobx, Vuex
- [Backend Frameworks](https://2019.stateofjs.com/back-end/): Express, Next.js, Koa, Meteor, Sails, Feathers, Nuxt, Gatsby
- [Testing](https://2019.stateofjs.com/testing/): Jest, Mocha, Storybook, Cypress, Enzyme, AVA, Jasmine, Pupetter
- [Mobile & Desktop](https://2019.stateofjs.com/mobile-desktop/): Electron, React Native, Native Apps, Cordova, Ionic, NW.js, Expo
- [Otras herramientas](https://2019.stateofjs.com/other-tools/):
    - **Utilidades:** Loadash, Moment, Jquery, data-fns, Rxjs, Underscore, Ramda, Immer
    - **Editores:** VsCode, Webstorm, Vim, Sublime Text, Atom, Emacs
    - **Browsers:** Chrome, Firefox, Safari
    - **Build tools:** Webpack, gulp, Parcel, Rollup, Browserify, Fusebox
    - **Otros lenguajes:** Python, C#, NET, Ruby, PHP, C/C++, Rust

## Algunos [Contenidos](https://2019.stateofjs.com/resources/) populares

- **Blogs**
    - [CSS trics](https://css-tricks.com/)
    - [Dev.to](https://dev.to/)
    - [Js Weekly](https://javascriptweekly.com/)
    - [Smashing Mag](https://www.smashingmagazine.com/)
    - [David Walsh](https://davidwalsh.name/)
    - [Overreacted](https://overreacted.io/)
    - [Kent](https://kentcdodds.com/blog/)
- **Cursos**
    - [StackOverflow](https://stackoverflow.com/)
    - [Udemy](https://www.udemy.com/)
    - [W3School](https://www.w3schools.com/)
    - [MDN](https://developer.mozilla.org/en-US/)
    - [Egghead](https://egghead.io/)
    - [Fronend Masters](https://frontendmasters.com/)
- **Podcasts**
    - [React Podcast](https://reactpodcast.com/)
    - [Syntax](https://syntax.fm/)
    - [The changelog](https://changelog.com/)
    - [Frontend Happy hour](https://frontendhappyhour.com/)
    - [DevChat](https://devchat.tv/js-jabber/)
    - [FullStack radio](http://www.fullstackradio.com/)
    - [Shop talk](https://shoptalkshow.com/)
    - [Software engineer daily](https://softwareengineeringdaily.com/)

## [Premios](https://2019.stateofjs.com/awards/)

- **Más usada:** React
- **Más satifacción:** Jest
- **Más interes:** GraphQL
- **Predicción:** Svelte
