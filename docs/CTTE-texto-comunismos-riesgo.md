# El comunismo de riesgo de Dmytri Kleiner

> Leído y traducido de [Dmytri Kleiner’s Venture Communism](https://blog.p2pfoundation.net/dmytri-kleiners-venture-communism/2017/07/10)

## Articulo

Michel Bauwens: En un informe de la Fundación P2P, [Value in the Commons Economy](http://commonstransition.org/value-commons-economy/), describimos y propusimos una estrategia de ‘[transferencia](https://wiki.p2pfoundation.net/Transvestment)‘, el cual es un proceso donde el capital es transferido de un modo de producción, por ejemplo, desde el capital privado usado para la acumulación de capital hacia la utilización para expandir los cosas comunes y crear sustento para los comuneros, en sus propios terminos, no en los terminos del capital.

Esto es derivado de la propuesta original del grupo Telekommunisten, donde las entidades capaces de hacer esa transferencias se llamán **Comunidades de riesgo**.

El siguiente [texto corto](https://ianwrightsite.wordpress.com/2016/12/14/blog-post-title/) y audio explican las bases de la idea y esta práctica.

Ian Wright: El comunismo de riesgo de Dmytri Kleiner es una propuesta reciente para ir desde un lugar a otro. Podes descargarte el manifesto de Dmytri [Descargar](http://telekommunisten.net/the-telekommunist-manifesto/)

La idea central en una nueva clase de organización/institución la **Comuna de riesgo**, una asociación de cooperativas que es única propietaria de todos los bienes constitutivos de esas mismas cooperativas de trabajo.

Toda la tierra, edificios, capital, etc son alquilados por las cooperativas a la Coumindad de riesgp. Todos las personas asocieadas a las cooperativas automáticamente son miembros de las comunidad. Por lo tanto, de los medios de producción son propiedad comunal.

En teoría esta organización resuelve el problema de la distribución altamente desigual entre del capital entre las cooperativas de trabajo en la economía de mercado.

Adicionalmente, la comuna de riesgo asigna democrativamente los fodos a nuevos emprendimientos de cooperativas de trabajo. La idea acá es que las instituciones de la comuna de riesgo compite con las instituciones del capitalismo de riesgo y eventualmente desplazar a estos ultimos.

Brinde una charla de media hora sobre las ideas de Dymtri es Oxford, UK, que dan más detalles. Podes escuchar el audio acá:

[![VENTURE COMMUNISM](http://img.youtube.com/vi/C-cViPD1-Jo/0.jpg)](https://www.youtube.com/watch?v=C-cViPD1-Jo "Venture Communism")

En la charla me refiero a las relaciones de entradas/salidos de la comuna de riesgo con el sector capitalista que lo contiene. Este diagrama resume como fluye el dinero:

![venture communism](@img/venturecommunism.png)

El comunismo de riesgo es precisamente el tipo de propuesta de institución que satisface la necesidad de la economía politica del socialismo y que además una nueva practica politica. Para mi, es una referencia como un punto de partida.


