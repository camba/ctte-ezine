# [Bombozila.com](https://bombozila.com/)

Nico nos comparte sobre esta plataforma de contenidos audiovisuales.

> Bombozila é uma plataforma de acesso a documentários independentes, que retratam as lutas sociais globais, fundada em 2016 no Rio de Janeiro | Brasil

## Misión
Somos uma plataforma de documentários que contam a história sociopolítica dos últimos anos. Damos destaque para realizações de pequenos produtores, coletivos de cine comunitário, e documentaristas que atendem à urgência política dos nossos territórios em luta e resistência.

Além do catálogo de filmes realizamos regularmente oficinas de formação audiovisual em articulação com diversos realizadores, coletivos e cineclubes para democratizar o acesso à comunicação e fomentar a produção audiovisual popular.

Também realizamos projeções públicas e campanhas de impacto social para impulsionar o acesso aos documentários urgentes e promover a aproximação entre filmes e audiências estratégicas para a transformação social. Também apoiamos a realização da “Semana Pela Soberania Audiovisual”, que é um movimento que acontece em diversos países da América Latina e Caribe. Países representados: México, Cuba, Guatemala, Nicaragua, Porto Rico, El Salvador, Equador, Colômbia, Peru, Bolívia, Wallmapu, Argentina, Chile, Estados Unidos, Canadá, Honduras, El Salvador e Brasil.

[Nota en Medium sobre Bombozila](https://medium.com/revista-bem-te-vi/bombozila-descolonizando-o-audiovisual-pela-soberania-do-vis%C3%ADvel-4745056866c1)
