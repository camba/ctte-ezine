# Coordinación técnica, tecnológica y educativa

## Fundamentación

Evaluamos colectivamente que hay pocos espacios destinados a potenciar los servicios y productos que actualmente comercializamos.
A veces además sub utilizamos los espacios que actualmente tenemos con éste fin.
Por otro lado carecemos de tiempo destinado a repensar y crear capacidades futuras en potenciales para generar nuevos productos y/o servicios.

## Objetivos

- Desarrollo de nuevos servicios o productos
- Mejorar los servicios y productos existentes
- Desarrollo de otros servicios o productos que eventualmente no puedan desarrollarse en otros espacios

## Finalidad

Cumplir con el objetivo propuesto de mejora, nos permitirá acelearar el proceso de aprendizaje y adquisición de nuevas capacidades para tener un mejor balance entre la experiencia necesaria, los tiempos a cumplir y la calidad del resultado del trabajo. Por otro lado el desarrollo de nuevos productos y servicios asegura tener potenciales negocios a futuro.
Además se generarán capacidades en el espacio para tener más:

 - Visión global sobre los proyectos
 - Proyección de los proyectos futuros
 - Potenciar los valores cooperativos

## Beneficiarios

El espacio tiene 3 diferentes grupos de beneficiaros que son:

 - El equipo productivo (nuevas capacidades)
 - El equipo de gestión (nuevos productos y servicios)
 - Indirectamente algunos clientes (Desarrollo de Prototipos para que otros equipos lo continuen)

## Condiciones básicas

Hay ciertas condiciones necesarias para poder obtener los mejores resultados.

 - Cantidad de personas acorde al tiempo de tareas decididas a encarar en el plan de trabajo.
 - Dialogo e interacción con los diferentes espacios de gestión para discutir rumbos de los nuevos productos y servicios.
 - Apertura e involucramiento del equipo productivo en el proceso de trabajo (recibir consejos, cambiar hábitos de trabajo, disponibilidad información de trabajo, etc.)

## Ejes de acción y actividades a realizar

### Evaluación

 - Diagnóstico de potenciales problemas productivos
 - Generar Radar Tecnológico

### Planificación

 - Definir arquitecturas o tecnologías con las cuales trabajar
 - Participación en estimación para nuevos proyectos
 - Participación en decisiones relacionadas a la conformación de equipos técnicos
 - Participación en reuniones con clientes dando soporte sobre tecnologías
 - Supervisión sobre decisiones técnicas

### Innovación

 - Investigación de herramientas y nuevas tecnologías
 - Consultoría Tecnológica y educativa
 - Desarrollar productos específicos para equipos internos
 - Desarrollo de prototipos para potenciales nuevos productos y/o servicios

### Soporte

 - Asistencia en la liberación de código fuente
 - Asistencia para la resolución de problemas técnicos
 - Transferencia de conocimiento
 - Complementar equipos en el comienzo de un nuevo proyecto
 - Formación de equipo productivo
   - Generación de capacitaciones específicas
   - Acompañamiento de Procesos de aprendizajes

## Responsables

Este nuevo equipo deberá crecer a demanda y evolución de la organización.
De hecho para tener una idea según OPSSI (“Observatorio Permanente de la Industria de Software y Servicios Informáticos de la Argentina”) involucra al 10,6% de su personal e invirtiendo el 7,7% de la facturación, lo que representa un total de US$ 202 millones.

El responsable de este área será José Luis Di Biase, y deberiamos poder complementar su trabajo en los siguientes meses.

## Modalidades de operación

Para llevar a cabo las actividades antes descriptas se pondrán en juego diversas herramientas y metodologías tales como:

 - Entrevistas presenciales
 - Encuestas
 - Trabajo en conjunto
 - Capacitaciones especificas
 - Pair Programming
 - Conformación de equipos de investigación

