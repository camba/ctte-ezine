---
title: Mayo 2023
---

# Cambá CTTE Ezine Mayo 2023

<div style="text-align: center;">

![Libros](@img/mayo2023.png)

</div>

```
Hay angustias que te dejan esquelético
No se calman ni con tandas de analgésicos
So let it go, que esta vez no es hipotético
Y si no soltás la mierda, vas a terminar colérico

Y entiendo que estés cansado
Hay veces que el no presente es una cuestión que aturde
Pasando del remordimiento de un hecho pasado
Al futuro que es un pozo plagado de incertidumbre

Y me ofrecieron mucho por cumplir con el perfil de imbécil
Y aunque tanto no me alejo, elijo lo difícil
Eso me conduce a cierta parálisis
Bardo afuera y bardo adentro, doble crisis

Ya no hago análisis
Estoy cortando los cartones de la bici
Juego con lo poco estable de mi psiquis
Pero todo va a estar bien, así que take it easy

Mi alma se pone fea cuando el día amanece
Y el sol pregunta a gritos: "¿hoy qué vas a hacer?"
Vuelvo a encontrar refugio donde to' oscurece
Y mis vergüenzas bailan sin que vos las puedas ver

Este es mi flash, así nomás y qué más da si no les llega a todos
Eso que les encanta es una trampa para bobos
Analizan las palabras, en algún lugar los jodo
No saben que no hay un qué si antes no existe un cómo (cómo)

Peco de acudir al ego
Y yo me inventé un lugar pa' aportarle magia al juego
Hacés siempre lo mismo, gil, te faltan huevos
No hables de identidad cuando te encasillás por miedo

Perdido en la muchedumbre, eh
Entre sueños que te aturden, eh
En la calma que se pudre, ah
Tu carne es incertidumbre
Esperanza que se pudre, eh
No dejes que sea costumbre, eh
El vivir entre la mugre, mugre
Ese brillo que te cubre, una capa que se pudre, ya

Miro para arriba, cae pánico
Lluvia en mi cara, tipo pálido
Quiero ser mágico como ese conejo
Que está corriendo atrás de tu reflejo, ¿no lo ves?

Uno, dos, tres, un bucle de estrés que nos atrapa
Silencios que aprietan la garganta
Palabras encriptadas, cae el hampa
A la verdad no puedo hacerle trampa

Entré fuerte, así como un trago de aguardiente
Y te hablo claro hasta con la faca entre los dientes
En este tramo no me lleva la corriente
Todo lo que derramo es lo que emano de mi vientre

Y hablar de mí me suena raro
No puedo dormir cuando afuera llueven mil palos, eh
El humo que nos venden sale cada vez más caro
Nadie se hace cargo del ruido de esos disparos
```

Letra de la canción [Mugre](https://www.youtube.com/watch?v=Me_dIiZP3zc) de [WOS](https://es.wikipedia.org/wiki/Wos_(m%C3%BAsico))

## Contribuciones Software libre
> Último jueves en la clase de la UNQ charlamos de las operaciones con bits y como se hacian con la Biblioteca/Libreria de Arduino. Entre al código y vi como estaba hecho. Como resultado nace esta contribución hecha una mañana de sábado en 10 minutos.
- [Arduino Core](https://github.com/arduino/ArduinoCore-avr/pull/533)

## Herramientas de software

- [AppSmith](https://github.com/appsmithorg/appsmith): Framework para construir paneles de administración, dashboard, etc 10 veces más rápido.
- [Htmlx](https://htmx.org/): Superpoderes para html
- [Doom Linux](https://github.com/shadlyd15/DoomLinux): Scrit para construir un linux desde cero que corre al bootear doom.
- [Damegender](https://github.com/davidam/damegender): ¿Conocer el género por el nombre? [web](https://damegender.davidam.com/)

## Informes/Encuestas/Lanzamientos/Capacitaciones
- [Curso de NLP de Lena Voita](https://lena-voita.github.io/nlp_course.html)
- [Commandlinefu](https://www.commandlinefu.com/) : Compendio de scripts para la linea de comandos
- [Lista de playgrounds para programación](https://jvns.ca/blog/2023/04/17/a-list-of-programming-playgrounds/)
- Lanzamientos nuevos:
    - [Node v20](https://nodejs.org/en/blog/announcements/v20-release-announce)
    - [Ionic 7](https://ionic.io/blog/ionic-7-is-here)
    - [Ardunio uno r4](https://blog.arduino.cc/2023/03/25/arduino-uno-r4/)
    - [Godot 4](https://godotengine.org/article/godot-4-0-sets-sail/)
    - [Electron 10 años](https://www.electronjs.org/blog/10-years-of-electron)

## Noticias, Enlaces y Textos

### Sociales/filosóficas
- [Eliezer Yudkowsky: Los peligros de la IA y el fin de la civilización humana | Lex Fridman #368](https://www.youtube.com/watch?v=AaTRHFaaPG8)
- [El Método Rebord #48 - Alejandro Dolina](https://www.youtube.com/watch?v=OA1biHKSyTw)
- [Punto de Emancipación 37 - Juan Grabois](https://www.youtube.com/watch?v=2HQ4DtGe4u4)
- [ACELERAR para TERMINAR con el CAPITALISMO | Nick Land y el Aceleracionismo](https://www.youtube.com/watch?v=QWgh66cERqU)
- [El FIN de la HUMANIDAD contra las IA | Robots y Capital](https://www.youtube.com/watch?v=CoZLD93A4Ec)

### LLM
- [Alpaca Electron](https://github.com/ItsPi3141/alpaca-electron): La forma más simple de correr Alpaca (y otros LLaMA-based local LLMs) en tu compu.
- [Gpt4all](https://gpt4all.io/): GPT4All Chat de IA local -  [Fuente general](https://github.com/nomic-ai/gpt4all), [Chat](https://github.com/nomic-ai/gpt4all-chat)
- [Vicuna](https://vicuna.lmsys.org/): Chatbot libre para impresionar a GPT-4 - [fuente](https://github.com/lm-sys/FastChat)
- [Vicuna AI vs ChatGp](https://www.youtube.com/watch?v=SBGLFTvW16E)
- Ataques LLM - Inyección en Prompt: [Parte 1](https://www.youtube.com/watch?v=h74oXb4Kk8k) [Parte 2](https://www.youtube.com/watch?v=Sv5OLj2nVAQ)
    - [Ataque Gpt Prompt](https://gpa.43z.one/)
    - [ChatGpt DAN y otros jailbreak](https://github.com/0xk1h0/ChatGPT_DAN)


## Cooperativas/Economía Popular

- [Nota a FACTTIC en revista HECHO EN BUENOS AIRES número 270 - Conocimiento Cooperativo](./assets/hechoenbsas.pdf)
- [Cambá participó en el  Festival Latinoamericano de Instalación de Software Libre 2023](https://blog.camba.coop/flisol-2023/)
- [Cierre de actividades de pycamp 2023](https://www.youtube.com/watch?v=oX46gGFMQ_Q)
- [Maia Numerosky, Licenciada en Matemática y científica de datos en la cooperativa de software ERYX, habló sobre las consecuencias que podría tener el uso de la inteligencia artificial.](https://www.radiosur.org.ar/noticia.php?id=14694)
- [Reseña del plenario FACTTIC 2023 en Villa La Angostura](https://www.idelcoop.org.ar/sites/www.idelcoop.org.ar/files/revista/articulos/pdf/resenas_1.pdf)

