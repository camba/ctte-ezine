---
title: Julio 2024
---

# Cambá CTTE Ezine Julio 2024

<div style="text-align: center;">

![Libros](@img/julio2024.jpg)

</div>

```
Ciclope de soledad
puede matarme resucitarme
tiempos de memoria y paz
suelen calmarme, desesperarme

cuentos de mediocridad
pueden cegarte, anarquizarme
yo ya no les creo mas
y eso fue antes de controlarme

dejen que el cielo lo maneje algun dios
la pesadilla ni siquiera empezo
y la sangre donde sangra el dolor
de mi corazon

ciclope de soledad
puede matarme, resucitarme
tiempos de memoria y paz
suelen calmarme, desesperarme

y es imposible no poder respirar
la hipocresia de esta inmensa ciudad
los dias pasan como pasan la voz
de esta cancion
y asi sale el sol..
```

Letra del tema [Así sale el sol](https://www.youtube.com/watch?v=1lgiw3rRfQY) de [Smitten](https://es.wikipedia.org/wiki/Smitten_(banda)).

## Contribuciones Software libre de Cambá
- [F1Prode](https://github.com/SifonFelipe/F1Prode/commits?author=juanmanueldaza&since=2024-06-01&until=2024-06-26): Interfaz para jugar al prode de Formula 1
- [Simpyt](https://github.com/fisadev/simpyt/pull/28): Editor para controlar las configuraciones para un Virtual Deck
- [Fabric](https://github.com/danielmiessler/fabric/pull/621): Se agrega opción para mostrar version en en cli de interfaz con IAs
- [Page-Assist](https://github.com/n4ze3m/page-assist/pull/128): Traducción al español de plugin del navegador para interactuar con IAs
- [EventoL](https://github.com/eventoL/eventoL/pull/824): Migraciones paso a paso de versiones de Django de Software para Manejo de Eventos


## Herramientas de software
- [HTTPie](https://httpie.io/) - Cliente HTTP simple y rápido
- [Mesop](https://github.com/google/mesop/) - Construi web UIs rápidamente
- [Kantouch](https://gitlab.com/osiux/kantouch): TUI para maneja KanBan con soporte para Touchscreen y markdown
- [Wxt](https://wxt.dev/): Framework para extensiones de Navegador
- [loooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo](https://github.com/ccbikai/loooooooooooooooooooooooooooooooooooooooooooooooooooooooooooooo.ong): Alargador de URLs


## Informes/Encuestas/Lanzamientos/Capacitaciones
- [Resultado de la Encuesta de sueldos 2024 - Primer parte](https://sysarmy.com/blog/posts/resultados-de-la-encuesta-de-sueldos-2024-1/)
- [Resultados de la Encuesta "State of JS 2023"](https://2023.stateofjs.com/en-US)
- [Evento Hackea La Tierra #9 Acciones de resistencia anti-distópica](https://hlt.calafou.org/es)
- [OpenAPI: Capacitación gratis de la Linux Foundation](https://training.linuxfoundation.org/express-learning/openapi-fundamentals-lfel1011/)
- [Finalmente Julian esta Libre](https://www.aljazeera.com/news/2024/6/25/julian-assange-is-free-wikileaks-founder-freed-in-deal-with-us) - [Julian Assange](https://es.wikipedia.org/wiki/Julian_Assange)

## Noticias, Enlaces y Textos

### Sociales/filosóficas
- [Constantino Cavafis. Poema «ÍTACA»](https://epistemomania.com/constantino-cavafis-itaca/) - [Análisis](https://www.youtube.com/watch?v=-xcZWQ6pTeI) - [Isla de Itacá](https://es.wikipedia.org/wiki/%C3%8Dtaca)
- [Trabajo Tóxico: Cuando el trabajo pierde el sentido](https://www.youtube.com/watch?v=EgtUFFclM5U)
- [Salir del conformismo ingenuo y "HACER DISIDENCIA" - Eric Sadin.](https://www.youtube.com/watch?v=6icIACgrWkE)
- [Cuando uno se compara con ChatGPT ya perdio](https://www.pagina12.com.ar/738263-miguel-benasayag-cuando-uno-se-compara-con-chat-gpt-ya-perdi), [Audio](https://ar.radiocut.fm/audiocut/inteligencia-artificial-y-dimension-humana/) - [Miguel Benasayag](https://es.wikipedia.org/wiki/Miguel_Benasayag)


### Tecnología
- [Les programadores 10x contagian mejoras a todo su equipo](https://stackoverflow.blog/2024/06/19/the-real-10x-developer-makes-their-whole-team-better/)
- [La IA no va a formar a tu equipo de progamación](https://simonwillison.net/2024/Jun/12/generative-ai-is-not-going-to-build-your-engineering-team/)
- [El bug más loco que encontre en toda mi carrea en la informática](https://threadreaderapp.com/thread/1793930355617259811.html)
- [Compilando MSDOS 4.0](https://0x705h.com/retro/2024/04/28/compilando-msdos-4.0.html)
- [Programador de Élite Revela: Cómo Nos Espían y Qué Hacer al Respecto - Ola Bini](https://www.youtube.com/watch?v=Zm1JXJAndi4)

### LLMs
- [Tres tipo de programadorxs asistidxs por IA](https://stackoverflow.blog/2023/12/11/three-types-of-ai-assisted-programmers/)
- [Mejoras en la Construcción éxitosa de software con el soporte de IAs](https://markus.oberlehner.net/blog/ai-enhanced-development-building-successful-applications-with-the-support-of-llms/)
- [Construir aplicaciones potenciadas con IA en React, Vue, Svelte y Solid](https://github.com/vercel/ai)
- [Machine Learning Operations: ML-Ops](https://ml-ops.org/)
