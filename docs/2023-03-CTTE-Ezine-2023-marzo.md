---
title: Marzo 2023
---

# Cambá CTTE Ezine Marzo 2023

<div style="text- align: center;">

![Agave](@img/marzo2023.jpg)

</div>


```
Quem sabe eu ainda sou uma garotinha
Esperando o ônibus da escola, sozinha
Cansada com minhas meias três quartos
Rezando baixo pelos cantos
Por ser uma menina má

Quem sabe o príncipe virou um chato
Que vive dando no meu saco
Quem sabe a vida é não sonhar

Eu só peço a Deus
Um pouco de malandragem
Pois sou criança
E não conheço a verdade
Eu sou poeta e não aprendi a amar
Eu sou poeta e não aprendi a amar

Bobeira é não viver a realidade
E eu ainda tenho uma tarde inteira
Eu ando nas ruas
Eu troco um cheque
Mudo uma planta de lugar
Dirijo meu carro
Tomo o meu pileque
E ainda tenho tempo pra cantar

Eu só peço a Deus
Um pouco de malandragem
Pois sou criança
E não conheço a verdade
Eu sou poeta e não aprendi a amar
Eu sou poeta e não aprendi a amar

Eu ando nas ruas
Eu troco um cheque
Mudo uma planta de lugar
Dirijo meu carro
Tomo o meu pileque
E ainda tenho tempo pra cantar

Quem sabe eu ainda sou uma garotinha!
```

Letra de la canción [Malandragem](https://www.youtube.com/watch?v=bFRBQjBuQmo) de [Cassia Eller](https://es.wikipedia.org/wiki/C%C3%A1ssia_Eller)


## Herramientas de software

- [Sharegtp](https://sharegpt.com/): Comparti tus chats de ChatGpt - [github](https://github.com/domeccleston/sharegpt)
- [TimeShift](https://github.com/linuxmint/timeshift): Herramientas de backup y restore para sistemas gnu/linux.
- [Open Assistant](https://open-assistant.io/es): Asistente por chat con un modelo que será abierto. - [github](https://github.com/LAION-AI/Open-Assistant)
- [Grapheneos](https://grapheneos.org/): Sistema operativo móvil seguro y con privacidad compatible con Android.


## Informes/Encuestas/Lanzamientos/Capacitaciones

- [Ingenieros de prompting](https://www.washingtonpost.com/technology/2023/02/25/prompt-engineers-techs-next-big-job/): ¿Es realmente el próximo gran trabajo de tecnología?
- [Presentación](https://view.genial.ly/63e127cc69acc200122326ec) que brinda recursos para contribuir a la igualdad de género en la ciencia en conmemoración del **11 de febrero Día internacional de las niñas y las mujeres en la ciencia**.
- [Adventofcode](https://adventofcode.com/2022/about): Ejercicios/Rompecabezas de programación que estamos usando como prácticas para los ingresantes.
- [Costo de computación](https://compute-cost.com/): Herramienta de calculo de costo de computación de diferentes servicios (Actualmente para AWS).
- [Encuentros de la 2600](https://www.2600.com/meetings): Míticas reuniones hackers con una historia de más de 20 años, y podes ir en Buenos Aires (Primer viernes del mes).
- [Pycamp 2023](https://pyar.discourse.group/t/pycamp-2023-en-corrientes/1021): Campamento de progrmación python, Corrientes, del Jueves 23 de Marzo al domingo 26 de Marzo.

## Noticias, Enlaces y Textos

### Sociales/filosóficas

- [Extinction Internet](https://networkcultures.org/blog/publication/extinction-internet/): Libro sobre como pensar el fin de internet, [Geert Lovink](https://en.wikipedia.org/wiki/Geert_Lovink). [Entrevista](https://bluelabyrinths.com/2023/01/02/the-end-of-the-internet-an-interview-with-geert-lovink/)
- [Entrevista a Noam Chomsky sobre ChatGPT, educación, etc](https://www.youtube.com/watch?v=IgxzcOugvEI)
- [Javier MILEI y la LIBERTAD para morirse de HAMBRE | Rolando ASTARITA](https://www.youtube.com/watch?v=TW1iKsV4EZk)
- [Todo corre prisa - Byung-Chul Han](https://www.youtube.com/watch?v=FW1ukTAGnz8)


### Tecnología
- [ChatGPT con Rob Miles](https://www.youtube.com/watch?v=viJt_DXTfwA): ¿Cómo funciona ChatGPT?
- [Ch(e)at GPT?](https://www.youtube.com/watch?v=XZJc1p6RE78): ¿Cómo saber si te copiaste con ChatGPT?
- [CodeGolf](https://codegolf.stackexchange.com/): Preguntas y respuestas para los aficionados a las competiciones o resolución de rompecabezas de programación.
- [The Commodordion](https://linusakesson.net/commodordion/index.php): Construcción de un acordión con Computadoras Commodore.
- [Catálogo de Tecnologías Locales](https://tecnologiaslocales.org/): Tecnologías para la acción climática busca ser una plataforma para visibilizar y circular distintos artefactos, procesos o sistemas desarrolladas por comunidades.
