---
title: Febrero 2023
---

# Cambá CTTE Ezine Febrero 2023

<div style="text- align: center;">

![Siesta de Verano](@img/febrero2023.png)

</div>

```
Qué tipo de viejo vas a ser
Qué final vas a tener
Vas a dar asco
Vas a dar risa
Vas a ir al campo
Vas a ir a misa
Vas a portarte como un payaso
Para intentar que te hagan caso
O no

Elige el viejo que quieres ser de mayor
Porque esa será tu única elección

Qué tipo de viejo vas a ser
En qué programa vas a aparecer
En Callejeros
En Ana rosa
Hablando al bulto
De cualquier cosa
O tal vez seas una eminencia
Y lo des todo por la ciencia
O no

Tú sabes que hay un viejo en tu interior
Luchando para hacerse con el control

Tú eres muy chungo y él
No va a ser mejor
Tú eres mala persona
Y él va a ser todavía más cabrón
```

Letra de la canción [Shiseido](https://www.youtube.com/watch?v=Xk_0KuTRcME) de [Los punsetes](https://es.wikipedia.org/wiki/Los_Punsetes)


## Herramientas de software

- [ChatGPT](https://github.com/lencx/ChatGPT): Aplicación para escritorio, con soporte de chat prompts. [Acá van para probar](https://github.com/f/awesome-chatgpt-prompts)
- [Octosuite](https://github.com/bellingcat/octosuite): Herramienta para la recoleccion de datos para inteligencia de datos abiertos (OSINT)
- [Chataigne](https://github.com/benkuper/Chataigne): Framework para integracion para intervenciones audiovisuales
- [Linen](https://github.com/linen-dev/linen.dev): Alternativa libre a clientes de chat como Slack y Discord


## Informes/Encuestas/Lanzamientos/Capacitaciones

- [Estado de javascript 2022](https://2022.stateofjs.com/en-US/)
- [Desarrolladorxs utilizando más linux que macos](https://linux.slashdot.org/story/22/12/25/0114237/stack-overflow-survey-finds-more-developers-now-use-linux-than-macos)
- X11 Server: [Momento con menos desarrollo luego de 20 años](https://www.phoronix.com/news/XServer-2022-Development-Pace)
- [Gitops con Gitlab](https://about.gitlab.com/blog/2022/04/07/the-ultimate-guide-to-gitops-with-gitlab/) Compendios de tutoriales hecho por devs de Gitlab para integrar las practicas de Gitops.


## Noticias, Enlaces y Textos

### Sociales/filosóficas

- [Algoritmos entrenados para ser racistas](https://www.pikaramagazine.com/2022/11/algoritmos-entrenados-para-ser-racistas/)
- [OpenIA utiliza trabajadorxs keniatas para "ser" menos racista](https://time.com/6247678/openai-chatgpt-kenya-workers/)
- [Hackfeminismo: afectadas y afectando desde las tecnologías](https://sursiendo.org/2019/08/hackerfeminismo-afectadas-y-afectando-desde-las-tecnologias/)
- [Open Game License: Licencia inspirada en GPL historia y situacion actual](https://hackaday.com/2023/01/11/wizards-slay-the-dragon-that-lays-the-golden-egg/)


### Tecnología
- [La peor placa de video](https://www.youtube.com/watch?v=l7rce6IQDWs)
- Dos Modelos de redes neuronales charlando autonomamente en tu navegador. [Whisper y wasm](https://github.com/ggerganov/whisper.cpp/tree/master/examples/talk.wasm)
- [10 mujeres importantes en la historia de la tecnología](https://www.fundacionaquae.org/wiki/10-mujeres-que-cambiaron-la-historia-de-la-tecnologia/amp/)
- [Experiencia con la gamificacón de Duolingo en primera persona](https://www.cnet.com/culture/internet/duolingo-transformed-me-into-a-monster/)

### Cooperativas/Economía Popular

- [Informe Monitor ACI 2022 ](https://monitor.coop/es) Mencion a FACTTIC dentro del inciso de Digitalización, Participacioón y Democracia
- [Camada Jovén - Generación sin Patrón](https://lavaca.org/mu178/generacion-sin-patron-la-camada-joven/)
