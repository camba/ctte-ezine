---
title: Noviembre 2021
---

# Cambá CTTE Ezine Noviembre 2021

**Existimos**
![primavera](@img/existimos.jpg)

```
Cuando este oscuro y un jamás venga a visitarte
vos solo recordá...

Nunca te olvides de jugar
y jamás te calles aun cuando nadie hable
andá de frente y se leal, sin miedo a encontrar lo bueno en los demás

Si peleás, peleá, si amas, amá
si vas a dar, da más

Recordá que cuidar muchas veces es soltar, que torcer no es quebrar
y el valor de la verdad se ve al contraste

No tengas miedo de cambiar
ni de equivocarte hasta maravillarte
Y si elegís tu libertad
¿Por qué caminas cuando podes volar?

Si peleás, peleá, si amas, amá
si vas a dar, da más

Recordá que ganar no es principio hasta el final
y triunfar vale más solidaria en los demás
y que contagie...

Soñá grande y busca que allá hay muchos mas
de valiosos sin valor que cambie
Si esta vida es tuya amor, y de nadie mas
¿Por qué caminas cuando podes volar?

```

Aquí la letra de **Shaila** el tema [A emilia](https://www.youtube.com/watch?v=so5tZDc5iN8)

## Herramientas de software

- [Ferdi](https://getferdi.com/) - Todos tus servicios de comunicación en un solo lugar.
- [Grub-Invaders](https://github.com/stokito/grub-invaders) - Juego de grub.
- [Dos-like](https://github.com/mattiasgustavsson/dos-like) - Motor para hacer cosas que se sientan com en MS-DOS.
- [Credo](https://github.com/rrrene/credo) - Herramienta de analisis de código estático para Elixir.
- [Autojump](https://github.com/wting/autojump) - Commando CD que aprende para navegar directorios.

## Informes/Encuestas/Lanzamientos/Capacitaciones

- Evento FACTTIC: Mariano Zuckerfeld - [Informacionalización, plataformización y automatización](https://youtu.be/XEpTNVc_fqo)
- Elizabeth Lopez Bidone - [Aportes parael análisis de la rotación laboral tecno-informacional](https://revistas.unlp.edu.ar/hipertextos/article/view/10207/8999)
- AnibalLeaks - [Se filtraron datos de Argentinos](https://twitter.com/search?q=%23AnibalLeaks) - [Muestra](https://cdn-131.anonfiles.com/b5n864N9u1/7707c645-1634255979/afiliados.sql), [Foro](https://raidforums.com/Thread-Argentine-Military-IOSFA-Download-for-Free)
- [Jerarquias de desacuerdos de Graham](https://en.wikipedia.org/wiki/Paul_Graham_(programmer)#/media/File:Graham's_Hierarchy_of_Disagreement-en.svg)

## Noticias, Enlaces y Textos

### Sociales/filosóficos

- [La nueva derecha](http://lobosuelto.com/la-nueva-derecha-sztulwark-fernandez-savater/)
- Discurso, Subjetividad, Coyuntura latinoamericana, [OFENSIVA SENSIBLE / EP1: SZTULWARK - HOROWICZ](https://www.youtube.com/watch?v=p1SXgXdY_dY)
- [Coyuntura politica argentina, Juan Grabois con Jorge Fontevecchia](https://www.youtube.com/watch?v=h_DzH4Fb0zo)
- [El método Rebord] - Dos momentos seleccionados con [Pedro Rosemblat](https://youtu.be/OQ9ZTV1m41M?t=6348) y [Mayra Arena](https://youtu.be/DEtMSDpn1ws?t=5996)


### Tecnología

- [Hackeresas](https://vimeo.com/63894010)
- [La ecuatortura #CiberMessi (olabini)](https://peertube.cybercirujas.club/videos/watch/1527f0d9-0eaa-4904-8d4f-bc73d10ba379)
- [Internet no se puede regular](https://wetoker.com/internet-no-se-puede-regular-bea-busaniche-y-enrique-chaparro-20-anos-derribando-mitos-y-mucho-mas/)
- [TempleOS](https://en.wikipedia.org/wiki/TempleOS) - [Documental](https://www.youtube.com/watch?v=UCgoxQCf5Jg) - [Web](https://templeos.org/)


### Otras

- [Jornadas Argentinas de Didáctica de las Ciencias de la Computación: 4,5,6 de nov 2021](https://jadicc.program.ar/)
- [Comunidades haciendo internet - nuevas redes (altermundi)](https://www.youtube.com/watch?v=fnHJfDJwEiY)
