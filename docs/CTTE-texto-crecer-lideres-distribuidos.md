# ¿Como hacer crecer el liderazgo distribuido?

> Traducción al español de texto visto en [How to grow distributed leadership](https://medium.com/enspiral-tales/how-to-grow-distributed-leadership-7f6b25f0361c)
> Escrito por [Alanna Irving](https://medium.com/@alanna.irving), 25 de Agosto de 2015

## Texto

Hace años que me he estado poniendo en contextos donde no hay jefes, pero que habia muchos lideres. Como coordinar voluntarios en un festival en el desierto, construir una red de emprendedores distribuida, fundar un startup tecnológico cooperativo, diseñar nuevos procesos para participación y hacerse cargo de un Castillo francés para eco hackear el futuro.

Ahora Estoy preguntandome, ¿Cómo crece este tipo de liderazgo? ¿Qué condiciones necesita para florecer? ¿Cómo se nutre? ¿Cómo es su desarrollo? ¿Cómo crecer como lider en una organización donde no hay una escalera que subir?

En una estructura piramidal organizacional, esta claro quien esta a cargo: el liderazgo se concentra en la cima. Pero ¿Qué ocurre si tu comunidad u organización no es tanto una piramide sino más parecida a una red, o un ecosistema, o un jardín.

El liderazgo distribuido desata un increible poder de colaboración, autonomía y organización en red. En este contexto, el liderazgo no esta en la parte superior de la piramide.

Mirando el funcionamiento de las  diferentes formas de liderazgo en una red colaborativa podemos aprender como crear más y más.


### La tierra: El Poder compartido

Antes de charlar sobre como se desarrolla el liderazgo distribuido necesitamos hablar de poder.

El poder no solo viene de la posición que ocupas como autoridad, como un titulo. Se acumula en gente por diversas razones y a veces más allá de control o conciencia (experiencia, fundador, estilo de comunicación, género, edad, habilidades, y como otros factores).

Lo que es importante es como un grupo lidia con el poder. Si la cultura es hóstil a cuestionar al poder, la creación y el liderazgo no puede florecer. A veces los que no tienen poder, no se dan cuenta de lo que ocurre. Para tener una critica efectiva sobre el poder, cada uno necesita voz. En la ausencia de una jerarquía explicita, aquellos con poder tienen la responsabilidad de hacer el trabajo de reconocer que lo tienen, y participar en distribuirlo.

No podemos declamar "No hay jefes", y esperar que todos se auto organicen, se autogetionen. Las jerarquías ocultas van a emerger y el liderazgo distribuido va a desaparecer. Para tener un poder funcional distribuido hay que dejar claro espacio para que una nueva clase de liderazgo se enraice/comience.

### La semilla: El Liderazgo de uno mismo

Si el suelo ha sido limpiado y preparado, el liderazgo distribuido puede crecer. Pero no comienza liderando a otra gente, sino que comienza liderandose a si mismo (como un individuo, como un par, como adherente a la causa)

Vos podes identificar el trabajo necesario para hacer efectivo por vos mismo. Vos provees valor y haces progresar sin instrucciones directas. Vos podes manejar tus tiempos y comprometerte para manejar estandares altos. Además podes marcar buenas fronteras. Tanto los "si" como los "no" son poderosos y considerados por los demás.

Cuando sos parte de un grupo de pares, te adaptas y colaboras. Tenes que tener conciencia sobre tus formas preferidas de trabajo y comunicación. Vos conoces el valor único que traes, y comprendes que otra gente es diferente a vos. Has ganado el sentido de conocerte a vos en un proyecto útil, liberando tus fortalezas.

Las personas expertas en liderarce saben cuando tienen que seguir a otre. Vos sabes cuando en una coordinación sos parte de un equipo que va a alcanzar metas compartidas. Podes seguir instrucciones y comunicarte cuando estas bloqueada y/o necesitas algo. Brindar seguridad a les otres depende de vos. Contribuis a discusiones sobre el proceso continúo de mejoras y compartis compromisos sobre lo que haya que hacer.

Has desarrollado un nivel critico de conciencia de vos misma. ¿Qué necesito para ser una persona productiva? ¿Para tu salud física y emocional? ¿Qué necesitas o queres aprender? ¿Cuales son tus fortaleza y debilidades? ¿Cúal es el impacto que tenes sobre los demás? Practicas un desarrollo profesional autodirigido.

Un buen auto liderazgo es la llave para construir credibilidad y confianza entre los pares, creando posibilidades para vos y contribuir en otros modos de liderazgo a futuro.


### Los Brotes: Liderando a otros

Que las personas se coordinen sin jerarquía es el arte de la ciencia de la facilitación, diseño de proceso, de invitar al otro y el empleo del liderazgo sin jefes.

Diseñas e implementas sistemas y procesos efectivos para distribuir el poder. Hay que ponerlos en funcionamiento, lo que significa que hay que establecerlos y hacerlos accesibles para todas las personas. Se tiene una medida de porque y como la gente y los proyectos se bloquean y como se desbloquean. Hay que ver el paisaje entero del proyecto, y como el proyecto encaja en una comunidad u organización más grande.

Sos buena haciendo crecer y ayudando a equipos. Ganas comprensión de las diferencias y preferencias de las personas y creas procesos que trabajan sus potenciales. Ayudas a grupos a delegar tareas, comunicar y trabajar en conjunto sin coerción.

Sos mentora de otras personas para que aprendan como liderarse a si mismas, y para ayudarlas a convertirse un individuo más pleno. Podes ayudar a traer perspectivas diversas en como el grupo esta trabajando y sintetizar lo que ocurre para mejorar continuamente. Podes facitlitar compromiso con cuestionamiento al poder y usar diferente mecanismos para acrecentar las formas para distribuirlo.

### Floreciendo: Haciendo crecer el liderazgo

Un ambiente realmente colaborativo es uno que tiene un conjunto de lideres diversos. El éxito final es hacer crecer lo más posible el liderazgo en otres que te haga obsoleto.

Mirar más allá de un equipo o un proyecto y ver que el éxito a largo plazo sin jerarquías depende constantemente en nutrir más liderezgos. Intencionlamente hay que crear oportunidades para que las personas puedan avanzar.

Tenes que saber cuando estar expectante y no involucrarte. Incluso cuando sabes que podes hacerlo mejor y más rápido vos mismo, tenes que darle espacio a otres para practicar el liderazgo. También tenes que reconocer que el camino que vos pensas como el más rápido o mejor no es siempre el correcto. Concientemente tratas de invertir tiempo en el trabajo que tiene más impacto o es más especializado y te desafias a delegar todo lo demás. Sos mentora de otras personas implementando procesos que distribuyen el poder y alentas las experimentación.

Tenes que saber cuando intervenir. Las personas se sienten seguras practicando las habilidad de liderazgo con tu ayuda. Sos una red de contención, un apoyo para les otres. Paradojicamente para poder distribuir el liderazgo a veces hay que consolidad poder. Cuando sentis que esto es necesario, tenes que poder facilitar conseguir el consentimiento porque te ganaste la credibilidad para poder hacerlo, y los que te rodean entienden tus intenciones. Podes claramente marcar las diferencias entre ganar poder no intencionalmente y concientemente consolidar poder para distribuirlo mejor.

Podes pensar el sistema como un todo, ¿Donde puede influir mejor la energia puesta en el liderazgo y ser más efectiva? ¿Cual es camino para construir las habilidades para el liderazgo? ¿Qué bloquea a las personas en comprometerse y actual en los roles de liderazgo? ¿Cómo puede evolucionar la cultura para hacerlo más simple? Implementar sistemas de soporte, responsabilidad, transparencia y mejora continua que puede funcionar sin que te involucres directamente.

Criticas el poder de manera sistemática, y pensas criticamente sobre que tipo de personas se convierten en lideres en tu ambiente, como también porque eso ocurre. ¿Hay desequilibrios en la demografía? ¿Qué es lo que necesitan los diferentes tipos de personas con diversas perspectivas y habilidades para ponerse en movimiento, atravesar por este camino y prosperar? Si el camino al liderazgo no es accesible para todos quiere decir que hay semillas que no germinan.


### Polinización: Ecosistema de liderazgos

En este nivel, no tengo respuestas solo preguntas.

Si estas trabajando en todos estos modos de liderazgo, y pensas acerca del impacto más allá de tu propia comunidad u organización, sos un lider del ecosistema.

¿Cuáles son las herramientas, los marcos conceptuales, los protocolos de interacción que necesitamos para habilitar las redes y la colaboración entre ellas? ¿Cómo podemos crear catalizadores para generar nuevas comunidades y organizaciones colaborativas? ¿Cómo muy diferente grupos efectivamente pueden federarse?

¿Cómo podemos rediseñar las estructuras societales de poder que desperdician el potencial de las personas y limitan la libertad? ¿Cómo estructuramos nuestras comunidades, ciudades, empresas y sociedades para que cada uno puede liderar? ¿Qué es que ocurren la marginación y como la combatimos?

¿Cómo podemos dar más acceso a las personas al poder compartido? ¿Cual es la influencia politica y cultural para un cambio significativo? ¿Cómo nos coordinamos la acción colectiva sin achatar la diversidad?

¿Cuales son las innovaciones que hacen esto obsoleto, y podemos pasar a trabajar en el próximo problema? ¿Cuales son los procesos, estructuras e ideas que crean nuevos niveles de agenciamiento colectivo?

¿Qué es lo que esta evolucionando? ¿En qué estamos creciendo?

### Cosecha: haciendo crecer el entendimiento/la empatía

A todos los que conozco que están trabajando en liderazgo sin jefes trabajan en todos estos modos a la vez. Dependiendo del proyecto, del día, del contexto, las personas. Nunca es una progresión lineal. En un ambiente colaborativo, nos movemos alrededor de los demás, dando y recibiendo liderando, siguiendo.

Yo estoy aprendiendo todo el tiempo. A veces quedo estancada abriendo espacios para coordinación en medio del caos. Otras veces fallo al tratar de balancear mi bienestar y mi productividad al estar en pijama y mirar gifs divertidos de gatos. Quizas trato de crear compromiso en los procesos participativos pero nadie participa. A veces creo oportunidades para que alguno pueda liderar, pero no quieren asumir el riesgo y me quedo impaciente. Otras veces me compromento con grandilocuentes preguntas y me siento perdida.

Pero cuando me pregunto a mi misma que he aprendido, vuelvo al jardin.

*Algunas preguntas que quizas te preguntes para continuar desarrollandote como lider en ambiente sin jefes.*

- ¿Cómo opera el poder? Si alguien trata de liderar, ¿hay espacio para el crecimiento? ¿Tienen todos la misma oportunidad de participar?
- ¿Qué poder mantengo? ¿Puedo distribuirlo? ¿Puedo usarlo para invitar a la critica del poder, en los casos donde no los que no tienen poder no son escuchados?
- ¿Estoy cuidandome para poder ser feliz, equilibrada y productiva? ¿Tengo conciencia de mi misma? ¿Qué necesito aprender?
- En una situación cualquiera, ¿tengo que trabajar solo, colaborar, seguir o liderar? ¿Puedo visualizar como hacer la mayor contribución que pueda?
- ¿Estoy haciendo un buen trabajo ayudando a otros? ¿Estoy creando dinamicas de equipo y oportunidades para ayudar a los demás a hacer un mejor trabajo? ¿Quien muestra liderazgo alrrededor mío? ¿Me estoy dando cuenta y aprendiendo de ello, aún cuando difieren de mi estilo?
- ¿Se cuando involucrarme y cuando hacerme al costado? ¿Creo espacios en donde no tengan una necesidad directa mía?
- ¿Estoy pensando en hacer crecer el liderazgo en mi ambiente a largo plazo? ¿Estoy invirtiendo en el desarrollo de liderazgo de otros?
- ¿Esta mi trabajo planteandome pregunta significativas? ¿Estoy colaborando en la conversación colectiva acerca de la evolución de los liderazgos?

Gracias a joshuavial y Richard D. Bartlett.
