---
title: Abril 2020
---

# Cambá CTTE Ezine Abril 2020

## Texto

```
¿A qué le llaman distancia?:
eso me habrán de explicar.
Sólo están lejos las cosas
que no sabemos mirar

Los caminos son caminos
en la tierra y nada más.
Las leguas desaparecen,
si el alma empieza a aletear.

Hondo sentir, rumbo fijo,
corazón y claridad:
si el mundo está dentro de uno,
¿afuera, por qué mirar?

¡Qué cosas tiene la vida
misteriosas por demás!
Uno está donde uno quiere,
muchas veces sin pensar.

Si los caminos son leguas
en la tierra y nada más,
¿a qué le llaman distancia?:
eso me habrán de explicar.
```

Aquí la letra de [Héctor Roberto Chavero](https://es.wikipedia.org/wiki/Atahualpa_Yupanqui) en el tema ["A Que Le Llaman Distancia"](https://www.youtube.com/watch?v=xc39XIOOo9Y)


## Aportes a proyectos de Software libre

### CTTE Ezine

> Se empiezan a hacer públicas las ediciones mensuales de revista electrónica de Cambá, con infraestrutura hecha por medio de colaboraciones de otres socies ( estefi, Gaby, Santi, Facu, entre otres)
- [Repo](https://gitlab.com/camba/ctte-ezine)
- [Web pública](https://camba.gitlab.io/ctte-ezine/)

### Hacktic

> Estos proyectos fueron hechos en el marco de [Hacktic](https://hackdash.org/dashboards/facttic) una Hackaton dónde participaron múltiples cooperativas de [Facttic](https://facttic.org.ar/) durante semana santa de 2020.

- **ApiBueno:** Argentina Covid19 API (API para el seguimiento de la pandemia del coronavirus en Argentina consumible por aplicaciones)
    - [Repo](https://github.com/facttic/apibueno)
    - [API](https://apibueno.herokuapp.com/)
    - [Metabase](http://apibueno-dashboard.herokuapp.com/public/dashboard/89e76543-e0db-49e9-832e-875995379b68)
- **Recumap:** Provee a las organizaciones sociales con una herramienta simple para crear y hacer seguimiento de espacios que necesitan asistencia, facilita la oportunidad para manejar donaciones y mapearlas, y también para publicar recursos durante la emergencia del COVID19
    - [Repo - Backend](https://github.com/facttic/recumap-backend)
    - [Repo - Frontend](https://github.com/facttic/recumap-frontend)
- **FITNess**: Platforma Web desarrolada para manejar colaborativamente las oportunidades de trabajo entre las cooperativas de desarrollo de software.
    - [Repo](https://github.com/facttic/FITness)


## Herramientas de software

- Aplicaciones para llamadas grupales con audio y video libres para probar:
    - [Jiti](https://jitsi.org/)
    - [Spreed](https://github.com/nextcloud/spreed)
    - [Wire](https://github.com/wireapp/wire)
    - [Jami](https://jami.net/)
    - [Tox](https://tox.chat/)
    - [Peercalls](https://peercalls.com/)

- [Marzipano: Visor 360° para la web de Google](https://github.com/google/marzipano)
- [Contrab guru](https://crontab.guru/)
- [Web apis implementadas como funciones de composición de Vuejs](https://tarektouati.github.io/vue-use-web/)
- Impresiona a tus amigues en video llamada con [Avatarify](https://github.com/alievk/avatarify) o [Virutal Background](https://elder.dev/posts/open-source-virtual-background/)

## Informes/Encuestas

- [Evan You - State of the Vuenion 2020](https://www.youtube.com/watch?v=wyx9Mogte4w)
- ¿Qué instancia de Jitsi me conviene usar? [Jitsimeter](https://ladatano.partidopirata.com.ar/jitsimeter/) [Repo](https://0xacab.org/faras/jitsimeter)
- [Como se monto el livestream para libreplanet 2020 en menos de una semana](https://www.fsf.org/blogs/community/how-to-livestream-a-conference-in-just-under-a-week)
- Nueva versión de [Lineage 17.1](https://lineageos.org/Changelog-24/)


### [Encuesta a desarrolladores de IONIC 2019](https://ionicframework.com/survey/2019)

- El tiempo en promedio que tarda en hacerse una app mobile es de entre 3 y 6 meses (Entre 4 y 5 meses el 37%, enter 5 y 6 meses el 28%)
- La frecuencia de nuevas versiones para 2/3 de las empresas es al menos una vez por mes y casi 1/4 con más de 1 o más por semana.
- Uso de diferentes plataformas: Google play 82%, IOS App store 70%, Web browser 60%
- Cross-platform vs native: SDKs/ionic 86%, Cordova 48%, Andoird 20%, Ios 11%
- **PWAs:** 24% Planea hacer este año, 35% Desarrolaron una el año pasado

### [Habilidades demandas para unx Programadorx Remoto (Marzo 2020)](https://cvcompiler.com/blog/in-demand-skills-to-get-a-remote-developer-job-march-2020/)

> Basado en publicaciones de determinadas web basado en 330 posts </br>
> Expectativa con respecto a habilidades técnicas de los que están buscando desarrolladoras fullstack remotos.</br>


Habilidad | %
--- | ---
Javacript | 72%
React | 64%
AWS | 55%
Node.js | 50%
Angular | 45%
CSS | 45%
HTML | 42%
SQL | 38%
Python | 36%
Docker | 34%
Typescript | 31%
Microservices | 28%
Redux | 26%
PostgresSQL | 25%
Kubernetes | 24%
Git/Github | 24%
NoSQL | 22%
PHP | 20%
Mysql | 19%
GC platform | 15%
Java | 12%


*En el enlace están desacoplados frontend y backend.*

## Noticias, Enlaces y Textos

### El deseo de cambiarlo todo

- [Comunicación Feminista con respuesta a la pandemia](https://www.vialibre.org.ar/2020/04/19/webinar-comunicacion-feminista-como-respuesta-a-la-pandemia/)
- [Capitalismo, repdroducción y cuarentena, Silvia Fedirici](http://lobosuelto.com/capitalismo-reproduccion-y-cuarentena-silvia-federici/)
- [Rastros humanos en las superficies del mundo, Judith Butler](https://contactos.tome.press/rastros-humanos-en-las-superficies-del-mundo/?lang=es)

### Más sociales/filosóficos

- [Aprendizajes tecnosociales del virus - Pablo Vannini](http://www.agenciapacourondo.com.ar/ciencia-y-tecnologia/aprendizajes-tecnosociales-del-virus)
- [Anfibia Los falsos profetas de la pandemia - Por Silvio Waisbord  Ilustración Julieta De Marziani](http://revistaanfibia.com/ensayo/los-falsos-profetas-la-pospandemia/)
- [Los estados deben respetar los derecho Humanos al combatir la pandemia](https://www.vialibre.org.ar/2020/04/02/declaracion-conjunta-de-la-sociedad-civil-los-estados-deben-respetar-los-derechos-humanos-al-emplear-tecnologias-de-vigilancia-digital-para-combatir-la-pandemia/)

### Más técnicos

- [Es cardano quien va a matar a Ethereum](https://web.archive.org/web/20200327080448/https://hackernoon.com/is-cardano-the-ethereum-killer-9uc63230)
- [Información sobre el Cubo de rubik que se resuelve solo](https://media.dmm-make.com/item/4462/)
- [Javacript Pro Tips](https://hackernoon.com/16-javascript-protips-2020-edition-n7et32dd)
- [El gobierno de Santa Fe viola la ley de Software Libre - LUGRO](http://www.lugro.org.ar/El-gobierno-de-Santa-Fe-violacion-a-ley-Software-Libre)

### Otras

#### Mi Primer Gnu/Linux

> Escrito por @josx: Hoy abrí el cajón y me reencontre con la primer versión de gnu/linux de la que hice uso. Esto ocurrío en el año 1996 y lo compre en un Musimundo (aunque no lo crean). Son seis cds y traen [Red Hat](https://www.redhat.com/en) 4.0, [Slackware](http://www.slackware.com/) 3.1 y [Debian](https://www.debian.org/) 1.2 entre otras cosas, con Kernels 2.0.27 y otros 2.1.14. Acá [versión online del contenido](https://archive.org/details/LDR1296), y hay muchos otros gracias [archive.org](https://archive.org/).

![Gnu/Linux](@img/3303642890_1b73e3ef97_o.jpg)

#### Recomendación Pelis

- [Existenz](https://www.imdb.com/title/tt0120907/)
    Vi está pelicula de la mejor Ciencia ficción hace 20 años, donde entran en juego la realidad virtual, el diseño de videojuegos, el cuestionamiento a la realidad material y a nuestra percepción de las cosas.

- [Lucio](https://www.imdb.com/title/tt1082851/) [1](https://web.archive.org/web/20080915051403/http://www.lucio.com.es/)
    Acabo de terminar de ver un documental sobre parte de la agitada y prolifica vida de don Lucio Urtubia. Lucio,  albañil y militante anarquista, relata su vida de militancia y trabajo. El documental es simple, sincero y directo. Realmente conocer personajes como este hacen que la vida vaya teniendo cada vez más sentido. Algunos le podrán decir estafador, ladrón, criminal, lo que fuere yo veo una persona que se niega a negar la humanidad, y que trae aires de libertad y amor. Les aviso hay muchas frases para tomar nota y pensarlas un rato largo. Los invito sin dudas a verla, es realmente una grata noticia par mi haberla encontrado en mi camino.

#### Audio

[Radialistas (Apasionadas y Apasionados)](https://radialistas.net) una ONG sin fines de lucro con sede en Quito, Ecuador.
Que su misión es contribuir a la democratización de las comunicaciones, especialmente de la radio, desde las perspectivas de género, ciudadanía y defensa de la Madre Tierra. Y además trabaja solidaria y complementariamente con las redes de comunicación ya existentes.
Genera este tipo de contenido para fomentar según sus propias palabras el *Acceso universal y gratuito al software. La experiencia de Linux anuncia la victoria contra el imperio Microsoft.*

Escuhemos [Un pinguino contra bill gates](./assets/10030002.mp3)

