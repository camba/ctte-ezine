---
title: Julio 2021
---

# Cambá CTTE Ezine Julio 2021

**Los días**
![losdias](@img/losdias.png)

```
Hoy la pelea que doy
Es quererme más
Hoy el grito que doy
Es silencio
Hoy te pido perdón
Si te lastimé el corazón

Hoy no quiero lo que me hace mal
Lo oscuro del juego
Hoy que es tiempo de sanar
Las heridas del tiempo
Hoy que pronto será ayer
Regálate el momento

Hoy pude ver quien soy
Conocerme más
Hoy que el veneno
Encontró su remedio
Hoy me doy el perdón
Si me lastimé el corazón

Hoy vale más despertar
Que soñar en este juego
Hoy que es tiempo de sanar
Las heridas del tiempo
Hoy que es tiempo de ser luz

Esa es mi revolución
Llenar de amor mi sangre
Y si reviento
Que se esparza en el viento
El amor que llevo adentro
Esa es mi revolución
```

Aquí la letra de [Cuatro pesos de propina](https://es.wikipedia.org/wiki/Cuatro_Pesos_de_Propina) en el tema ["Mi revolución"](https://www.youtube.com/watch?v=e2o4iCvQyzI)


## Aportes a proyectos de Software libre

- [DjRumble](https://github.com/dj-rumble) - Nuevo Proyecto libre comenzado por Santiago Botta y Juan Manuel Baez
- [Agregamos IA² a Coophub](https://github.com/fiqus/coophub/commit/57a4c0608fa4133ea709c0eeccdd768bd5dc296c)
- [Cambio menor en Electron preferences](https://github.com/tkambler/electron-preferences/pull/114)

## Herramientas de software

- [Udemy downloader](https://github.com/FaisalUmair/udemy-downloader-gui) - Bajate los cursos
- [v86](https://github.com/copy/v86/) -  [Web](https://copy.sh/v86/) - Virtualizción en Navegador con WASM
- [Docker-OSX](https://github.com/sickcodes/Docker-OSX) - Corre Mac en Docker
- [Mininim](https://github.com/oitofelix/mininim) - [Web](http://oitofelix.github.io/mininim/) - Engine del juego Príncipe de Persia
- [Whatsapp IP leak](https://github.com/bhdresh/Whatsapp-IP-leak) - Conoce la IP y geoposición de personas de WP.

## Informes/Encuestas/Lanzamientos/Capacitaciones

- Cambá lanza junto a Viarava aplicación de radios - [Entrevista en radio](https://viarava.org.ar/junto-a-camba-lanzamos-la-app-de-una-radio-muchas-voces-y-cdm-noticias/) - [Nota](https://www.notaalpie.com.ar/2021/07/27/radios-comunitarias-lanzaron-su-propia-app/)
- Imperdible [Lanzamiento Huayra 5.0](https://www.youtube.com/watch?v=4RdABSn3Gf4) - [Comentario de Monk (FACTTIC)](https://youtu.be/4RdABSn3Gf4?t=5451)
- [Grupo Clementina cumple 1 año](https://irisfernandez.com.ar/betaweblog/index.php/2021/07/04/el-grupo-clementina-cumple-1-ano/)
- [Redes de internet comunitaria para organizaciones rurales](https://www.youtube.com/watch?v=xMKLTA1exKE) - [Cumbre Argentina de Redes Comunitarias](https://carc.libre.org.ar/)

## Noticias, Enlaces y Textos

### Sociales/filosóficos

- [Feudalismo tecnológico reemplaza el mercado capitalista](https://www.project-syndicate.org/commentary/techno-feudalism-replacing-market-capitalism-by-yanis-varoufakis-2021-06/spanish)
- [Cooperativas en la Argentina 2020, Schujman, Mario Saúl](https://rephip.unr.edu.ar/handle/2133/20912)
- [Mesa Software libre en el encuentro nacional de RNMA](https://archive.org/details/mesa-software-libre-encuentro-nacional-rnma)
- [Cómo aplica la GPL a los modelos de redes neuronales](https://opensource.stackexchange.com/questions/6961/how-does-gpl-apply-to-neural-network-models)
- [Agroecología](http://lavaca.org/libroagroecologia/) - Libro de lavaca editora recorre diversas experiencias agroecológicas del país, a través de viajes, reportajes e imágenes que muestran cómo ya está en marcha otra forma de producir y de vivir.

### Tecnología

- [Curso de NLP Hugging Face](https://huggingface.co/course/chapter1)
- [Enamora a la personas que hacen revisión de código](https://mtlynch.io/code-review-love/)
- [Haciendo builds con ASTRO](https://css-tricks.com/a-look-at-building-with-astro/)
- [Qué forma tiene la autonomía digital](https://video.nogafam.es/w/3a7c5a23-a4bd-482a-9e81-7e86a464a287)

### Otras

- [Cybercirujas Club](https://cybercirujas.sutty.nl/) - Un lugar donde encontrar información para facilitar los intercambios cybercirujas - [Entrevista radial](https://radiocut.fm/audiocut/ecologismo-reciclado-y-software-libre-ciber-cirujas-en-segurola-y-habana/)
- [Economia Crítica por Tomás Astelarra sobre Humano](https://www.youtube.com/watch?v=mitfL6SItFg) y [Humano, comercio cooperativo en Traslasierra](https://www.youtube.com/watch?v=5TWnxTMpDIw)
