---
title: Octubre 2022
---

# Cambá CTTE Ezine Octubre 2022

<div style="text- align: center;">

![Octubre](@img/portada-ezine-octubre.jpg)

</div>

```
Mi táctica es mirarte
Aprender como sos
Quererte como sos
Mi táctica es hablarte
Y escucharte
Construir con palabras
Un puente indestructible
Mi táctica es
Quedarme en tu recuerdo
No sé cómo ni sé
Con qué pretexto
Pero quedarme en vos
Mi táctica es Ser franco
Y saber que sos franca
Y que no nos vendamos
Simulacros
Para que entre los dos
No haya telón
Ni abismos
Mi estrategia es
En cambio
Más profunda y más
Simple
Mi estrategia es
Que un día cualquiera
No sé cómo ni sé
Con qué pretexto
Por fin me necesites
```

**Poema del poeta y novelista Uruguayo** de [Mario Benedetti](https://es.wikipedia.org/wiki/Mario_Benedetti), [versión recitada por el mismo](https://www.youtube.com/watch?v=Pku15u39UxA)


## Herramientas de software

- [Vue-compiler](https://github.com/HerringtonDarkholme/vue-compiler) - Reimplementación del compilador de templates de vue en Rust
- [ml5js](https://ml5js.org/) - Un machine learnging amigable para la web
- [Upscayla](https://github.com/upscayl/upscayl) - Mejora resolution de imagenes con IA
- [Yuzu](https://yuzu-emu.org/) - Emulador experimental de Nintendo Switch


## Informes/Encuestas/Lanzamientos/Capacitaciones

- Richard Stallman lanza primera versión del **GNU C Language Intro and Reference Manual]** - [Email](https://lists.gnu.org/archive/html/info-gnu/2022-09/msg00005.html), [Repo](https://git.savannah.nongnu.org/cgit/c-intro-and-ref.git/), [Manual](https://www.cyberciti.biz/files/GNU-C-Language-Manual/GNU%20C%20Language%20Manual.html)
- Andreas Kling lanza [Ladybrid](https://github.com/SerenityOS/ladybird) un navegador web multiplataforma - [Info](https://awesomekling.github.io/Ladybird-a-new-cross-platform-browser-project/)
- [**Xscreensaver** cumple 30 años desed su primer lanzamiento](https://www.jwz.org/blog/2022/08/xscreensaver-was-released-30-years-ago/)
- Lanzamiento de nueva versión de [Arduino IDE 2.0](https://github.com/arduino/arduino-ide) - [Nuevas funcionalidades](https://blog.arduino.cc/2022/09/14/its-here-please-welcome-arduino-ide-2-0/)
- [Ranking de lenguaje de programación 2022](https://spectrum.ieee.org/top-programming-languages-2022)

## Noticias, Enlaces y Textos

### Sociales/filosóficas

- [Las criptos y la izquierda con Cory Doctorrow](https://theblockchainsocialist.com/crypto-and-the-left-with-cory-doctorow/) - [Bio](https://en.wikipedia.org/wiki/Cory_Doctorow)
- [Voces de la Tierra con Claudio Naranjo](https://www.youtube.com/watch?v=LN1yJnGgJCQ) - [Bio](https://es.wikipedia.org/wiki/Claudio_Naranjo)
- [Susy Shock en Es la política](https://www.youtube.com/watch?v=aZX9JOeeQ_c) - [Bio](https://es.wikipedia.org/wiki/Susy_Shock)
- [Enrique Dussel explica la teoría: "El Giro Descolononizador"](https://www.youtube.com/watch?v=mI9F73wlMQE) - [Bio](https://es.wikipedia.org/wiki/Enrique_Dussel)


### Tecnología

- [¿Cómo decodificar un código QR a mano?](https://www.youtube.com/watch?v=KA8hDldvfv0)
- [¿Cómo era programar hace 40 años?i](https://www.youtube.com/watch?v=7r83N3c2kPw)
- [Floppotron 3.0](http://silent.org.pl/home/2022/06/13/the-floppotron-3-0/)
- [Historia de python, contada por su creador Guido van Rossum](https://www.youtube.com/watch?v=J0Aq44Pze-w)
- Extractos de entrevista a John Carmack: [Mejor setup para programar](https://www.youtube.com/watch?v=tzr7hRXcwkw), [¿Gana javascript?](https://www.youtube.com/watch?v=rczu8kc8JZA), [Mejor lenguaje de programación](https://www.youtube.com/watch?v=RfWGJS7rckk)

### Cooperativas/Economía Popular

- [La historia que no nos contaron: la programación como testigo de la exclusión](https://nayracoop.github.io/historia-mujeres-computacion/)
- [Entrevista a Neto Licursi sobre El día del software libre](https://archive.org/details/neto-licursi-facctic)
- [Aportes para un cooperativismo de plataformas feminista](https://www.youtube.com/watch?v=PVqXwzdYOW4)
- [Radiografía de las cooperativas en Argentina y algunos datos desconocidos](https://www.ambito.com/economia/cooperativas/radiografia-las-argentina-y-algunos-datos-desconocidos-n5476438)
