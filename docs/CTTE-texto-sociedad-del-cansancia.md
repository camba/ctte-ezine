# Resumen - [La sociedad del cansancio](https://www.youtube.com/watch?v=zPNbDDxysk0)

> La sociedad del cansancio es un libro escrito por el filosofo coreano [Byung Chul Han](https://es.wikipedia.org/wiki/Byung-Chul_Han). Aquí enlace a video con un resumen [1](https://www.youtube.com/watch?v=zPNbDDxysk0)

## Siglo XX

- Paradigma inmunológico
- Yo y el otre/extraño (enemigo externo)
- Negar la negatividad del otro reconociendolo como tal
- Sociedad disciplinaria
- Vigilancia y castigo
- Dialectica del no-poder/deber

## Siglo XXI

- Paradigma Neurologico
- Enimigo/otre esta dentro de uno (enemigo interno)
- Yo positivo todo lo abarca, todo lo cubre
- Enfermedades neuronales como reacción a la postividad (psiquicas y psicologica)
- Sujeto de Rendimiento se somete a la culpa
- Sociedad del rendimiento


## Nuevas formas de violencia

- Control por el rendimiento, la obligación de rendir (culpa si no puedo)
- La cultura del emprendedor (culto al hacer y poder hacer)
- Si se puede (meta deseada en nuestro tiempo, reemplaza a las leyes, mandatos, obligaciones)
- No hay limites y generador de depresivos y fracasados
- Pasaje de la disciplina a la auto disciplina
- Soy mi propio amo, autoexplotación, libertad paradójica (ese hacer es mi carcel social)
- Exceso de estimulos
- Escasa tolerancia al vacío, huye del espacio del aburrimiento que lleva a la actitud contemplativa
- El multitarea como regresión a instancias de superviciencia animal

## El Imperativo a rendir

- Conduce al rendimiento sin rendimiento
- Produce agotamiento excesivo, que requiere se estimulos para seguir o caer en la angustia
- Estamos construyendo una  sociedad del dopaje
