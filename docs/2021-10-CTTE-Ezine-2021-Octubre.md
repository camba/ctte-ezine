---
title: Octubre 2021
---

# Cambá CTTE Ezine Octubre 2021

**Primavera**
![primavera](@img/primavera.png)

```
Cuando las nubes pasen mi amor
Cuando las nubes pasen mi amor
Despues de la tormenta
Las sombras de alargan
La niebla se va
Rumor de vida nueva
Se escuchan los pasos
Se escucha el compás
Y yo me desanudo la garganta
Me doy media vuelta
Retomo mi andar

Cuando las nubes pasen mi amor
Cuando las nubes pasen mi amor
Despues de la tormenta
Los torsos desnudos se llenan de sol
Los capullos, las mano se abren,
se estiran se llenan de sol
Y yo me desanudo la garganta
Te miro a los ojos
Me lleno de vos
```

Aquí la letra de **Pablo Echaniz** el tema [Cuando las nubes pasen](https://www.youtube.com/watch?v=8-s4WdNVbWk)


## Aportes a proyectos de Software libre

- [Django admin export actions](https://github.com/otto-torino/django-admin-export-action/pull/1#event-5336729515): Aporte para traducción al español


## Herramientas de software

- [Iptvnator](https://github.com/4gray/iptvnator) - Mira los canales de TV vía internet, [Canales de Argentina](https://github.com/mortal251/tvargentina/blob/main/ssiptvarg)
- [Pyxel](https://github.com/kitao/pyxel) - Motor para hacer juegos retro en Python
- [Hacking Ecology](https://hackingecology.com/es/) - Herramientas libres accesibles, flexibles y fiables para el monitoreo del agua, con base de datos seguras, inalterables, distribuidas y accesibles.
- [CDPedia](http://cdpedia.python.org.ar/) - Accedé a la información de la Wikipedia aunque no tengas una conexión a Internet.


## Informes/Encuestas/Lanzamientos/Capacitaciones

- [El Colectivo En Las Flores contando sobre su trabajo con @arduino  con la documentación del Laboratorio de Tecnologías Creativas](https://www.fcedu.uner.edu.ar/?p=54204) - [Tweet](https://twitter.com/niamfrifruli/status/1439592610964856843)
- [Breve guía sobre distros](https://clementina.org.ar/Publicaciones/LibroDistros.pdf) hecho por el grupo [Clementina](https://clementina.org.ar/)
- [Conclusiones de informe de trabajo con software libre 2021](https://www.linuxadictos.com/la-fundacion-linux-dice-que-las-empresas-ya-han-comenzado-a-demandar-las-habilidades-en-la-nube.html)

## Noticias, Enlaces y Textos

### Sociales/filosóficos

- [Estoicismo: Un ANTÍDOTO contra la INSATISFACCIÓN](https://www.youtube.com/watch?v=Sqwr-zUFL9E)
- [La gente feliz no necesita consumir](https://www.elclubdeloslibrosperdidos.org/2018/04/la-gente-feliz-no-necesita-consumir-la.html)
- [Bienvenido a la maquina moral](https://www.moralmachine.net/hl/es)
- [Mesa redonda: Hablemos de regulación de la IA- Fundación Vía Libre](https://www.youtube.com/watch?v=2DzFpm0sXX4)

### Tecnología

- [Turbo Rascal Syntax Error: Entorno de programación completo para hacer juegos para computadoras de 8, 16bits](https://lemonspawn.com/turbo-rascal-syntax-error-expected-but-begin/) - ¿Como empezar? [1](
https://retrogamecoders.com/introduction-to-trse-programming/) [2](https://retrogamecoders.com/trse-programming-tutorial-part-2/)
- Bytebeat: [Nota en diario](https://www.pagina12.com.ar/365020-sintetizadores-para-la-cartera-el-bolsillo-la-mochila-o-la-r) - [Guía para iniciarse](https://github.com/TuesdayNightMachines/Bytebeats) - [Jugar online](https://greggman.com/downloads/examples/html5bytebeat/html5bytebeat.html)
- [Desafio de algoritmos en 100 días](https://github.com/coells/100days)
- Copilot: [Preocupación de Richard Stallman](https://news.slashdot.org/story/21/09/18/0432224/richard-stallman-shares-his-concerns-about-githubs-copilot----and-about-github)
- [Proyecto gemini](https://gemini.circumlunar.space/docs/faq.html) - [clientes y más info](http://techrights.org/2021/04/28/joining-gemini-space/)

### Otras
- [El Éxito ¿es Suerte o Trabajo Duro?](https://www.youtube.com/watch?v=IrRiVoH3sGQ)
- [Coprinf Coop.ar](https://www.argentina.gob.ar/noticias/el-coopar-nos-visibiliza-como-cooperativas-construye-y-refuerza-la-identidad)
- [El club del cybercirujeo](https://www.instagram.com/p/CUU__zxAwZw/): Sabado 2/10 en @teatromandril Humberto Primo 2758 de 19 a 01! Entrada a la gorra.
