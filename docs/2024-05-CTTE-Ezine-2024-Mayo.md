---
title: Mayo 2024
---

# Cambá CTTE Ezine Mayo 2024

<div style="text-align: center;">

![Libros](@img/mayo2024.jpg)

</div>

```
Detrás del sol donde se esconden las sombras
Están las piedras del Amor y lo temido
Si sabes volar y despertar por la noche
Las cuerdas que te van a atar, no siempre aprietan

No queda nada mas que volver a hacerlo así
Que volver a repetir

Persiguiéndote hasta los sueños profundos
Las caras que te hacen temblar siempre son las mismas (no)
Y aunque creas q sabes como muerden los demás
Son marcas que vas a llevar, toda la vida

No queda nada mas que volver a ser así, que volver a repetir
No queda nada mas, que volver a hacerlo así, que volver a repetir

Y aunque te ahogues en los sueños
Peor es vivir sin intentar sin despertar de esta película

No queda nada mas que volver a ser así, que volver a repetir
No queda nada mas, que volver a hacerlo así, que volver a repetir
```

Letra del tema [Las piedras](https://www.youtube.com/watch?v=ZauKVF0Ep2w) de [El Espacio es el lugar](https://www.instagram.com/elespacioesellugar/).

## Cooperativas/Economía Popular
- [Buscador de estado de cooperativas y mutuales a abril 2024](https://0xacab.org/sutty/sitios/cooperativas-suspendidas-sutty-nl) - [Código fuente](https://cooperativas-suspendidas.sutty.nl/)
- [Lanzamiento de Colmena](https://akademie.dw.com/es/lanzamiento-de-colmena-ha-revolucionado-la-producci%C3%B3n-y-distribuci%C3%B3n-de-nuestros-contenidos/a-68785062) - [Blog](https://blog.colmena.media/) y [Documentación](https://docs.colmena.media/)
    - Alguna de las Organizaciones del consorcio: [tandacn](https://twitter.com/tandacn), [Muywaso](https://muywaso.com/)
- [40 videos de la historia de Crear y Cambá](https://vimeo.com/user6253062)


## Herramientas de software

- [AFFiNE](https://github.com/toeverything/AFFiNE) - Nueva generación de base de conocimiento para planificar, ordenar y crear todo en uno (alternativa a miro y notion)
- [CyberChef](https://github.com/gchq/CyberChef/) - Una web app para encriptar, codificar, comprimir y hacer analisis de dato
- [Piku](https://github.com/piku/piku) - El PaaS más pequeño que hayas visto
- [Tomb](https://dyne.org/software/tomb/) - Encripción de carpetas en GNU/Linux
- [Jsr.io](https://jsr.io/) - Registry de paquete de software para javascript y typescript

## Informes/Encuestas/Lanzamientos/Capacitaciones

- [zine Actualidad CyberCiruja 01](https://nxt.mybsd.cloud/index.php/s/7A7cm9TAyxJYgzn)
- [Lanzamiento de Ubuntu 24.04 Noble Numbat](https://fridge.ubuntu.com/2024/04/25/ubuntu-24-04-lts-noble-numbat-released/)
- [Canonical cumple 20 años](https://www.muylinux.com/2024/03/06/canonical-cumple-20/)
- [Nueva versión de la distro Dyne 4.0](https://dyne.org/software/dynebolic/)
- [Metasploit: Mejoras de la versión 6.4](https://osint.com.ar/metasploit-framework-6-4-avances-significativos-en-autenticacion-kerberos-y-mas/)


## Noticias, Enlaces y Textos

### Sociales/filosóficas
- Eric Sadin en Argentina
    - [Untref](https://www.youtube.com/watch?v=7IPS1sWfyVs)
    - [Malba](https://www.youtube.com/watch?v=0MYePfKdQ0E)
    - [Entrevista  en La ley de la selva](https://www.youtube.com/watch?v=TNqjTj0-wHk)
    - Notas en diarios_ [infobae](https://www.infobae.com/cultura/2024/04/10/eric-sadin-no-soy-un-profeta-pero-veo-las-cosas-antes-y-digo-lo-que-creo-nadie-dice/), [Pagina12](https://www.pagina12.com.ar/727963-eric-sadin-los-milei-van-a-florecer-en-todo-el-mundo)
- [¿De VERDAD quieren TERMINAR con el CAPITALISMO? | Deseo POSTCAPITALISTA por Mark Fisher](https://www.youtube.com/watch?v=XfeIE-01vdU)
- [Matrix fue un documental](https://www.youtube.com/watch?v=y04ErVTSRNI)

### Tecnología
- [La comlpejidad es el enemigo](https://www.examplelab.com.ar/links-files/traducciones/complexity/)
- [Algoritmo de cifrado DES](https://www.microsiervos.com/archivo/seguridad/algoritmo-cifrado-des.html)
- [¿Crees que sabes de git?](https://www.youtube.com/watch?v=aolI_Rz0ZqY)
- [Vue.js Vapor Mode](https://www.vuemastery.com/blog/the-future-of-vue-vapor-mode/)


### LLMs

- [Command R: Tuneado para RAG y Agentes](https://www.youtube.com/watch?v=tbYKa4PgDVA)
- [Gentoo Prohibe código con IA en sus repos](https://news.itsfoss.com/gentoo-linux-bans-ai-code/)
- [Nuevo Jailbrak de IAs Universal](https://www.youtube.com/watch?v=9IM5d-egZ7M)
- [Lanzamiento de Llama 3](https://www.youtube.com/watch?v=0AaNT7XO41I)
