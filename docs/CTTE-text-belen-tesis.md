# Habita - suelo y cielo

> Textos extraidos y usados como introducción a cada parte de la tesis una poeta del conurbano llamada [María Belén Sánchez](https://mariabelensanchez.wordpress.com/)

> Leer [Tesis completa](./assets/finalparaentregar.pdf)

## En el principio la semilla

Nace como semilla, idea, rompe la corteza espacial. Irrumpe, plácida caída.
Crece, se reproduce. Forma tejido y red de especies nacientes. Minúscula
partícula que en acto fecundo espera vida. Connota un tiempo, revela espacio.
Semilla crece, sigilo y paciencia, movimiento constante, alumbra el momento,
mundo origina, color vehículo, memoria y energía.

## Siembra

Puede semilla hacerse siembra. Arrojarse al suelo y esperar crecer. Puede
sembrasrse al infinito, riego y cautela, amor o parecer. Agua, sol, paz-ciencia,
fortaleza de viento y marea. Puede crecer hacia arriba y abajo, creciente sobre
la tierra y todo atrevesando. Guarida y silencio, con tiempo cambiante, arriba
al momento la siembra amante.

## Tránsito

Hacer pasear las raíces, como líneas danzantes. Como escritura en el tiempo,
como historia haciendo historia. Llevar sobre uno un tipo de existencia capaz
de ser moldeable. Estar permeable, dejar entrar, nuevas formas de ser uno mismo.
Siendo muchos. Andar con paso, casi cansino, sobre el nivel del mar y sobre una
superficie más alta. Más alta, casi llegando al sol. Y entre el cielo y el suelo,
ahí moviéndonos.

## Suelo fértil

Terroso, poderosa arcilla que hace nacer. Hacer suelo y pisar, firme arraigo que
fue semilla, sembrada en tránsito, hecha crecer.

## Cosecha

Cuando tiempo es de cosecha, se levanta lo nacido. Nacido al abrigo del sol y de
agua. Cuando tiempo es de cosecha, el ritmo se acelera, se acelera la certeza. Se
recoge fruto.

## Florecer

Luminosa, fresca. Vida que nace, florece y muere. fertilidad que se emancipa,
vuela buscando donde hacer-se. Reproducir el mundo, recrear, ser y estar. Habitar
desde los órganos, desde el género y desde la identidad.

## Brote

Minúscula crecida. Dato inicial de una tarea por venir. Todo empieza desde ahí,
pequeño brote. Lo gigante, pequeñito. Verde o de color, nace como habiéndose
gestado en el silencio. Chiquito, del mundo brota, como parte de todo. Compacto
primero, expansivo después.


## Arraigo

De dónde viene, hacia dónde va. Que las extensas raíces me preceden y me conectan.
Dibujan recorridos de otros y otras que comparten el mundo. Que piensan la duda,
motivan el sueño, ejercen presión.

## Rizoma

Se desparrama, se ramifica. Vincula, rompe y rearma. Gesta nueva semilla, ordena
el mundo. Construye red y trama.

