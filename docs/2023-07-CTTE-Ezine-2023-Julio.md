---
title: Julio 2023
---

# Cambá CTTE Ezine Julio 2023

<div style="text- align: center;">

![Pez](@img/pez.jpg)

</div>

```
Ando un poco para adentro
Esperando que algo pase
Ando desilusionado
Y sin ganas de sentir
Ando buscando una canción
Que acorte las distancias
Entre este mundo y yo

Ando un poco paranoico
Y con miedo a que me toquen
Ando desilusionado
Y sin ganas de salir
Ando sintiendo que el dolor
Ya es parte de esta vida
Y lo aceptamos por que si

Es preciso que me saque
Toda la tensión de el alma
Es preciso que me afloje
Y te mire a los ojos
Es preciso que me arranque
Toda esta seriedad que ves
Ya no me deja ver

Salir afuera, que afuera sale el sol
Y no te olvides que no sos un robot
Ya no dejes que controlen más tu vida
El miedo esta en la mente.

Y te vas poniendo viejo
Y te vas endureciendo
El silencio te acompaña
Y tus sueños ya no son
Paste de un póster que quedo
Mal colgado en la pared del cuarto
Que te vio soñar

Y me niego a creerme
Que tengo que tener miedo
A la gente diferente
Y es que pienso que tu
Idea de seguridad
Es la cosa más absurda
Y egoísta que escuche
```

Letra de la canción [El miedo](https://www.youtube.com/watch?v=joj6vgXv6aE) de **Sancamaleon** del disco [Cancionero para niños sin fe](https://es.wikipedia.org/wiki/Cancionero_para_ni%C3%B1os_sin_fe)


## Participaciones Cambá

- En página 18 de la revista  "Innovación para pymes y emprendimiento" nos hicieron una nota titulada [Elegimos y militamos el softwar libre](https://issuu.com/fundacionempretec/docs/innovacion_n10)
- Nuestros asociados Leonardo Vaquel y Carlos Cuoco participaron en RightsCON 2023 en conjunto con ADC en donde debatieron sobre publicidad electoral en redes sociales. [Nota](https://adc.org.ar/2023/06/09/rightscon-adc-debatio-sobre-publicidad-electoral-en-redes-sociales-y-tecnologias-de-reconocimiento-facial/)

## Herramientas de software

- [AudioCraft](https://github.com/facebookresearch/audiocraft) es una biblioteca de facebook para el procesamiento de audio y generación de deep learning.
- [Upscaler](https://github.com/upscayl/upscayl) software libre para escalar images con AI
- [Presupuestos participativos](https://github.com/cobudget/cobudget)
- [Colada](https://colada.dev/) es una debugger time-travel para la biblioteca de estados Pinia para Vue.
- [GW-BASIC](https://codeberg.org/tkchia/GW-BASIC) código original de Microsoft de este interprete muy importante en la historia de la programación.

## Informes/Encuestas/Lanzamientos/Capacitaciones
- 1.5TB de datos que permite descargar el grupo MEDUSA sobre la Comisión Nacional de Valores de Argentinas. [Medusa Blog](http://medusaxko7jxtrojdkxo66j7ck4q5tgktf7uqsqyfry4ebnxlcbkccyd.onion/) - [Post con Datos](http://medusaxko7jxtrojdkxo66j7ck4q5tgktf7uqsqyfry4ebnxlcbkccyd.onion/detail?id=4ca881bcb245afa267b9595356112d36)
- [Encuesta de desarrolladores 2023 de la comunidad de Stackoverflow](https://survey.stackoverflow.co/2023/) - [Tendencias](https://stackoverflow.blog/2023/06/13/developer-survey-results-are-in/) 
- Lanzamiento de [Huayra 6](https://huayra.educar.gob.ar/) - [Video](https://www.youtube.com/watch?v=_x1puS2W8oo)
- Hacker, argentino y peronista devuelve 200M de dólares obtenidos por el vulnerabilidades del protocolo Euler. [1](https://es.beincrypto.com/hacker-euler-reaparece-mensajes-argentina/) y [2](https://www.dlnews.com/articles/defi/euler-hacker-sends-cryptic-messages-after-returning-crypto/)
- Lanzamiento de [Debian 12 "bookworm"](https://www.debian.org/News/2023/20230610) Luego de 1 año, 9 meses y 28 días.
- Resultado del desafio ["Gamedev Jam 2023"](https://github.blog/2023-06-21-gamedev-js-2023/)


## Noticias, Enlaces y Textos

### Sociales/filosóficas
- [¿Esta la programación muerta? | Stephen Wolfram y Lex Fridman](https://www.youtube.com/watch?v=uD353DeOM-4)
- [El cerebro sin organos de la IA](https://lobosuelto.com/el-cumplimiento-el-cerebro-sin-organos-de-la-inteligencia-artificial-franco-bifo-berardi/)
- [Noam Chomsky habla sobre Chatgpt](https://www.sinpermiso.info/textos/noam-chomsky-habla-sobre-chatgpt-para-que-sirve-y-por-que-no-es-capaz-de-replicar-el-pensamiento)

### LLMs
- [¿Es tu texto algo generado por la AI de openAI?](https://platform.openai.com/ai-text-classifier/)
- [Las inteligencias artificiales y sus regulaciones](https://arielvercelli.org/2023/05/17/video-presentacion-del-articulo-las-inteligencias-artificiales-y-sus-regulaciones-revista-ecae-en-la-facultad-de-ciencias-exactas-y-naturales-de-la-uba/)
- [Stability AI lanza StableVicuna, el primer chatbot RLHF](https://stability.ai/blog/stablevicuna-open-source-rlhf-chatbot)
- [Tabla de LLMs Abiertos](https://huggingface.co/spaces/HuggingFaceH4/open_llm_leaderboard)
- "Orca" 🐳 Open-Source Model Surprised Everyone" - [Paper](https://arxiv.org/pdf/2306.02707.pdf) [Video 1](https://www.youtube.com/watch?v=Dt_UNg7Mchg) [Video 2](https://www.youtube.com/watch?v=KoI6G7oWYvM)

### Tecnología
- Tutoriales de como clonar voces [1](https://www.youtube.com/watch?v=6QAGk_rHipE) [2](https://rioharper.medium.com/cloning-your-voice-cb321908b060#22d2) [3](https://colab.research.google.com/drive/1-W0T1Scp_940kf4E63CY09MBOeR19kUO)
