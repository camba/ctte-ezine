# Preguntas sobre tecnología

> Estas preguntas fueron vistas en [p2pFoundation](https://blog.p2pfoundation.net/78-questions-to-ask-about-any-technology/2019/02/26). “78 Reasonable Questions to Ask about Any Technology” del libro “Turning Away from Technology” de Stephanie Mills y originalmente derivado de las escritas por [Jaques Ellul](https://es.wikipedia.org/wiki/Jacques_Ellul).

## Ecológicas

- ¿Cual es el efecto sobre la salud del planeta y las personas?
- ¿Preserva o destruye la biodiversidad?
- ¿Preserva o reduce la integridad del ecosistema?
- ¿Cuales son los efectos sobre la tierra?
- ¿Cuales son los efectos en la vida salvaje?
- ¿Cuantos y que desechos generan?
- ¿Incorpora principio de diseño ecológicos?
- ¿Rompe el vinculo entre personas y naturaleza?
- ¿Preserva o reduce la biodiversidad cultural?
- ¿Cuales son la totalidad de sus efectos, es ecológico?

## Sociales

- ¿Sirve a la comunidad?
- ¿Potencia a los miembros de la comunidad?
- ¿Como afecta a la percepción de nuestras necesidades?
- ¿Es consistente con la creación de una economía humana comunitaria?
- ¿Cómo afecta las relaciones entre personas?
- ¿Socava la convivencia?
- ¿Socava las formas tradicionales de comunidad?
- ¿Cómo afecta nuestra forma de sentir o experimentar el mundo?
- ¿Adopta diversas formas de conocimiento?
- ¿Contribuye, recrea a la renovación de formas de conocimiento tradicional?
- ¿Sirve para comoditizar el conocimiento o las relaciones entre personas?
- ¿En que medidad redefine la realidad?
- ¿Hace visible el sentido del tiempo y la historia?
- ¿Cual es su potencial para convertirse en adictivo?

## Morales

- ¿Qué valores adopta su uso?
- ¿Qué se gana con su uso?
- ¿Cuales son los efectos más allá de los individuales?
- ¿Qué se pierde al usarlo?
- ¿Cuales son los efectos sobre las persona en la sociedad?

## Estéticas

- ¿Es fea?
- ¿Causa fealdad
- ¿Hace ruido?
- ¿Qué ritmo marca?
- ¿Cómo afecta la calidad de vida (como distintivo de la forma estandar de vivir)?

## Prácticas

- ¿Qué es lo que hace?
- ¿A quiénes beneficia?
- ¿Cúal es el propósito?
- ¿Dónde fue producido?
- ¿Dónde se usa?
- ¿A Dónde hay que recurrir cuando esta rota u obsoleta?
- ¿Cuan caro es?
- ¿Puede ser reparada? ¿por una persona cualquiera?
- ¿Cual es el costo total?

## Éticas

- ¿Cuan complicado es?
- ¿Qué nos permite ignorar?
- ¿Hasta donde podemos distanciarnos de sus efectos?
- ¿Podemos asumir responsabilidad social, personal o comunal por sus efectos?
- ¿Puede los efectos directamente aprehendidos?
- ¿Qué tecnologías auxiliares requiere?
- ¿Qué comportamiento puede ser posible en el futuro?
- ¿Qué otro tipo de tecnología puede hacerse posible?
- ¿Altera nuestro sentido del tiempo y la relación de forma que conduzcan al nihilismo?

## Vocacionales

- ¿Cómo es su impacto sobre lo artesanal?
- ¿Reduce, mata o incrementa la creatividad humana?
- ¿Es la tecnología menos importante para la tarea?
- ¿Reemplaza o ayuda a las personas en su tarea?
- ¿Puede ser sensible para circunstancia orgánicas?
- ¿Empeora o mejora la calidad de los bienes?

## Políticas

- ¿Cuál es su mística?
- ¿Concentra o ecualiza el poder?
- ¿Necesita o instituye un conocimiento de élites?
- ¿Es totalitarista?
- ¿Necesita de una burocracia para su perpetuación?
- ¿Qué requerimientos legales necesita?
- ¿Debilita la autoridad moral tradicional?
- ¿Neceista de la defensa militar?
- ¿Habilita o sirve para los propósitos militares?
- ¿Como afecta al warfare?
- ¿Adopta comportamientos o pensamientos masivos?
- ¿Es consistente con la creación de una economía global?
- ¿Potencia a las corporaciones transnacionales?
- ¿Qué tipo de capital necesita?

## Metafísicas

- ¿Qué aspecto de nuestro mundo interior refleja?
- ¿Expresa amor?
- ¿Expresa furia?
- ¿Qué aspecto de nuestro pasado refleja?
- ¿Refleja cinismo o pensamiento lineal?
