---
title: Diciembre 2020
---

# Cambá CTTE Ezine Diciembre 2020

## Canción

```
Lo que espero de ti me duele, me duele solo pensar
Si se siente el peso en la espalda, la piel del otro es igual
Es mas fácil, es igual, es mas fácil
Aunque aun no sepas bien como conocer a quien tienes al lado
No guardes lo tuyo en un bolsillo de ocación
Si en el vientre sentís hormigueos es que sos de verdad, se da sin pensar
Y aunque las bocas te apunten livianas, con cuidado lo que venga de ti
Si el recuerdo apacigua tu sueño, no dejes de hablar cuando estes despierto
Para algunas cosas de la vida no hay forma de estar listo
No por ser fuerte la voz es certera, y ser muchos tampoco es tan lindo
Porque se que mi voz se te graba, como hacer para no equivocarme
Y en desorden perturbar la calma al momento que tu hijo te hable
Y aunque las bocas te apunten livianas, con cuidado lo que venga de ti
Si el recuerdo apacigua tu sueño no dejes de hablar cuando estes despierto
Cuando estes despierto
Cuando estes despierto
```

Canción llamada **Lo que espero de ti** de [Alejandro Balbis](https://es.wikipedia.org/wiki/Alejandro_Balbis) - [Video](https://www.youtube.com/watch?v=qHXF_EC89v4).

De regalo una entrevista hecha por ..::Crear::.. a Alejandro cuando paso por Lanús hace 9 años: Entrevistador Neto, Sostenedor de Camara Josx. [Video Recuerdo](https://www.youtube.com/watch?v=Afnym8PGrX0)


## Herramientas de software

- [Olivia](https://github.com/olivia-ai/olivia): Chatbot hecho en Golang que usa Machine learning. Su objetivo es proveer una solución alternativa y libre para los servicios como DialogFlow.
- [Elivia](https://gitlab.e.foundation/e/elivia/): Asistente personal de /e/OS, de la fundación [E](https://e.foundation/)
- [SMAT](https://www.smat-app.com/about) - Kit de herramientas para análisis de redes sociales
- [Smolpxl](https://gitlab.com/andybalaam/smolpxl) - biblioteca JavaScript para hacer juegos pixelados retro
- [Pifuhd](https://github.com/facebookresearch/pifuhd): Convertir imagén en modelo 3d
- [Bubbleteae](https://github.com/charmbracelet/bubbletea): Para construir aplicaciones de terminal de manera divertida y funcional


## Informes/Encuestas/Lanzamientos/Capacitaciones

- Entrevista de la revista **Replay** a Miguel "Miky" Ojeda: Made in Lomas (Hackeando juegos) - [PDF](./assets/made_in_lomas.pdf)
- [Guía de compra de tecnología ética de la FSF](https://www.fsf.org/givingguide/v11/)
- [Alternativas para Reemplazar google](https://nomoregoogle.com/)

## Noticias, Enlaces y Textos

### El deseo de cambiarlo todo

- [Canal 3eras Jornadas de Género y Software Libre](https://tube.undernet.uy/accounts/tusl/video-channels)
    - Lo que no se nombra, no existe. Disputando espacios y lenguaje [1](https://tube.undernet.uy/videos/watch/82384829-6b9a-4cf5-9684-bbe8796c1ec7) [2](https://tube.undernet.uy/videos/watch/58b9f65b-04c4-40b7-91e3-1f4c64dd689e)
    - Por qué a las feministas nos importa el software libre [1](https://tube.undernet.uy/videos/watch/32910694-f123-4ec3-b0f4-bb9020329e6c) [2](https://tube.undernet.uy/videos/watch/50c56a59-37ca-4918-a11c-0c663b996445)
    - [Presentación del Recursero Trans](https://tube.undernet.uy/videos/watch/e8aa4070-fec8-4cd0-8159-4dbb628f3905)

- [Wini Wini y el chiptune de juguetes](https://rebelion.digital/2020/11/23/repo-wini-wini-y-el-chiptune-de-juguetes/)

### Sociales/filosóficos

- [Exposición de Nicolas Doallo en AbreLatam:  Inteligencia Artificial en América Latina ¿sólo una moda?](https://youtu.be/eVQN8lEL9gc?t=908)
- [Presentación del libro "En letras de sangre y fuego", Trabajo, máquinas y crisis del capitalismo de George Caffentzis, editado por Tinta Limón y Fundación Rosa Luxemburgo](https://www.youtube.com/watch?v=mC1_DAq3Bok)
- [EL HACKER COMO FILÓSOFO](https://www.youtube.com/watch?v=jCt8vQMCNiQ): Charla Video-Conferencia de Santiago de Salterain
- [Galaksija](https://jacobinmag.com/2020/08/computer-yugoslavia-galaksija-voja-antonic): Vieja Computadora para armar, pionera en Jugoslavia, [Más info](https://en.wikipedia.org/wiki/Galaksija_%28computer%29)


### Otras

- [Escalera Schroeder en 3d](https://www.microsiervos.com/archivo/juegos-y-diversion/escalera-tridimensional-schroeder-ilusion-visual.html)
- [Librehost](https://libreho.st/): Red de cooperación y solidaridad que usa software libre para fomentar la descentralización a tráves de federación y plataformas distribuidas. [Listado](https://lab.libreho.st/librehosters/directory)
- Columna de Tecnología de Neto, Pinky y Cerebro, en Sonámbules en [FM Las CHACRAS 104.9](https://radiolaschacras.blogspot.com/)
   - Entrevista a Maia Numerosky, socia de Eryx - [Audio](https://archive.org/details/entrevista-IA-humedales)
