---
title: Diciembre 2021
---

# Cambá CTTE Ezine Diciembre 2021

| ![estrategia](@img/estrategia.jpg) |
| :--: |

```
Esperanza potencial: la canción por escribir,
Como un arma cargada con la palabra
La palabra que apuntala al corazón la señal de reaccionar
La ambición de hacer sin renunciar
Aquel puente que nos una en el final
Y evitar la desilusión…

Si ves como es hoy lo fundamental
Crear, crecer, hacer al aprender a ser mejor
Es lo que soy. (La rabia y la poesía en la canción)

Desgarra el pronunciar
Lo que es inexorable:
las fronteras por cruzar,
su horizonte está siempre más allá…

…o hacer lo imposible algo real
Subvirtiendo todo por imaginar…
Un sendero nuevo, es la razón!
Como orfebre de los versos, denunciar
Que mirar sin ver es resignar,
ser ciego en la obstinación.

Si ves como es hoy lo fundamental
Crear, crecer, hacer al aprender a ser mejor
Es lo que soy, en lo que soy:
(mil voces se hacen una en la canción)

Dar en mi voz una ilusión
(La rabia y la poesía en la canción)
```

Aquí la letra de [**Rodia**](https://es.wikipedia.org/wiki/Rodia) el tema [La rabia y la poesia](https://www.youtube.com/watch?v=T688SjZb4G8)


## Aportes a proyectos de Software Libre
> Festival de aportes a scrapers de Redes sociales

### [Osintgram](https://github.com/Datalux/Osintgram/)
- [PR Documentación](https://github.com/Datalux/Osintgram/pull/363)
- [PR Salida json de comentarios ](https://github.com/Datalux/Osintgram/pull/364)

### [Tiktok-scraper](https://github.com/drawrowfly/tiktok-scraper/)
- [PR Documentación docker](https://github.com/drawrowfly/tiktok-scraper/pull/703)
- [PR Documentación cli](https://github.com/drawrowfly/tiktok-scraper/pull/704)
- [PR Dockerfile versión de imagenes](https://github.com/drawrowfly/tiktok-scraper/pull/705)

### [Twint](https://github.com/twintproject/twint/)
- [PR Fijar version de aiohttp ](https://github.com/twintproject/twint/pull/1299)
- [PR Mejora en Dockerfile](https://github.com/twintproject/twint/pull/1300)

### [Fisherman](https://github.com/Godofcoffe/FisherMan/)
- [PR Colorama no existe](https://github.com/Godofcoffe/FisherMan/pull/6)
- [PR Attribute error](https://github.com/Godofcoffe/FisherMan/pull/7)
- [PR Docker support](https://github.com/Godofcoffe/FisherMan/pull/8)

### [Facebook-scraper](https://github.com/kevinzg/facebook-scraper/)
- [PR UnboundLocalError catch](https://github.com/kevinzg/facebook-scraper/pull/547)
- [PR Debug](https://github.com/kevinzg/facebook-scraper/pull/580)

### [Django-import-export](https://github.com/django-import-export/django-import-export/)
- [PR Migración faltante](https://github.com/django-import-export/django-import-export/pull/1346)
- [PR Soporte para Big integer](https://github.com/django-import-export/django-import-export/pull/1351)

### [Python social auth](https://github.com/python-social-auth/social-core/)
- [PR Errores en apple backend](https://github.com/python-social-auth/social-core/pull/634)


## Herramientas de software

- [Supabase](https://supabase.com/) - Alternativa de software para Firebase
- [ShellCheck](https://github.com/koalaman/shellcheck) - ShellCheck, Análisis estático para script de shell
- [Darling](https://github.com/darlinghq/darling) - Capa de Emulación de Darwin/macOS para Linux
- [Revolt](https://github.com/revoltchat) - Chat Web, Alternativa a Discord
- [Rapid Photo Downloader](https://damonlynch.net/rapid/) - Manejo simple y profesional para manejar tus fotos

## Informes/Encuestas/Lanzamientos/Capacitaciones

- Charla: [Global Coop Show & Tell - Open Food network](https://www.youtube.com/watch?v=gtoAquep5JA) - [CoopsDevs](https://coopdevs.org/) desde Cataluña.
- Texto: [Coopedia: Una base de conocimientos de código abierto sobre cooperativas Revista idelcoop , numero 234](https://www.idelcoop.org.ar/revista/234/coopedia-una-base-conocimientos-codigo-abierto-cooperativas) - Florencia Otarola de Cambá
- Libro: [Asambles y reuniones: Metodologías de autoorganización](https://es.hackmeeting.org/hm/images/f/f7/Asambleas_y_reuniones-TdS.pdf)
- [Modelo de Gobernanza disco.coop](https://disco.coop/governance-model/)


## Noticias, Enlaces y Textos

### Sociales/filosóficos

- Las DAO están de moda, Invito reflexionar sobre su potencia uso en organizaciones sociales:
[dwalden Boske: Intro a DAOs para organizaciones sociales](https://www.youtube.com/watch?v=Q4M3azyt6IY)
- [Diego Sztulwark. La ofensiva sensible. Seminarios de Cultura y Hábitat Taller a77](https://www.youtube.com/watch?v=srALZstjy4Q)
- [Spinoza y La tirania del trabajo formal asalariado](https://www.youtube.com/watch?v=g82_5F8PePM)
- Reflexiones de politica coyuntural Argetnina: [El Método Rebord #15 - Leandro Santoro](https://www.youtube.com/watch?v=Y1YuKsS9vYA)


### Tecnología

- [Programar un clon de Whatsapp en React Native](https://decodebuzzing.medium.com/whatsapp-clone-react-native-part1-8054ba362884)
- [Crear un juego multiusuario realtime desde cero](https://www.smashingmagazine.com/2021/10/real-time-multi-user-game/)
- [¿Cómo publicar una biblioteca en python de software libre?](https://simonwillison.net/2021/Nov/4/publish-open-source-python-library/)
- [Implicancias masivas de las conservación de la libertades del software Vs. Vizio](https://fossa.com/blog/massive-implications-software-freedom-conservancy-vs-vizio/)


### Otras

- [3er congreso de Ciencia abierta y ciudadana - 1/12 al 3/12](https://congresos.unlp.edu.ar/ciaciar/talleres-y-sesiones-tematicas/)
- [Guía para regalar tecnología etica](https://www.fsf.org/givingguide/v12/)
