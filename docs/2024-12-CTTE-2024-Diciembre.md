---
title: Diciembre 2024
---

# Cambá CTTE Ezine Diciembre 2024

<div style="text-align: center;">

![Diciembre 2024](@img/diciembre2024.jpg)

</div>

```
"...when we let it ring from every village and every hamlet,
from every state and every city, we will be able to speed up
that day when all of God's children-black men and white men,
Jews and Gentiles, Protestants and Catholics-will be able to
join hands and sing in the words of the old Negro spiritual:
"Free at last! Free at last! Thank God Almighty, we are free at last!..."
(Martin Luther King, Jr., "I Have a Dream")

Desangrandonos, el miedo es de los débiles
El cielo se torna gris, una batalla terrible
Resiste, resiste, y que el sol brille
Y que la fuerza hoy en mi corazón, los ilumine
Y en sus manos nos guie hacia un mundo eterno es posible
Y que su espada abriendo mi pecho no nos limite
A soñar un cielo en el que nuestros hijos conquisten

Enfrentandonos y que hablen los imbéciles
Mi visión se torna gris, que se apiaden los débiles
Resiste, resiste, y que el sol brille
Y que la fuerza hoy en mi corazón, nos ilumine
Y en sus manos nos guie hacia un mundo eterno es posible
Y que su espada abriendo mi pecho no nos limite
A soñar un cielo en el que nuestros hijos conquisten

Resiste!
Y hoy aquí es nuestra muerte, nuestra victoria
Resiste!
Y que los dioses nos protejan en sus manos y la fuerza de tu corazón
Resiste!
Soñemos con la gloria eterna, hacia la victoria vamos
Resiste! Resiste! Resiste!
Lo siento en nuestra piel!
```

Letra de la Canción ["La batalla de las termópilas"](https://www.youtube.com/watch?v=9P0pLGMndBQ) de la banda [Da Skate](https://es.wikipedia.org/wiki/Da_Skate) basada en la [historia hómonima](https://es.wikipedia.org/wiki/Batalla_de_las_Term%C3%B3pilas)


## Contribuciones software libre
### PRs
- [Share: Adds logical to valid the expired datetime param in create_share function](https://github.com/aaronsegura/nextcloud-async/pull/23)
- [Fix environment variables setup for langflow](https://github.com/langfuse/langfuse-docs/pull/961)
- [Fix: typo pytest filename](https://github.com/Aider-AI/aider/pull/2369)
- [Readme: Add missing servers to the featured server list](https://github.com/modelcontextprotocol/servers/pull/92)
### Issues
- [Resolver: Gitlab support](https://github.com/All-Hands-AI/OpenHands/issues/5210)
- [Guardrails Support](https://github.com/langflow-ai/langflow/issues/4863)
- [Need Gitlab MCP Server](https://github.com/modelcontextprotocol/servers/issues/66)
### Software
- [Fsfs: Free space from space](https://github.com/josx/fsfs)

## Herramientas de software
- [Bruno: alternativa liviana a postman/insomnia](https://github.com/usebruno/bruno)
- [VSDK](https://github.com/ventilastation/vsdk/) - Kit de desarrollo de [Ventilastation](https://ventilastation.protocultura.net/)
- [Escucha el sonido del silencio](https://github.com/anars/blank-audio/)
- [Pake: Convertir cualquier web a una aplicación de escritorio](https://github.com/tw93/Pake)
- [Tateti en una llama a printf](https://github.com/carlini/printf-tac-toe)

## Informes/Encuestas/Lanzamientos/Capacitaciones
- [Guía de compras ética de Tecnología del día de acción de gracias](https://www.fsf.org/givingguide/v15/)
- [Resultados Encuesta Estado de Css 2024](https://2024.stateofcss.com/en-US)
- [Ranking de Lenguajes de Programación](https://www.devjobsscanner.com/blog/top-8-most-demanded-programming-languages/)
- [Liberación de georeferencias de Foursquare](https://docs.foursquare.com/data-products/docs/access-fsq-os-places)
- [Dentro del radar de Tecnologíar: Un documental sobre Thoughtworks](https://www.youtube.com/watch?v=w_u8mQpTuhc)

## Noticias, Enlaces y Textos

### Tecnología
- [Guía práctica para cuando los proyectos Django crecen](https://slimsaas.com/blog/django-scaling-performance)
- [IA: logs de decisión automoatizados](https://addyosmani.com/blog/automated-decision-logs/)
- [WebVM: entornos virtual linux corriendo en le navegador con WebAssembly](https://labs.leaningtech.com/blog/webvm-20)
- [Guido van Rossum: Python y el futuro de la programación | Lex Fridman Podcast #341](https://www.youtube.com/watch?v=-DVyjdw4t9I)
- [Mi IA local, Asistencia de Voz (reemplazando Alexa!!)](https://www.youtube.com/watch?v=XvbVePuP7NY)


### LLMs
- [Guardrails: Agregando filtros a los LLMs](https://github.com/guardrails-ai/guardrails)
- [Perplexica un motor de busqueda potenciado por IAs](https://github.com/ItzCrazyKns/Perplexica)
- [Aider Composer: Integración de Aider a VScode](https://github.com/lee88688/aider-composer)
- Nuevos modelos chinos  "open souorce" para código:
    - [Qwen2.5-coder](https://github.com/QwenLM/Qwen2.5-Coder) (Ya disponible en la BestIA)
    - [Athene-v2](https://nexusflow.ai/blogs/athene-v2) (Ya disponible en la BestIA)
    - [Deepseek-r1-lite](https://api-docs.deepseek.com/news/news1120) (Aún No esta lanzado para descargar)
- [Model Context Protocol: Protocolo de Anthropic libre](https://glama.ai/blog/2024-11-25-model-context-protocol-quickstart)
- [Dario Amodei: CEO de Anthropic sobre Claude, AGI & el futuro de la humanidad y la IA | Lex Fridman Podcast #452](https://www.youtube.com/watch?v=ugvHCXCOmm4)
