# Mi cuerpo, mi reino, mi carne, mi sangre, mi piel, donde yo mando, donde yo decido.

La salud, y por supuesto la ginecología, es una cuestión que sobrepasa abundantemente los límites de nuestra especie y nos conecta a las condiciones generales de la vida. Algo así de grande no puede estar en manos de una casta de especialistas, ni de una especie. "Esto está pasando".
La estrategia infame del poder descarnado de su \<salud\> hipócrita, un pacto escalofriante de nuestro cuerpo hacía sí mismo: (des)conexión, delegarse al control medico-farmaceutico-dependiente que se nutre de los miedos y la ignorancia que genera sobre nosotras mismas. "Esto está pasando". La industria alimentaria, cosmética y farmacéutica, así como las drogas recreativas, se basan en el mundo vegetal o en moléculas de síntesis que copian sus principios activos. Todo entra directamente y sin metáforas a nuestros cuerpos... ¡Que esa incorporación sea alianza de opresiones, poder para todas, brujería!

La farmacopea que proponemos cabe en nuestras cocinas, es experimental y sensual, se practica colectivamente y explora saberes vividos y en algunos casos casi olvidados. Recuerda que hackear el cuerpo es muy complicado! Ninguna de estas recetas es instantánea, se deben repetir y complementar.
Se trata de desacralizar la ciencia y la farmacopea; de desmitificar el Lab, parodia de la imposición blanca; de recuperar refentes médicos como Maria Gillain-Boivin, la abuela curandera de tu pueblo o el chamán de tu comunidad; de reinventar herramientas; exigir que la técnica sea escalable y priorice el aprendizaje colectivo.

No estamos cancelando el saber médico, solo lo queremos abierto y de nuestro lado, desarmar la fachada de igualdad de los sistemas de salud, una trans-formación radical de subversión curativa y disidencia clínica, redes de apoyo humano como los Centros Médicos Gratuitos de Las Panteras Negras; el Sistemas de Salud Autónomo de las Comunidades Zapatistas, como la Clínica "Comandanta Ramona"; el Centro de Salud Autogestionado Mapuche "Boroa Filulawen"; el Espacio Okupado Social de Salud de un antiguo ambulatorio de Grecia; Las Hesperian Health Guides o la potencia de los "Médicos de Calle" como Green & Black Cross(GB).

## Salud de código abierto y declaración de datos abiertos.

El lenguaje médico necesita ser traducido, adaptado a nuestros cuerpos y experiencias, no a estadísticas o protocolos.
Tiene que ser comprensible y cercano. Necesitamos saber y comprender, que la información sea pública, legible, traducida y abierta. Protocolos clínicos, diagnósticos estándar e infografías deberían estar disponibles.

Cuando lleguemos a entender el porqué este o aquel análisis y lo que implica (en tiempo, efectos secundarios y tecnologías) y estar completamente informadas sobre los medicamentos y tratamientos requeridos y sus alternativas, cuando no sólo recibamos prescripciones del tipo "debes hacer/tomar" o declaraciones personales moralistas del doctor, entonces y sólo entonces el consentimiento de tratamientos invasivos, procedimientos de riesgo, consecuencias devastadoras o facturas a pagar prohibitivas, será NUESTRO consentimiento.
Las tecnologías deberían ser cercanas y fáciles, actualizadas y desafiantes (preservativos que cambian de color vs diagnósticos de laboratorio caros; investigación de contracepción masculina vs bombardeo hormonal a las mujeres; empoderamiento del suelo pélvico vs cirugías masivas de incontinencia; alternativas como la vasectomía vs no alternativas al aborto).
La memoria médica tiene que ser reseteada de manera crítica, "Los Padres" de la Ob/Gin deberían mostrarse en su contexto y no como héroes o santos. No honraremos su macabro legado, usaremos de manera crítica lo que han dejado sin olvidar nunca y compartiendo siempre la manera que llegaron a tener un lugar en la historia de la medicina.

Devenir PLANTA - BRUJA - MÁQUINA
Transhackfeminismo Ginecológico en Alegre Distopía.
