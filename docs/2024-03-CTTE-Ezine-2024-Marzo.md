---
title: Marzo 2024
---

# Cambá CTTE Ezine Marzo 2024

<div style="text-align: center;">

![Playa](@img/marzo2024.png)

</div>

```
Muero en este verano,
Me aturdió tanta superficialidad
Por tanta voz que no para de hablar,
Vuelvo en este verano
No es temprano ni tarde para empezar
Canto a la vez despedida y cuple
Al barrio de ayer hoy vuelvo a caer
Me embriaga otra vez, vida, mi palidez.

Muero en este verano,
Me aturdió tanta superficialidad
Por tanta voz que no para de hablar,
Vuelvo en este verano
No es temprano ni tarde para empezar
Canto a la vez despedida y cuple
Al barrio de ayer hoy vuelvo a caer
Me embriaga otra vez, vida, mi palidez.

Por el amor de cantarte a vos,
El carrusel como un sueño es.
El camino hay que desandar extenuado barro pisar,
Un esfuerzo para entender,
Dura trama la de crecer.
Muero en este verano...
```

Letra del tema [Muero en este Verano](https://www.youtube.com/watch?v=lf9MMMTOnlQ) de [Alejandro Balbis](https://es.wikipedia.org/wiki/Alejandro_Balbis).


## Cooperativas/Economía Popular

- Cambá y su [Espacio Abierto de Tecnologías Creativas en Las Chacras, Traslasierra, Córdoba](https://ltc.camba.coop/2024/02/27/espacio-abierto-de-tecnologias-creativas-las-chacras-traslasierra-cordoba/)
- [Cambá: Crónica de la Bicicleteada por la costa de Avellenda](CTTE-texto-bicicleteada1)
- Participación de FACTTIC en video [VARLOR ARGENTINO, Cooperativas y Mutuales por el País](https://www.youtube.com/watch?v=6lQZNshqgxs&list=PL44O7NT6CLXErocMi4rO48vir1pXMch13&index=8)
- [¿Quién necesita una persona que maneje proyectos?](https://medium.com/@nicolasdimarco/who-needs-a-pm-right-a7a5f469d66e)
- [Cooperativismo: Sindicatura](https://www.youtube.com/watch?v=ok7Ko70wx7U)

## Herramientas de software

- [Sqitch](https://sqitch.org/) - Manejo de cambios para motores de base de datos.
- [Postgrest](https://postgrest.org/) - Servi api restful desde cualquier base de datos Postgres.
- [HTMZ](https://leanrada.com/htmz/) - Herramienta de bajo poder para html
- [UV](https://github.com/astral-sh/uv) - Instalador muy rápido de paquetes de python escrito en Rust.

## Informes/Encuestas/Lanzamientos/Capacitaciones

- [Casablanca USA: El software del futuro deberia ser "memory safe"](https://www.whitehouse.gov/oncd/briefing-room/2024/02/26/press-release-technical-report/?s=08)
- [OWASP: Seguridad y gobernanza de LLMs](https://owasp.org/www-project-top-10-for-large-language-model-applications/llm-top-10-governance-doc/LLM_AI_Security_and_Governance_Checklist-v1.pdf)
- [Make: Guia de Placas de Hardware](https://make.co/wp-content/uploads/2024/02/Make-Volume-87-Boards-Guide-2024-Insert.pdf)
- [Top ten de herammientas para desarrolladores del 2023](https://stackshare.io/posts/top-developer-tools-2023)
- [Partido Pirata: Archivo de Educar](https://archivo-pirata-antifascista.partidopirata.com.ar/2024/01/27/archivo-de-educ-ar.html)

## Noticias, Enlaces y Textos

### Tecnología

- [Jerga del argot hacker, "Jargon"](https://es.wikipedia.org/wiki/Jargon_File) - [v4.4.8](https://www.catb.org/jargon/) y [Original](https://www.dourish.com/goodies/jargon.html)
- [Historia: ¿Por que usamos SSH en el puerto 22](https://www.ssh.com/academy/ssh/port)
- [Javascript Hinchado](https://tonsky.me/blog/js-bloat/)
- [Freenginx](https://freenginx.org/) - [Fork de nginx de desarrollador principal](https://mailman.nginx.org/pipermail/nginx-devel/2024-February/K5IC6VYO2PB7N4HRP2FUQIBIBCGP4WAU.html)
- [Quitando contraseña de pdf](https://itsfoss.com/remove-pdf-password-linux/)


### LLMs
- [Cheatsheet](https://cheatsheet.md/) - Tutoriales de herramientas de IA.
- [Langchain tutorials](https://python.langchain.com/docs/additional_resources/tutorials)
    - [Agentes LangChain con modelos Open Source](https://www.youtube.com/watch?v=Ce03oEotdPs)
    - [The LangChain Cookbook Part 2 - Guia para principiantes, 9 casos de uso](https://www.youtube.com/watch?v=vGP4pQdCocw)
- [Open-Interpreter](https://github.com/KillianLucas/open-interpreter/) - Interfaz de lenguaje natural para computadoras.
