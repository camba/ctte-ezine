# Análisis Encuesta sobre Ezine 2019

Conslusiones sobre los resultados obtenidos de la encuesta sobre la percepción de Cambá sobre la Revista Electrónica de Cambá.
Total de respuestas **21** (18 total, 3 incompletas) y estuvo disponible para contestar durante más de 21 días.


## Análisis Cuantitativo

Para análizar los resultados de la encuesta se realizo un proyecto de software pequeño y lo más rápido posible con dos premisas fundamentales a tener en cuenta:
- Que nos permita visualizar gráficos a partir de los datos de la encueta
- Que nos permita tener una base de código simple para visualización de datos

### Visualizarlo los gráficos en nuestro servidor de staging

- Web: [http://ezine-stats.staging.camba.coop/](http://ezine-stats.staging.camba.coop/)

### ¿Como esta hecho?

- Repositorio: [https://recursos.camba.coop/camba/ezine-stats/](https://gitlab.com/camba/ezine-stats/)
- @florencia contribuyo con los CSS y organización gráfica


## Análisis Cualitativo

A partir de los datos extraídos de la encuesta (campos para volcar palabras escritas como comentarios), se han resumido y tratado de contestar integrando las diferente posiciones y visiones dando como resultado el siguiente texto. No hay voluntad de opinión en este texto de CTTE, sino que se recopilo para que refleje las opiniones vertidas por las personas que trabajan en Cambá.

- Descargar [Datos que exportados en formato csv del Limesurvey](./assets/CTTE-encuesta-ezine-2019.csv)

### ¿Cómo te describirías como lectora?

Somos personas lectoras que profundizamos según nuestros interes especificos de manera bien sesgada (social, tecnológico), casi siempre leemos más durante los días en los que sale o cercanos, y en tiempo productivo y nos cuesta leer el contenido mucho más cuando estamos en casa porque debemos acordarnos.

Factores que juegan encontra de la lectura:
- Falta de nivel técnico
- No se encuentra bien en la wiki
- Falta de tiempo
- Consideramos contenido de manera efimera

Algunos comentarios:
- No se escuchan audios
- No mira herramientas, sino la tendencia
- Se Lee más lo creativo, las reflexiones y lo de música


### Nivel de actualización y utilidad de los temas y contenidos

En líneas generales el contenido es útil, amplio y relevante  más allá de la fecha de publicación original, esta bien la periodicidad del mismo y se nota que hay bastante trabajo hecho. Igualmente se insiste que a veces el contenido esta por encima de las posibilidades o conocimiento, o que son temas que se desconocen y no llaman la atención. Por otro lado también se cuestiona el encasillamiento que produce tener una lectura muy especifica de solo ciertas temáticas y se pondera la posibilidad de opinión sobre los contenidos.

### ¿Le das prioridad en relación a lo que lees en internet habitualmente?

Varias creen que le dan la importancia adecuada, y además consideran que hay esfuerzo grande en hacerla.
Por otro lado se expresa el deseo de intentar darle la atención que creen necesaria, y la alineación con los valores basados en el interes intelectual personal.
Algunas se consideran que no leen demasiado, otras que no tienen tiempo para leer y otras que leen más libros que cosas en internet.

Estrategias de lectura:
- Consumen contenido durante el viaje a Cambá
- Guardar enlaces interesantes
- Priorizar contenido filósofico
- Leerlo apenas sale


### ¿Recordas algun articulo o idea en particular?  

- [La salvación de lo bello](CTTE-texto-salvacion-de-lo-bello)
- [Análisis de la developer survey de stack overflow](CTTE-Ezine-2019-Enero#stackoverflow-2018)
- [Proyecto que te va leyendo los gestos mientras mirás un video](https://stealingurfeelin.gs/)
- [Encuesta de sueldos](CTTE-Ezine-2019-Septiembre#encuesta-de-sueldos-201902-julioagosto)
- [Lenguaje de programación argento onda python](https://despacito-lang.github.io/updates/2019/10/28/hola-mundo.html)
- [Cita a la tesis de Belé,](CTTE-text-belen-tesis)
- [Video de Dario Z](CTTE-entrevista-dario)
- [Unicorn Grocery](CTTE-texto-cooperativas-evaluacion-pares)
- [Nota de Ten Pines](CTTE-texto-10pines)
- [coordinación vs conducción](Coordinación-vs.-conducción)
- [estado de js](CTTE-Ezine-2019-Enero#state-of-js-2018)
- [Confucio](CTTE-Ezine-2019-Noviembre#frase)
- [haveibeenpwned](https://haveibeenpwned.com/)
- [La entrevista a Anita](http://ar.ivoox.com/es/polemica-var-s03m03-showmethemoney_mf_38466587_feed_1.mp3)
- [Aporte de nati](CTTE-texto-devenir-planta-bruja-maquina)
- [Smartmatic en el tema de votacion](CTTE-Ezine-2019-Agosto#seguridad)
- [La gorra](CTTE-Ezine-2019-Agosto#seguridad)
- [Bifo](CTTE-Fenomenologia-del-fin)
- [Zizek](CTTE-Ezine-2019-Abril#más-sociales)
- [Recortes de booom boom kid](CTTE-texto-musica-feminista)
- [Duelo de maquina y humano](CTTE-Ezine-2019-Julio#otras)
- [Grabois](CTTE-texto-economia-popular)
- [Concepto de libertad de espinoza](CTTE-texto-spinoza)
- ["Mi libertad entonces consiste en moverme en un entorno delimitado que me asigne a mi mismo para cada una de las cosas de hago. Es más, mi libertad será mayor y más significativa cuanto más límites al campo de acción y más obstaculos tenga que sortear. Cualquier cosa que disminuya las restricciones también hacer disminuir la fuerza. Cuanto más restricciones uno se impone, más uno se libera de las cadenas que atán al espíritu."](CTTE-Ezine-2019-Junio#igor-stravinsky-dixit)
- Las contribuciones a software libre
- Nuevas versiones de Vue
- Análisis de frase: Tienen el mismo espíritu libertario o están emparentadas con la conyuntura que vive la coope en ese momento

### ¿Abre puertas a temas no conodidos anteriormente?

Los contenidos según los lectores tienen a veces perspectivas que nunca habían considerado, sirven para potencialmente profundizar en alguna temáticas, y ofrecen un panorama de temas para tener en el radar. Algunas ponderan los artículos filosóficos y otras los técnicos.
Se menciona un ejemplo de utilización de contenido como fuente para armar la capacitación de Maquetación.

### ¿Trasmite valores de la cooperativa?

Se puntualiza que al menos no se no atenta contra los valores y por otro lado se ven reflejados en múltiples lugares:

- Noticias de software libre y de cooperativismo
- Promoción de lecturas feministas
- La temáticas y personajes
- Debate político
- Voluntad de incrementar las capacidades de autogestión
- Contexto de nuestro ser cooperativo
- Compartir conocimiento
- Adopción de perspectiva crítica respecto
- Estar "al día" con avances de tecnologías


### ¿Despierta algunas emociones o reflexiones?

La mezcla entre tecnología, poesía, filosofía es el factor que le aporta identidad al contenido,
no se queda en la lectura a nivel interés de la cooperativa para ser más competitiva, sino que genera
un gran deseo de seguir aprendiendo y profundizando la formación.

Algunas de las cosas que más nos emocionan son:

- Frase/fragmento de apertura
- Interpelación por los textos sociales y otras experiencias cooperativas
- Flashback a la adolescencia
- Entrevista a Pepe Mújica

Algunas de la cosas que más nos hacen reflexionar:

- Problemas sociales de la industria del software
- Articulos sobre género
- Gustos
- Amplitud del conocimiento
- Presente y futuro, como miembros de la coope y del mundo en gral


### ¿Recomendarias a otras coops o personas?

En líneas generales debería estar publicada y disponible de manera abierta en internet para compartirla o reenviarla mediante un newsletter.
Hay espacios en especial considerados como FACTTIC, ya que también podria generar que otrxs quieran crear/compartir contenido.
Debemos tener en cuenta que no debe ocupar un mayor tiempo de trabajo que el actual o diversificar el trabajo necesario.

### ¿Tienes algunas sugerencias en qué podemos mejorar?

Listado de mejoras posibles:

- Pieza de arte para encabezar secciones o publicaciones
- "Bajada" (en sentido periodístico gráfico) o párrafo de síntesis para enlaces y articulos
- "Epígrafe que sirva como invitación al lector a introducirse en el tema
- Abrirla por fuera de Cambá
- Agregar más entrevistas
- Mostrar una obra artistica por mes de alguna peronas lectora
- Centralizar y agrupar links y articulos por sección en general (no solo mensual).
- Más Participación de la coope para ampliar las miradas y las palabras
- Agregar anécdota de Cambá o cooperativismo
- Sección colaborativa en comisión de Cambá
- Más tema de tecnologías que usemos o que podamos usar en proyectos
- Qué tenga interfaz gráfica
- Forma cooperativa de armarlo
- Agregar Capacitaciones que hacemos
- Explicitar posición, perspectiva o puntos de vista en los articulos o enlaces técnicos.

Ejemplos de obras de arte:
- [Hotglue](https://www.hotglue.me/)
- [Celeste Leroux](https://celestelerouxel.hotglue.me/)
- [Matadora](https://www.behance.net/gallery/60851319/MATADORA)
- [Digital Detox day](https://www.behance.net/gallery/27019607/Digital-Detox-Day)

