# CTTE Ezine

La cooperativa de trabajo Cambá mensualmente libera una nueva edición de su revista electrónica.
Este repo es la infraestructura técnica que le da soporte a esa publicación.

- Ver publicación del Ezine - [Click Aquí](https://camba.gitlab.io/ctte-ezine/)

## Tecnología

- Usamos [VuePress](https://vuepress.vuejs.org/), un generador de sitios estáticos escrito en Vue.
- Mediante [gitlab-ci](https://docs.gitlab.com/ee/ci/) se hace un deploy automatizado en las [gitlab pages](https://docs.gitlab.com/ee/user/project/pages/).

## Desarrollo

```
npm install
npm start

```

Pone en tu navegador ```http://localhost:8080```

